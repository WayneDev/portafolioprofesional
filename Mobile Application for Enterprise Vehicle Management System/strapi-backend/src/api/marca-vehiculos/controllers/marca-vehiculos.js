'use strict';

/**
 * marca-vehiculos controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::marca-vehiculos.marca-vehiculos');
