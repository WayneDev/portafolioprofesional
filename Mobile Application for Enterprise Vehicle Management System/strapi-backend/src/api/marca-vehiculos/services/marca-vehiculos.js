'use strict';

/**
 * marca-vehiculos service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::marca-vehiculos.marca-vehiculos');
