'use strict';

/**
 * marca-vehiculos router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::marca-vehiculos.marca-vehiculos');
