'use strict';

/**
 * estado-paradas controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::estado-paradas.estado-paradas');
