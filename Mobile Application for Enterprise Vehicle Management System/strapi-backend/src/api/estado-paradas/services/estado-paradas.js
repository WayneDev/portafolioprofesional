'use strict';

/**
 * estado-paradas service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::estado-paradas.estado-paradas');
