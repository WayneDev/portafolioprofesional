'use strict';

/**
 * estado-paradas router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::estado-paradas.estado-paradas');
