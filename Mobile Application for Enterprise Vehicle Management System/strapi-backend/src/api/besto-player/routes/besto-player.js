'use strict';

/**
 * besto-player router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::besto-player.besto-player');
