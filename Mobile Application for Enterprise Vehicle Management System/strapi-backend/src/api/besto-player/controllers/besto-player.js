'use strict';

/**
 * besto-player controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::besto-player.besto-player');
