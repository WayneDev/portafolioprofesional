'use strict';

/**
 * besto-player service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::besto-player.besto-player');
