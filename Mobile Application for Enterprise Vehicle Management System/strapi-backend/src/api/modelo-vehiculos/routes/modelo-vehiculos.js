'use strict';

/**
 * modelo-vehiculos router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::modelo-vehiculos.modelo-vehiculos');
