'use strict';

/**
 * modelo-vehiculos controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::modelo-vehiculos.modelo-vehiculos');
