'use strict';

/**
 * modelo-vehiculos service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::modelo-vehiculos.modelo-vehiculos');
