'use strict';

/**
 * tipo-vehiculos router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::tipo-vehiculos.tipo-vehiculos');
