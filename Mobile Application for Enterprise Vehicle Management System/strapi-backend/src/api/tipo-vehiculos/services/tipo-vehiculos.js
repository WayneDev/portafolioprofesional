'use strict';

/**
 * tipo-vehiculos service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::tipo-vehiculos.tipo-vehiculos');
