'use strict';

/**
 * tipo-vehiculos controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::tipo-vehiculos.tipo-vehiculos');
