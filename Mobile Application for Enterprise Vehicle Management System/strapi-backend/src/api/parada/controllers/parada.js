'use strict';

/**
 * parada controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::parada.parada');
