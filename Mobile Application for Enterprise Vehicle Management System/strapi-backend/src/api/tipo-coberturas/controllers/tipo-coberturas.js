'use strict';

/**
 * tipo-coberturas controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::tipo-coberturas.tipo-coberturas');
