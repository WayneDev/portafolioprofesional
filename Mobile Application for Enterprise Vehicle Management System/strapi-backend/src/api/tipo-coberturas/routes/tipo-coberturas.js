'use strict';

/**
 * tipo-coberturas router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::tipo-coberturas.tipo-coberturas');
