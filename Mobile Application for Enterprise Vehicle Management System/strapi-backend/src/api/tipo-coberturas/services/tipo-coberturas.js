'use strict';

/**
 * tipo-coberturas service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::tipo-coberturas.tipo-coberturas');
