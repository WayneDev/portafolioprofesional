'use strict';

/**
 * poliza-seguros service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::poliza-seguros.poliza-seguros');
