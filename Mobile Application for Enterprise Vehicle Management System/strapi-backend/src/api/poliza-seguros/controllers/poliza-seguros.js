'use strict';

/**
 * poliza-seguros controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::poliza-seguros.poliza-seguros');
