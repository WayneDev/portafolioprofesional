'use strict';

/**
 * poliza-seguros router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::poliza-seguros.poliza-seguros');
