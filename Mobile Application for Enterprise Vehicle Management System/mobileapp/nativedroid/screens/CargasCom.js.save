rimport React, { useState, useEffect, useRef } from 'react';
import { View, Text, StyleSheet, Animated, FlatList, Image} from 'react-native';
import CheckBox from 'expo-checkbox';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import MapView, { Marker, Polyline } from 'react-native-maps';
import * as Location from 'expo-location';
import AsyncStorage from '@react-native-async-storage/async-storage';
import pinMarkerIcon from '../assets/pics/location-pin.png'
import locationMarkerIcon from '../assets/pics/location-pin2.png'
import MapViewDirections from 'react-native-maps-directions';

const MapScreen = ({ navigation, route }) => {
  const ip = '34.94.10.37';
  const { token } = route.params || { token: null };
  const [userData, setUserData] = useState({});
  const [routeData, setRouteData] = useState({});
  const [idRutaNoFinalizada, setIdRutaNoFinalizada] = useState(null);
  const [refuelsData, setRefuelsData] = useState({});
  const [fadeAnim] = useState(new Animated.Value(0));
  const [errorMsg, setErrorMsg] = useState('');
  const [paradasTerminadas, setParadasTerminadas] = useState({});
  const [waypoints, setWaypoints] = useState([]);

 
  const handleParadasTerminadas = (item) => {
     setParadasTerminadas({
       ...paradasTerminadas,
       [item.id]: !paradasTerminadas[item.id],
     });
  };

  useEffect(()=> {
    if (!token) {
      console.error('Token de autenticación no proporcionado');
      return;
    }
  }, []) 

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await fetch(`http://${ip}:1337/api/users/me?populate=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (response.ok) {
          const data = await response.json();
          setUserData(data);
          Animated.timing(
            fadeAnim,
            {
              toValue: 1,
              duration: 1000,
              useNativeDriver: true
            }
          ).start();
        } else {
          console.error('Error al obtener los datos del usuario');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchUserData();
 }, [route.params.token, fadeAnim]);

useEffect(() => {
  const fetchRouteData = async () => {
    try {
      const routeResponse = await fetch(`http://${ip}:1337/api/rutas/${idRutaNoFinalizada}/?populate=*`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (routeResponse.ok) {
        const data = await routeResponse.json();
        setRouteData(data);
        console.log(routeData);
      } else {
        console.error('Error al obtener los datos de la ruta');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };
  fetchRouteData();
}, [idRutaNoFinalizada])

useEffect(() => {
  const fetchRefuelsData = async () => {
    if (userData && userData.rutas && userData.rutas.length > 0) {
      try {
        const routeResponse = await fetch(`http://${ip}/api/cargas-combustibles?filters[vehiculos][id]=${routeData.data.attributes.vehiculo.data[0].id}&populate=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (routeResponse.ok) {
          const data = await refuelsResponse.json();
          setRefuelsData(data);
          console.log({refuels: refuelsData});
        } else {
          console.error('Error al obtener los datos de las cargas de combustible');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
  };

  //fetchRefuelsData();
 }, [routeData])


 useEffect(() => {
  if (routeData && routeData.data && routeData.data.attributes.paradas) {
      const paradas = routeData.data.attributes.paradas.data; // Accede a paradas.data
      console.log(paradas);
      if (Array.isArray(paradas)) { // Verifica que paradas es un array
        const waypoints = paradas.map(parada => ({
          latitude: parada.attributes.coordenadas.latitude,
          longitude: parada.attributes.coordenadas.longitude,
        }));
        setWaypoints(waypoints);
      }
  }
 }, [routeData]);

 useEffect(()=> {
    if(userData.rutas) {
      const filtroRuta = userData.rutas.find(ruta => ruta.finalizado === false)
      setIdRutaNoFinalizada(filtroRuta ? filtroRuta.id : null);
    }
 },[userData])

const GOOGLE_MAPS_APIKEY = 'AIzaSyCFJCJC8Hom3XZsmKfOS1z2XimOsFPuLo4';

  return (
    <LinearGradient colors={['#7b92b2', '#fff']} style={styles.background}>
      <View style={[styles.mainContainer, styles.container]}>
        <View style={styles.header}>
          <View style={[styles.button]}>
            <Ionicons name="map-outline" color={'#fff'} size={22} />
          </View>
          <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#fff'}} >Ruta actual</Text>
        </View>
      </View>
    </LinearGradient>
  );
};


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginVertical: 70,
    marginHorizontal: 40,
  },
  header: {
    flexDirection: 'row',
  },
  routeInfoContainer: {
    marginVertical: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#7b92b2',
    paddingBottom: 10,
    marginBottom: 10,
  },
  subTitle: {
    color: '#0792b3',
  },
  map: {
    width: '100%',
    height: '60%',
    marginTop: 10,
  },
  button: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 4,
    borderColor: '#0792b3',
    backgroundColor: '#0792b3',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    marginRight: 10
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5,
    padding: 10,
  },
  itemContainer: {
      marginBottom: 10,
      padding: 10,
      borderWidth: 1,
      borderColor: '#ccc',
  },
  background: {
    flex: 1,
    width: '100%',
  },
  titulo: {
    fontSize: 50,
    color: '#7b92b2', 
    fontWeight: 'bold',
    paddingHorizontal: 20,
  },
  subTitle: {
    fontSize: 20,
    color: 'gray',
    paddingBottom: 30
  },
  textInput: {
    padding: 10,
    paddingStart: 30,
    width: '80%',
    height: 50,
    marginTop: 10,
    borderRadius: 30,
    backgroundColor: '#fff',
  },
  errorMessage: {
    fontSize: 14,
    color: 'red',
    marginTop: 20,
  }
});

export default MapScreen;
