//LISTO

import React, { useState, useEffect, useRef } from 'react';
import { View, Text, Image, StyleSheet, Animated, TextInput, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import carGIF from '../assets/pics/car-wheel.gif'

const CarInfScreen = ({ navigation, route }) => {
  const [userData, setUserData] = useState(null);
  const [routeData, setRouteData] = useState(null);
  const [vehicleData, setVehicleData] = useState(null);
  const [idRutaNoFinalizada, setIdRutaNoFinalizada] = useState(null);
  const fadeAnim = new Animated.Value(0);
  const ip = '34.94.10.37';
  const { token } = route.params || { token: null }; 

  useEffect(()=> {
    if (!token) {
      console.error('Token de autenticación no proporcionado');
      return null;
    }
  }, []) 

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await fetch(`http://${ip}:1337/api/users/me?populate=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (response.ok) {
          const data = await response.json();
          setUserData(data);
          Animated.timing(
            fadeAnim,
            {
              toValue: 1,
              duration: 1000,
              useNativeDriver: true
            }
          ).start();
        } else {
          console.error('Error al obtener los datos del usuario');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchUserData();
 }, [route.params.token]);

 useEffect(() => {
  const fetchRouteData = async () => {
    if(idRutaNoFinalizada!=null) {
      try {
        const routeResponse = await fetch(`http://${ip}:1337/api/rutas/${idRutaNoFinalizada}/?populate=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (routeResponse.ok) {
          const data = await routeResponse.json();
          setRouteData(data);
          //console.log(routeData);
        } else {
          console.error('Error al obtener los datos de otra ruta');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
  };
  fetchRouteData();
}, [idRutaNoFinalizada])

useEffect(()=> {
  if (userData) { // Asegúrate de que userData no sea null
     const filtoRuta = userData.rutas.find(ruta => ruta.finalizado === false);
     setIdRutaNoFinalizada(filtoRuta ? filtoRuta.id : null);
  }
 },[userData])

  useEffect(() => {
    const fetchVehiculeData = async () => {
      if(userData) {
        try {
          const response = await fetch(`http://${ip}:1337/api/vehiculos/${userData.vehiculos_users[0].id}/?populate=*`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          if (response.ok) {
            const data = await response.json();

            setVehicleData(data);
            
            //console.log('Datos del vehículo:', data);
  
            if (data && data.data && data.data.attributes) {
              console.log('Placas del vehículo:', data.data.attributes.placas);
            } else {
              console.error('No se encontraron las placas del vehículo');
            }
  
            Animated.timing(
              fadeAnim,
              {
                toValue: 1,
                duration: 1000,
                useNativeDriver: true
              }
            ).start();
          } else {
            console.error('Error al obtener los datos del vehiculo');
          }
        } catch (error) {
          console.error('Error:', error);
        }
      }
    };
    fetchVehiculeData();
  }, [userData, token]);

  
  return (
    <LinearGradient colors={['#7b92b2', '#fff']} style={styles.background}>
      <View style={[styles.mainContainer, styles.container]}>
      <View style={styles.header}>
        <View style={[styles.button]}>
          <Ionicons name="car-outline" color={'#fff'} size={22} />
        </View>
        <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#fff'}} >Vehiculo actual</Text>
      </View>
      
      {vehicleData && (
        <>
            <View style={{marginVertical: 10, backgroundColor: '#7b92b2', padding: 15, borderRadius: 20}}>
            <Text style={{ fontWeight: 'bold', fontSize: 22, color: '#fff', paddingBottom: 15}}>Datos principales</Text>
            {vehicleData && vehicleData.data.attributes.modelo && (
              <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#fff'}}>Tipo : <Text style={{fontWeight: 'normal', color: '#fff'}}>{vehicleData.data.attributes.tipo.data[0].attributes.nombre}</Text></Text>
            )}
            {vehicleData && (
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#fff'}}>Placas: <Text style={{fontWeight: 'normal', color: '#fff'}}>{vehicleData.data.attributes.placas}</Text></Text>
            ) } 
            {vehicleData && (
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#fff'}}>No.Serie: <Text style={{fontWeight: 'normal', color: '#fff'}}>{vehicleData.data.attributes.numSerie}</Text></Text>
            ) } 
          </View>
          <View style={{display: 'flex', width: '100%', alignItems: 'center'}}>
            <Image source={require('../assets/pics/car-wheel2.gif')} style={{width:150, height: 150}}/>
          </View>

          <View style={styles.detailsContainer}>
              <Text style={{ fontWeight: 'bold', fontSize: 22, color: '#fff', paddingBottom: 10}}>Más datos</Text>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Marca:</Text>
                {vehicleData && (
                  <Text style={[styles.text, { color: '#fff', paddingHorizontal: 10, fontWeight: 'normal'}]}>{vehicleData.data.attributes.marca.data[0].attributes.nombre}</Text>
                ) } 
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Modelo:</Text>
                {vehicleData && (
                  <Text style={[styles.text, { color: '#fff', paddingHorizontal: 10, fontWeight: 'normal'}]}>{vehicleData.data.attributes.modelo.data[0].attributes.nombre}</Text>
                ) } 
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Color:</Text>
                {vehicleData && (
                  <Text style={[styles.text, { color: '#fff', paddingHorizontal: 10, fontWeight: 'normal'}]}>{vehicleData.data.attributes.color}</Text>
                ) } 
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Año:</Text>
                {vehicleData && (
                  <Text style={[styles.text, { color: '#fff', paddingHorizontal: 10, fontWeight: 'normal'}]}>{vehicleData.data.attributes.anio}</Text>
                ) } 
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Capacidad de carga:</Text>
                {vehicleData && (
                  <Text style={[styles.text, { color: '#fff', paddingHorizontal: 10, fontWeight: 'normal'}]}>{vehicleData.data.attributes.modelo.data[0].attributes.cantCarga}</Text>
                ) } 
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Capacidad de pasajeros:</Text>
                {vehicleData && (
                  <Text style={[styles.text, { color: '#fff', paddingHorizontal: 10, fontWeight: 'normal'}]}>{vehicleData.data.attributes.modelo.data[0].attributes.capPasajeros}</Text>
                ) } 
              </View>
              <View style={styles.section}>
                <Text style={[styles.label, { color: '#fff', marginBottom: 10 }]}>Kilometraje de adquisición:</Text>
                {vehicleData && (
                  <Text style={[styles.text, { color: '#fff', paddingHorizontal: 10, fontWeight: 'normal'}]}>{vehicleData.data.attributes.kilometraje}</Text>
                ) } 
              </View>
          </View>
        </>
      )}
      </View>
    </LinearGradient>
 );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginVertical: 70,
    marginHorizontal: 40,
  },
  container: {
    flex: 1,
  },
  background: {
    flex: 1,
    width: '100%',
  },
  header: {
    flexDirection: 'row',
  },
  button: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 4,
    borderColor: '#0792b3',
    backgroundColor: '#0792b3',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    marginRight: 10
  },
  detailsContainer: {
    paddingTop: 20,
  },
  section: {
    display: 'flex',
    borderBottomWidth: 2,
    borderBottomColor: '#7b92b2',
    paddingBottom: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    minWidth: '85%',
    justifyContent: 'space-between',
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
    textAlign: 'center',
    color: '#fff',
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
  },
});

export default CarInfScreen;
