import React, { useState, useEffect, useRef } from 'react';
import { View, Text, StyleSheet, Animated, FlatList, Image, TouchableOpacity} from 'react-native';
import CheckBox from 'expo-checkbox';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import MapView, { Marker, Polyline } from 'react-native-maps';
import * as Location from 'expo-location';
import pinMarkerIcon from '../assets/pics/location-pin.png'
import locationMarkerIcon from '../assets/pics/location-pin2.png'
import CheckedIcon from '../assets/pics/checked.png'

import MapViewDirections from 'react-native-maps-directions';
import { ScrollView } from 'react-native-gesture-handler';

const MapScreen = ({ navigation, route }) => {
  //constantes
  const ip = '34.94.10.37';
  const { token } = route.params || { token: null };
  const [fadeAnim] = useState(new Animated.Value(0));
  //estados
  const [message, setMessage] = useState("");
  const [userData, setUserData] = useState(null);
  const [routeData, setRouteData] = useState(null);
  const [waypoints, setWaypoints] = useState([]);
  const [paradasTerminadas, setParadasTerminadas] = useState({});
  const [mapRegion, setMapRegion] = useState({latitude: 32.4566, longitude: -116.8273, latitudeDelta: 0.0500, longitudeDelta: 0.2105});
  const [paradasActivas, setParadasActivas] = useState([]);
  const[rutaFinalizada, setRutaFinalizada]  = useState(false);
  const [checkboxesDisabled, setCheckboxesDisabled] = useState(false);
  const handleParadasTerminadas = (item) => {
     setParadasTerminadas({
       ...paradasTerminadas,
       [item.id]: !paradasTerminadas[item.id],
     });
  };

  useEffect(() => {
    if (routeData && routeData.data && routeData.data.attributes && routeData.data.attributes.paradas && routeData.data.attributes.paradas.data) {
       const paradasActivasFiltradas = routeData.data.attributes.paradas.data.filter(parada => !paradasTerminadas[parada.id]);
       setParadasActivas(paradasActivasFiltradas);
    }
   }, [routeData, paradasTerminadas]);

  //verifica existencia del 
  useEffect(()=> {
    if (!token) {
      console.error('Token de autenticación no proporcionado');
      return;
    }
    userLocation();
  }, []) 

  const handleSubmit = async () => {
    try {
      const response = await fetch(`http://${ip}:1337/api/rutas/${userData.rutas[0].id}/?populate=*`, {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: {
            finalizado: true
          }
         }),
        redirect: "follow"
      });
      if (!response.ok) {
        setMessage("Ruta NO finalizada exitosamente!");
        console.log(response.status);
        throw new Error('Código de estado:');

      } else {
        const data = await response.json();
        setMessage("Ruta finalizada exitosamente!");
      }
      console.log({data: {
        finalizada: true
      }})

    } catch (error) {
      console.error('Error:', error);
      console.log(response.status);
    }
    setCheckboxesDisabled(true);
 };

  //pide permiso de acceso a la ubicacion y la almacena en la variable location
  const userLocation = async () => { 
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
       setErrorMsg('Permiso de acceso a la ubicación denegado.');  
       return;
    }
    try {
       let location = await Location.getCurrentPositionAsync({enableHighAccuracy: true });
       setMapRegion({
         latitude: location.coords.latitude,
         longitude: location.coords.longitude,
         latitudeDelta: 0.0500, 
         longitudeDelta: 0.2105
       }); 
       //console.log(mapRegion);
    } catch (error) {
       console.error('Error fetching user location:', error);
    }
  }

  //fetch de la info del usuario
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await fetch(`http://${ip}:1337/api/users/me?populate=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (response.ok) {
          const data = await response.json();
          setUserData(data);
          Animated.timing(
            fadeAnim,
            {
              toValue: 1,
              duration: 1000,
              useNativeDriver: true
            }
          ).start();
        } else {
          console.error('Error al obtener los datos del usuario');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchUserData();
 }, [route.params.token]);

 //fetch de la info de la ruta
useEffect(() => {
  const fetchRouteData = async () => {
    if (userData && userData.rutas && userData.rutas.length > 0) {
      try {
        const routeResponse = await fetch(`http://${ip}:1337/api/rutas/${userData.rutas[0].id}/?populate=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (routeResponse.ok) {
          const data = await routeResponse.json();
          setRouteData(data);
        } else {
          console.error('Error al obtener los datos de la ruta');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
  };
  fetchRouteData();
 }, [userData, token])

//obtener coordenadas de las paradas
 useEffect(() => {
  if (routeData && routeData.data && routeData.data.attributes.paradas) {
      const paradas = routeData.data.attributes.paradas.data; 
      //console.log(paradas);
      if (Array.isArray(paradas)) { 
        const waypoints = paradas.map(parada => ({
          latitude: parada.attributes.coordenadas.latitude,
          longitude: parada.attributes.coordenadas.longitude,
        }));
        setWaypoints(waypoints);
        //console.log({paradas: waypoints})

      }
  }
 }, [routeData]);

 useEffect(() => {
  const waypointsActuales = paradasActivas.map(parada => ({
     latitude: parada.attributes.coordenadas.latitude,
     longitude: parada.attributes.coordenadas.longitude,
  }));
  setWaypoints(waypointsActuales);
 }, [paradasActivas]);

 useEffect(() => {
  const totalParadas = routeData?.data?.attributes?.paradas?.data?.length || 0;
  const paradasTerminadasCount = Object.values(paradasTerminadas).filter(terminada => terminada).length;
 
  const todasParadasCompletadas = totalParadas === paradasTerminadasCount;
 
  if (todasParadasCompletadas) {
     setRutaFinalizada(true);
  }
  if (!todasParadasCompletadas) {
      setRutaFinalizada(false);
  }
 }, [routeData, paradasTerminadas]);
 
 
const GOOGLE_MAPS_APIKEY = 'AIzaSyCiSZByySVGT8RG6peRLzd9nyEMzyRfBE8';

  return (
    <LinearGradient colors={['#7b92b2', '#fff']} style={styles.background}>
      <View style={[styles.mainContainer, styles.container]}>
        <View style={styles.header}>
          <View style={[styles.button]}>
            <Ionicons name="map-outline" color={'#fff'} size={22} />
          </View>
          <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#fff'}} >Ruta actual</Text>
        </View>
        {routeData && (
          <View style={{marginVertical: 10, backgroundColor: '#7b92b2', padding: 15, borderRadius: 20}}>
            <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#fff'}}>ID de la ruta: <Text style={{fontWeight: 'normal'}}>{routeData.data.id}</Text></Text>
            {routeData.data.attributes.comentarios && (
              <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#fff'}}>Comentarios: <Text style={{fontWeight: 'normal'}}>{routeData.data.attributes.comentarios}</Text></Text>
            )}
          </View>
        )}
        {mapRegion.latitude && mapRegion.longitude && routeData && routeData.data && routeData.data.attributes && routeData.data.attributes.paradas && routeData.data.attributes.paradas.data && (
          <View>
            {(Array.isArray(waypoints) && waypoints.length > 0 && mapRegion) && (
              <MapView region={mapRegion} style={styles.map}>
                <Marker coordinate={mapRegion} title="Ubicación Actual" image={locationMarkerIcon}/>
                {routeData.data.attributes.paradas.data.map((parada, index) => (
                  <Marker
                    key={index}
                    coordinate={{
                      latitude: parada.attributes.coordenadas.latitude,
                      longitude: parada.attributes.coordenadas.longitude,
                    }}
                    title={parada.attributes.nombre}
                    image={paradasTerminadas[parada.id] ? CheckedIcon : pinMarkerIcon}
                  />
                ))}
                  <MapViewDirections
                    origin={mapRegion}
                    destination={waypoints[waypoints.length - 1]}
                    waypoints={waypoints.slice(0, -1)}
                    apikey='AIzaSyCiSZByySVGT8RG6peRLzd9nyEMzyRfBE8'
                    strokeWidth={3}
                    strokeColor='#0792b3' 
                  />
              </MapView>
            )}
            
            <View style={{marginVertical: 30, backgroundColor: '#7b92b2', padding: 15, borderRadius: 20}}>
              {routeData && routeData.data.attributes.paradas.data && (
                <FlatList
                style={{height:130}}
                  data={routeData.data.attributes.paradas.data} // Renderiza todas las paradas
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item }) => (
                    <View style={{display:'flex', justifyContent:'space-between', flexDirection: 'row', paddingHorizontal: 5}}>
                      <View style={{display: 'flex', flexDirection: 'row', marginBottom: 10, alignItems: 'center'}}>
                        <Image source={require('../assets/pics/location-pin-mini.png')}/>
                        <Text style={{fontSize: 18, color: paradasTerminadas[item.id] ? '#ccc' : '#fff', paddingLeft: 4 }}>{item.attributes.nombre}</Text>
                      </View>
                      <View>
                        {!checkboxesDisabled && (
                          <CheckBox
                            value={paradasTerminadas[item.id] || false}
                            onValueChange={() => handleParadasTerminadas(item)}
                            tintColors={{ true: '#0792b3', false: '#000' }} 
                          />
                        )}
                      </View>
                    </View>
                  )}
                />
              )}
              {rutaFinalizada && (
                <View style={{display: 'flex', width: '100%', alignItems: 'center'}}>
                  <TouchableOpacity onPress={handleSubmit} style={styles.logoutButton}>
                    <Text style={styles.logoutText}>Finalizar Ruta</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
            {(rutaFinalizada && message != "") && (
              <Text style={styles.logoutText}>{message}</Text>
            )}
          </View>
        )}
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginVertical: 70,
    marginHorizontal: 40,
  },
  header: {
    flexDirection: 'row',
  },
  routeInfoContainer: {
    marginVertical: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#7b92b2',
    paddingBottom: 10,
    marginBottom: 10,
  },
  subTitle: {
    color: '#0792b3',
  },
  map: {
    width: '100%',
    height: '60%',
    marginTop: 10,
  },
  button: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 4,
    borderColor: '#0792b3',
    backgroundColor: '#0792b3',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    marginRight: 10
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5,
    padding: 10,
  },
  background: {
    flex: 1,
    width: '100%',
  },
  titulo: {
    fontSize: 50,
    color: '#7b92b2', 
    fontWeight: 'bold',
    paddingHorizontal: 20,
  },
  subTitle: {
    fontSize: 20,
    color: 'gray',
    paddingBottom: 30
  },
  textInput: {
    padding: 10,
    paddingStart: 30,
    width: '80%',
    height: 50,
    marginTop: 10,
    borderRadius: 30,
    backgroundColor: '#fff',
  },
  errorMessage: {
    fontSize: 14,
    color: 'red',
    marginTop: 20,
  },
  logoutButton: {
    width: 200,
    backgroundColor: '#0792b3',
    paddingVertical: 10,
    borderRadius: 20,
    marginTop: 10,
    paddingHorizontal: 30,
    marginHorizontal: 2,
  },
  logoutText: {
    fontSize: 18,
    fontWeight: 'bold',
    color:'#fff',
    textAlign: 'center',
  },
});

export default MapScreen; 