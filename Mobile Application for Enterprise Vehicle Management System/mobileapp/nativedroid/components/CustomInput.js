import React from 'react';
import { View, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';

const CustomInput = ({ value, onChangeText, secureTextEntry, toggleSecureTextEntry }) => {
 return (
    <View style={styles.inputContainer}>
      <TextInput
        style={styles.textInput}
        placeholder="Ingresa tu contraseña"
        secureTextEntry={secureTextEntry}
        onChangeText={onChangeText}
        value={value}
      />
      <TouchableOpacity onPress={toggleSecureTextEntry} style={styles.iconContainer}>
        <Ionicons name={secureTextEntry ? "eye-off" : "eye"} size={24} color='#7b92b2'/>
      </TouchableOpacity>
    </View>
 );
};

const styles = StyleSheet.create({
 inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    padding: 10,
 },
   textInput: {
    padding: 10,
    paddingStart: 30,
    width: '80%',
    height: 50,
    marginTop: 10,
    borderRadius: 30,
    backgroundColor: '#fff',
  },
 iconContainer: {
    position: 'absolute',
    right: 25,
    paddingTop: 15
 },
});

export default CustomInput;
