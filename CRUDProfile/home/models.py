from django.db import models
from django.contrib.auth.models import User
from datetime import date
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=16, null=False, blank=False)
    age = models.IntegerField(default=0)
    weight = models.FloatField(default=1.0)
    status = models.BooleanField(default=True)
    timestamp = models.DateField(auto_now_add=True)
    date_update = models.DateField(auto_now=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.name

class Category(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=16)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    prodcut_name = models.CharField(max_length=16)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.prodcut_name
    
#Modelo Alumno
class Alumno(models.Model):
    matricula = models.CharField(max_length=10)
    nombre = models.CharField(max_length=100)
    apellidoP = models.CharField(max_length=100, default='No hay apellido')
    apellidoM = models.CharField(max_length=100, default='No hay apellido')
    telefono = models.CharField(max_length=20)
    fecha_nacimiento = models.DateField(default=date.today)

    def __str__(self):
        return self.nombre

#Modelo Profesor
class Profesor(models.Model):
    nombre = models.CharField(max_length=80)
    apellidoP = models.CharField(max_length=100, default='No hay apellido')
    apellidoM = models.CharField(max_length=100, default='No hay apellido')
    telefono = models.CharField(max_length=12)
    fecha_nacimiento = models.DateField()
    codigo = models.CharField(max_length=15)

    def __str__(self):
        return self.nombre

#Modelo Carrera 
class Carrera(models.Model):
    nombre = models.CharField(max_length=200)
    codigo = models.CharField(max_length=15)

    def __str__(self):
        return self.nombre