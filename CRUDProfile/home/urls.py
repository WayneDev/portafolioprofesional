from django.urls import path
from home import views

app_name = "home"

urlpatterns = [
    path('databaseindex/', views.DBindex.as_view(), name="databaseindex"),
    path('new/category/', views.NewCategory.as_view(), name="new_category"),
    path('delete/category/<int:pk>/', views.DeleteCategory.as_view(), name="delete_category"),
    path('update/category/<int:pk>/', views.UpdateCategory.as_view(), name="update_category"),
    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name="detail_category"),
    #Alumno
    path('alumnoindex/', views.AlumnoIndex.as_view(), name="alumnoindex"),
    path('delete/alumno/<str:pk>/', views.DeleteAlumno.as_view(), name="delete_alumno"),
    path('new/alumno/', views.NewAlumno.as_view(), name="new_alumno"),
    path('update/alumno/<str:pk>/', views.UpdateAlumno.as_view(), name="update_alumno"),
    path('detail/alumno/<str:str_pk>/', views.DetailAlumno.as_view(), name="detail_alumno"),
    path('dbv/', views.DatabaseView.as_view(), name='dbv'),
    path('', views.Index.as_view(), name='index'),
    #PaginaWeb
    path('home/', views.PaginaView.as_view(), name='paginav'),
]