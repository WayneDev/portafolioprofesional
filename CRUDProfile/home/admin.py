from django.contrib import admin
from .models import Profile, Category, Product, Alumno, Profesor, Carrera

# Register your models here.
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "user",
        "id",
        "timestamp",
        "status"
    ]

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "user",
        "id",
        "status"
    ]

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = [
        "prodcut_name",
        "category",
        "id",
        "status"
    ]

@admin.register(Alumno)
class AlumnoAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "apellidoP",
        "apellidoM",
        "telefono",
        "fecha_nacimiento",
        "matricula"
    ]

@admin.register(Profesor)
class ProfesorAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "apellidoP",
        "apellidoM",
        "telefono",
        "fecha_nacimiento",
        "codigo"

    ]

@admin.register(Carrera)
class CarreraAdmin(admin.ModelAdmin):
    list_display = [
        "nombre",
        "codigo"
    ]
