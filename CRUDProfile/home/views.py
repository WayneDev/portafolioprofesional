from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views import generic
from django.urls import reverse_lazy
from .models import Category, Alumno
from .forms import UpdateCategoryForm, NewCategoryForm, NewAlumnoForm, UpdateAlumnoForm

# Create your views here.

# def index(request):
#     return render(request, "home/index.html", {})

#Plantilla para Category
class DetailCategory(generic.View):
    template_name = "home/detail_category.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "category": Category.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)

class UpdateCategory(generic.UpdateView):
    template_name = "home/update_category.html"
    model = Category
    form_class = UpdateCategoryForm
    success_url = reverse_lazy("home:databaseindex")


class DeleteCategory(generic.DeleteView):
    template_name = "home/delete_category.html"
    model = Category
    success_url = reverse_lazy("home:databaseindex")


class NewCategory(generic.CreateView):
    template_name = "home/new_category.html"
    model = Category
    form_class = NewCategoryForm
    success_url = reverse_lazy("home:databaseindex")

class DBindex(generic.View):
    template_name = "home/databaseindex.html"
    context = {}

    def get(self, request):
        self.context = {
            "categories": Category.objects.all()
        }
        return render(request, self.template_name, self.context)

#Plantilla para alumno
class DetailAlumno(generic.View):
    template_name = "home/detail_alumno.html"
    context = {}

    def get(self, request, str_pk):
        self.context = {
            "alumno": get_object_or_404(Alumno, matricula=str_pk)
        }
        return render(request, self.template_name, self.context)

class DeleteAlumno(generic.DeleteView):
    template_name = "home/delete_alumno.html"
    model = Alumno
    success_url = reverse_lazy("home:alumnoindex")

    def get_object(self, queryset=None):
        return get_object_or_404(Alumno, matricula=self.kwargs['pk'])
    
class NewAlumno(generic.CreateView):
    template_name = "home/new_alumno.html"
    model = Alumno
    form_class = NewAlumnoForm
    success_url = reverse_lazy("home:alumnoindex")

class UpdateAlumno(generic.UpdateView):
    template_name = "home/update_alumno.html"
    model = Alumno
    form_class = UpdateAlumnoForm
    success_url = reverse_lazy("home:alumnoindex")

    def get_object(self, queryset=None):
        return get_object_or_404(Alumno, matricula=self.kwargs['pk'])

class AlumnoIndex(generic.View):
    template_name = "home/alumnoindex.html"
    context = {}

    def get(self, request):
        self.context = {
            "alumnos": Alumno.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class DatabaseView(generic.View):
    template_name = "base/dbv.html"
    context = {}

    def get(self,request):
        return render(request, self.template_name,self.context)
    
class Index(generic.View):
    template_name = "base/lobby.html"
    context = {}

    def get(self,request):
        return render(request, self.template_name,self.context)
    
#PaginaView
class PaginaView(generic.View):
    template_name = "home/paginaweb.html"
    context = {}

    def get(self,request):
        return render(request, self.template_name,self.context)
    