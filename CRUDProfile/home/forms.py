from django import forms
from .models import Category, Alumno

class UpdateCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "user",
            "name",
            "status"
        ]

class NewCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            "user",
            "name",
            "status"
        ]

#Alumno
class NewAlumnoForm(forms.ModelForm):
    class Meta:
        model = Alumno
        fields = [
            "nombre",
            "apellidoP",
            "apellidoM",
            "telefono",
            "fecha_nacimiento",
            "matricula"
        ]

class UpdateAlumnoForm(forms.ModelForm):
    class Meta:
        model = Alumno
        fields = [
            "nombre",
            "apellidoP",
            "apellidoM",
            "telefono",
            "fecha_nacimiento",
            "matricula"
        ]