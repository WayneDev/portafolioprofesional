from typing import Any
from django import http
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http.response import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.views import generic, View
from django.views.generic import ListView
from django.urls import reverse_lazy
from home.models import *
from home.forms import *
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
import json
from django.db.models import Count
from datetime import datetime, date
from django.db.models import Max


# #DRF
# from rest_framework.decorators import api_view
# from rest_framework.response import Response
# from rest_framework import status
# from rest_framework.authtoken.models import Token
# from django.contrib.auth.models import User
#-------------------------------------------------------------------------------

# Create your views here.

#APIs TiposVehiculos

# class TiposVehiculosView(View):

#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#             return super().dispatch(request, *args, **kwargs)  
    
#     def get(self, request, pk=0):
#         tiposvehiculos = []
#         if (pk > 0):
#             tiposvehiculos = list(tiposVehiculos.objects.filter(num=pk).values())
#             if len(tiposvehiculos) > 0:
#                 tipovehiculo = tiposvehiculos[0]
#                 datos = {'message': "Succes", 'tipovehiculo': tipovehiculo}
#             else:
#                 datos = {'message': "tipovehiculo not found..."}
#             return JsonResponse(datos)
#         else:
#             tiposvehiculos = list(tiposVehiculos.objects.values())
#             if len(tiposvehiculos) > 0:
#                 datos = {'message': "Succes", 'tiposvehiculos': tiposvehiculos}
#             else:
#                 datos = {'message': "tiposvehiculos not found..."}
#             return JsonResponse(datos)
        
#     def post(self, request):
#         # print(request.body)
#         jd = json.loads(request.body)
#         # print(jd)
#         tiposVehiculos.objects.create(nombre=jd['nombre'])
#         datos = {'message':"Succes"}
#         return JsonResponse(datos)
    
#     def put(self, request, pk=0):
#         tiposvehiculos = []
#         jd = json.loads(request.body, strict=False)
#         tiposvehiculos = list(tiposVehiculos.objects.filter(num=pk).values())
#         if len(tiposvehiculos) > 0:
#             tipovehiculo = tiposVehiculos.objects.get(num=pk)
#             tipovehiculo.nombre = jd['nombre']
#             tipovehiculo.save()
#             datos = {'message':"Succes"}
#             return JsonResponse(datos)
#         else:
#             datos = {'message': "tiposvehiculos not found..."}
#             return JsonResponse(datos)

#     def delete(self, request, pk=0):
#         tiposvehiculos = list(tiposVehiculos.objects.filter(num=pk).values())
#         if len(tiposvehiculos) > 0:
#             tiposVehiculos.objects.filter(num=pk).delete()
#             datos = {'message':"Succes"}
#         else:
#             datos = {'message': "tiposvehiculos not found..."}
#         return JsonResponse(datos)

class TiposVehiculosView(View):
    # Se deshabilita la protección CSRF para simplificar ejemplos. En producción, es recomendable habilitar CSRF.
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        tiposvehiculos = []

        # Obtener información de un tipo de vehículo por su ID
        if pk > 0:
            tiposvehiculos = list(tiposVehiculos.objects.filter(num=pk).values())
            if len(tiposvehiculos) > 0:
                tipovehiculo = tiposvehiculos[0]
                # Agregar la cuenta de vehículos asociados al tipo
                tipovehiculo['vehiculos_count'] = self.get_vehiculos_count(pk)
                datos = {'message': "Success", 'tipovehiculo': tipovehiculo}
            else:
                datos = {'message': "tipovehiculo not found..."}
            return JsonResponse(datos)
        else:
            # Obtener información de todos los tipos de vehículos
            tiposvehiculos = list(tiposVehiculos.objects.values())
            for tipovehiculo in tiposvehiculos:
                # Agregar la cuenta de vehículos asociados a cada tipo
                tipovehiculo['vehiculos_count'] = self.get_vehiculos_count(tipovehiculo['num'])
            
            if len(tiposvehiculos) > 0:
                datos = {'message': "Success", 'tiposvehiculos': tiposvehiculos}
            else:
                datos = {'message': "tiposvehiculos not found..."}
            return JsonResponse(datos)

    def post(self, request):
        # Crear un nuevo tipo de vehículo con la información proporcionada en el cuerpo de la solicitud
        jd = json.loads(request.body)
        tiposVehiculos.objects.create(nombre=jd['nombre'])
        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        tiposvehiculos = []
        jd = json.loads(request.body, strict=False)
        tiposvehiculos = list(tiposVehiculos.objects.filter(num=pk).values())
        
        # Actualizar la información de un tipo de vehículo por su ID
        if len(tiposvehiculos) > 0:
            tipovehiculo = tiposVehiculos.objects.get(num=pk)
            tipovehiculo.nombre = jd['nombre']
            tipovehiculo.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "tiposvehiculos not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        tiposvehiculos = list(tiposVehiculos.objects.filter(num=pk).values())
        
        # Eliminar un tipo de vehículo por su ID
        if len(tiposvehiculos) > 0:
            tiposVehiculos.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "tiposvehiculos not found..."}
        return JsonResponse(datos)

    def get_vehiculos_count(self, tipo_id):
        # Obtener la cantidad de vehículos asociados a un tipo
        return vehiculos.objects.filter(tipo=tipo_id).count()
#//////////////////////////////////////////////////////////////////////////////////////////////////////

# #APIs MarcaVehiculo

# class MarcasVehiculosView(View):

#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=0):
#         marcasvehiculos = []
#         if (pk > 0):
#             marcasvehiculos = list(marcasVehiculos.objects.filter(num=pk).values())
#             if len(marcasvehiculos) > 0:
#                 marcavehiculo = marcasvehiculos[0]
#                 datos = {'message': "Succes", 'MarcaVehiculo': marcavehiculo}
#             else:
#                 datos = {'message': "MarcaVehiculo not found..."}
#             return JsonResponse(datos)
#         else:
#             marcasvehiculos = list(marcasVehiculos.objects.values())
#             if len(marcasvehiculos) > 0:
#                 datos = {'message': "Succes", 'MarcasVehiculos': marcasvehiculos}
#             else:
#                 datos = {'message': "MarcasVehiculos not found..."}
#             return JsonResponse(datos)

class MarcasVehiculosView(View):
    # Deshabilitar la protección CSRF para simplificar ejemplos. En producción, es recomendable habilitar CSRF.
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        marcasvehiculos = []

        # Obtener información de una marca de vehículo por su ID
        if pk > 0:
            marca = marcasVehiculos.objects.filter(num=pk).first()
            if marca:
                # Contar la cantidad de vehículos asociados a la marca
                vehiculos_count = vehiculos.objects.filter(marca=marca).count()
                marcavehiculo = {'num': marca.num, 'nombre': marca.nombre, 'vehiculos_count': vehiculos_count}
                datos = {'message': "Success", 'MarcaVehiculo': marcavehiculo}
            else:
                datos = {'message': "MarcaVehiculo not found..."}
            return JsonResponse(datos)
        else:
            # Obtener información de todas las marcas de vehículos
            marcasvehiculos = marcasVehiculos.objects.annotate(vehiculos_count=Count('vehiculos')).values('num', 'nombre', 'vehiculos_count')
            if len(marcasvehiculos) > 0:
                datos = {'message': "Success", 'MarcasVehiculos': list(marcasvehiculos)}
            else:
                datos = {'message': "MarcasVehiculos not found..."}
            return JsonResponse(datos)

    def post(self, request):
        # Crear una nueva marca de vehículo con la información proporcionada en el cuerpo de la solicitud
        jd = json.loads(request.body)
        marcasVehiculos.objects.create(nombre=jd['nombre'])
        datos = {'message':"Success"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        marcasvehiculos = []
        jd = json.loads(request.body)
        marcasvehiculos = list(marcasVehiculos.objects.filter(num=pk).values())
        
        # Actualizar la información de una marca de vehículo por su ID
        if len(marcasvehiculos) > 0:
            marcavehiculo = marcasVehiculos.objects.get(num=pk)
            marcavehiculo.nombre = jd['nombre']
            marcavehiculo.save()
            datos = {'message':"Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "MarcasVehiculos not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        marcasvehiculos = list(marcasVehiculos.objects.filter(num=pk).values())
        
        # Eliminar una marca de vehículo por su ID
        if len(marcasvehiculos) > 0:
            marcasVehiculos.objects.filter(num=pk).delete()
            datos = {'message':"Success"}
        else:
            datos = {'message': "MarcasVehiculos not found..."}
        return JsonResponse(datos)
#//////////////////////////////////////////////////////////////////////////////////////////////////////

#APIs MODELOSVEHICULOS

# class ModelosVehiculosView(View):
    
#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=0):
#         modelosvehiculos = []
#         if (pk > 0):
#             modelosvehiculos = list(modelosVehiculos.objects.filter(num=pk).values())
#             if len(modelosvehiculos) > 0:
#                 modelovehiculo = modelosvehiculos[0]
#                 datos = {'message': "Succes", 'ModeloVehiculo': modelovehiculo}
#             else:
#                 datos = {'message': "ModeloVehiculo not found..."}
#             return JsonResponse(datos)
#         else:
#             modelosvehiculos = list(modelosVehiculos.objects.values())
#             if len(modelosvehiculos) > 0:
#                 datos = {'message': "Succes", 'ModeloVehiculos': modelosvehiculos}
#             else:
#                 datos = {'message': "ModelosVehiculos not found..."}
#             return JsonResponse(datos)

class ModelosVehiculosView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        modelosvehiculos = []
        if pk > 0:
            modelo = modelosVehiculos.objects.filter(num=pk).first()
            if modelo:
                vehiculos_count = vehiculos.objects.filter(modelo=modelo).count()
                modelovehiculo = {'num': modelo.num, 'nombre': modelo.nombre, 'consumoConbustible': modelo.consumoConbustible, 'cantCarga': modelo.cantCarga, 'capPasajero': modelo.capPasajero, 'vehiculos_count': vehiculos_count}
                datos = {'message': "Success", 'ModeloVehiculo': modelovehiculo}
            else:
                datos = {'message': "ModeloVehiculo not found..."}
            return JsonResponse(datos)
        else:
            modelosvehiculos = modelosVehiculos.objects.annotate(vehiculos_count=Count('vehiculos')).values('num', 'nombre', 'consumoConbustible', 'cantCarga', 'capPasajero', 'vehiculos_count')
            if len(modelosvehiculos) > 0:
                datos = {'message': "Success", 'ModelosVehiculos': list(modelosvehiculos)}
            else:
                datos = {'message': "ModelosVehiculos not found..."}
            return JsonResponse(datos)

    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        modelosVehiculos.objects.create(nombre=jd['nombre'], consumoConbustible=jd['consumoConbustible'], cantCarga=jd['cantCarga'], capPasajero=jd['capPasajero'])
        datos = {'message':"Succes"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        modelosvehiculos = []
        jd = json.loads(request.body)
        modelosvehiculos = list(modelosVehiculos.objects.filter(num=pk).values())
        if len(modelosvehiculos) > 0:
            modelovehiculo = modelosVehiculos.objects.get(num=pk)
            modelovehiculo.nombre = (jd['nombre'])
            modelovehiculo.consumoConbustible = (jd['consumoConbustible'])
            modelovehiculo.cantCarga = (jd['cantCarga'])
            modelovehiculo.capPasajero = (jd['capPasajero'])
            modelovehiculo.save()
            datos = {'message':"Succes"}
            return JsonResponse(datos)
        else:
            datos = {'message': "ModeloVehiculo not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        modelosvehiculos = list(modelosVehiculos.objects.filter(num=pk).values())
        if len(modelosvehiculos) > 0:
            modelosVehiculos.objects.filter(num=pk).delete()
            datos = {'message':"Succes"}
        else:
            datos = {'message': "ModeloVehiculo not found..."}
        return JsonResponse(datos)

#//////////////////////////////////////////////////////////////////////////////////////////////////////

#APIs VEHICULO

# class VehiculosView(View):
    
#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=0):
#         vehiculosview = []
#         if (pk > 0):
#             vehiculosview = list(vehiculos.objects.filter(num=pk).values())
#             if len(vehiculosview) > 0:
#                 vehiculo = vehiculosview[0]
#                 datos = {'message': "Succes", 'Vehiculo': vehiculo}
#             else:
#                 datos = {'message': "Vehiculo not found..."}
#             return JsonResponse(datos)
#         else:
#             vehiculosview = list(vehiculos.objects.values())
#             if len(vehiculosview) > 0:
#                 datos = {'message': "Succes", 'Vehiculos': vehiculosview}
#             else:
#                 datos = {'message': "Vehiculos not found..."}
#             return JsonResponse(datos)

class VehiculosView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, num_serie=None):
        vehiculos_data = []
        if num_serie:
            vehiculo = vehiculos.objects.filter(numSerie=num_serie).values(
                'numSerie', 'placas', 'kilometraje', 'fechaAdquisicion', 'usaDiesel', 'modelo__nombre', 'marca__nombre', 'tipo__nombre'
            ).first()
            if vehiculo:
                vehiculo['fechaAdquisicion'] = vehiculo['fechaAdquisicion'].strftime('%d/%m/%Y')
                datos = {'message': "Success", 'Vehiculo': vehiculo}
            else:
                datos = {'message': "Vehiculo not found..."}
        else:
            vehiculos_data = vehiculos.objects.values(
                'numSerie', 'placas', 'kilometraje', 'fechaAdquisicion', 'usaDiesel', 'modelo__nombre', 'marca__nombre', 'tipo__nombre'
            )
            for vehiculo in vehiculos_data:
                vehiculo['fechaAdquisicion'] = vehiculo['fechaAdquisicion'].strftime('%d/%m/%Y')
            datos = {'message': "Success", 'Vehiculos': list(vehiculos_data)}

        return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)

        # Obtener instancias de modelosVehiculos, marcasVehiculos y tiposVehiculos
        modelo_instance = modelosVehiculos.objects.get(pk=jd['modelo'])
        marca_instance = marcasVehiculos.objects.get(pk=jd['marca'])
        tipo_instance = tiposVehiculos.objects.get(pk=jd['tipo'])

        # Crear una instancia de vehiculos con las instancias obtenidas
        vehiculo = vehiculos.objects.create(
            numSerie=jd['numSerie'],
            placas=jd['placas'],
            kilometraje=jd['kilometraje'],
            fechaAdquisicion=jd['fechaAdquisicion'],
            usaDiesel=jd['usaDiesel'],
            modelo=modelo_instance,
            marca=marca_instance,
            tipo=tipo_instance
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, num_serie=None):
            jd = json.loads(request.body)
            
            try:
                vehiculo = vehiculos.objects.get(numSerie=num_serie)
            except vehiculos.DoesNotExist:
                datos = {'message': "Vehicle not found..."}
                return JsonResponse(datos, status=404)

            vehiculo.placas = jd['placas']
            vehiculo.kilometraje = jd['kilometraje']
            vehiculo.fechaAdquisicion = jd['fechaAdquisicion']
            vehiculo.usaDiesel = jd['usaDiesel']

            # Obtén instancias de modelosVehiculos, marcasVehiculos, y tiposVehiculos
            try:
                modelo_instance = modelosVehiculos.objects.get(num=jd['modelo'])
                marca_instance = marcasVehiculos.objects.get(num=jd['marca'])
                tipo_instance = tiposVehiculos.objects.get(num=jd['tipo'])
            except modelosVehiculos.DoesNotExist:
                datos = {'message': "Modelo not found..."}
                return JsonResponse(datos, status=404)
            except marcasVehiculos.DoesNotExist:
                datos = {'message': "Marca not found..."}
                return JsonResponse(datos, status=404)
            except tiposVehiculos.DoesNotExist:
                datos = {'message': "Tipo not found..."}
                return JsonResponse(datos, status=404)

            vehiculo.modelo = modelo_instance
            vehiculo.marca = marca_instance
            vehiculo.tipo = tipo_instance

            vehiculo.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)

    def delete(self, request, num_serie=None):  # Ajusta el parámetro a num_serie
        vehiculosview = list(vehiculos.objects.filter(numSerie=num_serie).values())  # Ajusta la consulta a numSerie
        if len(vehiculosview) > 0:
            vehiculos.objects.filter(numSerie=num_serie).delete()  # Ajusta la consulta a numSerie
            datos = {'message':"Succes"}
        else:
            datos = {'message': "Vehiculo not found..."}
        return JsonResponse(datos)
    
class VehiculosCountView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        total_count = vehiculos.objects.aggregate(total_count=Count('numSerie'))
        datos = {'message': 'Success', 'total_count': total_count['total_count']}
        return JsonResponse(datos)
#//////////////////////////////////////////////////////////////////////////////////////////////////////

#PRUEBAS
# #APIs LICENCIAS

# class LicenciasView(View):

#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=0):
#         licenciasview = []
#         if (pk > 0):
#             licenciasview = list(licencias.objects.filter(num=pk).values())
#             if len(licenciasview) > 0:
#                 liciencia = licenciasview[0]
#                 datos = {'message': "Succes", 'liciencia': liciencia}
#             else:
#                 datos = {'message': "liciencia not found..."}
#             return JsonResponse(datos)
#         else:
#             licenciasview = list(licencias.objects.values())
#             if len(licenciasview) > 0:
#                 datos = {'message': "Succes", 'licencias': licenciasview}
#             else:
#                 datos = {'message': "licencias not found..."}
#             return JsonResponse(datos)

#     def post(self, request):
#         jd = json.loads(request.body)

#         conductor_instance = usuarios.objects.get(pk=jd['conductor'])
#         tipo_instance = tipoLicencias.objects.get(pk=jd['tipo'])

#         # Crear una instancia de vehiculos con las instancias obtenidas
#         liciencia = licencias.objects.create(
#             fechaExpedicion=jd['fechaExpedicion'],
#             fechaVencimiento=jd['fechaVencimiento'],
#             comentarios=jd['comentarios'],
#             conductor=conductor_instance,
#             tipo=tipo_instance
#         )

#         datos = {'message': "Success"}
#         return JsonResponse(datos)

#     def put(self, request, pk=0):
#         licenciasview = []
#         jd = json.loads(request.body)
#         licenciasview = list(licencias.objects.filter(num=pk).values())
        
#         if len(licenciasview) > 0:
#             licencia = licencias.objects.get(num=pk)
#             licencia.fechaExpedicion = jd['fechaExpedicion']
#             licencia.fechaVencimiento = jd['fechaVencimiento']
#             licencia.comentarios = jd['comentarios']
            
#             # Obtén instancias de modelosVehiculos, marcasVehiculos, y tiposVehiculos
#             try:
#                 conductor_instance = usuarios.objects.get(num=jd['conductor'])
#                 tipo_instance = tipoLicencias.objects.get(num=jd['tipo'])
#             except usuarios.DoesNotExist:
#                 datos = {'message': "Usuario not found..."}
#                 return JsonResponse(datos, status=404)
#             except tipoLicencias.DoesNotExist:
#                 datos = {'message': "Tipo not found..."}
#                 return JsonResponse(datos, status=404)

#             licencia.conductor = conductor_instance
#             licencia.tipo = tipo_instance

#             licencia.save()
#             datos = {'message': "Success"}
#             return JsonResponse(datos)
#         else:
#             datos = {'message': "licencia not found..."}
#             return JsonResponse(datos, status=404)

#     def delete(self, request, pk=0):
#         licenciasview = list(licencias.objects.filter(num=pk).values())
#         if len(licenciasview) > 0:
#             licencias.objects.filter(num=pk).delete()
#             datos = {'message':"Succes"}
#         else:
#             datos = {'message': "Vehiculo not found..."}
#         return JsonResponse(datos)

class LicenciasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        licenciasview = []

        if pk > 0:
            licencia = licencias.objects.filter(num=pk).first()
            if licencia:
                conductor = licencia.conductor
                conductor_info = {
                    'id': conductor.num,
                    'nombre': f"{conductor.nombrePila} {conductor.apPaterno} {conductor.apMaterno}",
                }

                
                # Accede al número de serie del vehículo a través de la relación VehiculosUsuarios

                licencia_info = {
                    'numLicencia': licencia.num,
                    'nombreLicencia': licencia.tipo.nombre,
                    'fechaExpedicion': self.format_date(licencia.fechaExpedicion),
                    'fechaVencimiento': self.format_date(licencia.fechaVencimiento),
                    'comentarios': licencia.comentarios,
                    'conductor': conductor_info,
                }
                datos = {'message': "Success", 'licencia': licencia_info}
            else:
                datos = {'message': "Licencia not found..."}
            return JsonResponse(datos)
        
        else:
            licenciasview = licencias.objects.values(
                'num',
                'tipo__nombre',
                'conductor__vehiculosusuarios__vehiculo__numSerie',
                'fechaExpedicion',
                'fechaVencimiento',
                'comentarios',
                'conductor',
            ).order_by('num')

            licencias_data = []
            for licencia in licenciasview:
                conductor = usuarios.objects.filter(num=licencia['conductor']).first()
                conductor_info = {
                    'id': conductor.num,
                    'nombre': f"{conductor.nombrePila} {conductor.apPaterno} {conductor.apMaterno}",
                }
                licencia_info = {
                    'numLicencia': licencia['num'],
                    'numSerie': licencia['conductor__vehiculosusuarios__vehiculo__numSerie'],
                    'nombreLicencia': licencia['tipo__nombre'],
                    'fechaExpedicion': self.format_date(licencia['fechaExpedicion']),
                    'fechaVencimiento': self.format_date(licencia['fechaVencimiento']),
                    'comentarios': licencia['comentarios'],
                    'conductor': conductor_info,
                }
                licencias_data.append(licencia_info)

            if licencias_data:
                datos = {'message': "Success", 'licencias': licencias_data}
            else:
                datos = {'message': "Licencias not found..."}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)

        try:
            conductor_instance = usuarios.objects.get(pk=jd['conductor'])
            tipo_instance = tipoLicencias.objects.get(pk=jd['tipo'])
        except (usuarios.DoesNotExist, tipoLicencias.DoesNotExist):
            datos = {'message': "Usuario or Tipo not found..."}
            return JsonResponse(datos, status=404)

        # Formatear fechas
        fecha_expedicion = self.parse_date(jd['fechaExpedicion'])
        fecha_vencimiento = self.parse_date(jd['fechaVencimiento'])

        licencia = licencias.objects.create(
            fechaExpedicion=fecha_expedicion,
            fechaVencimiento=fecha_vencimiento,
            comentarios=jd['comentarios'],
            conductor=conductor_instance,
            tipo=tipo_instance
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        licenciasview = []
        jd = json.loads(request.body)
        licenciasview = list(licencias.objects.filter(num=pk).values())

        if len(licenciasview) > 0:
            licencia = licencias.objects.get(num=pk)

            # Formatear fechas
            fecha_expedicion = self.parse_date(jd['fechaExpedicion'])
            fecha_vencimiento = self.parse_date(jd['fechaVencimiento'])

            licencia.fechaExpedicion = fecha_expedicion
            licencia.fechaVencimiento = fecha_vencimiento
            licencia.comentarios = jd['comentarios']

            try:
                conductor_instance = usuarios.objects.get(pk=jd['conductor'])
                tipo_instance = tipoLicencias.objects.get(pk=jd['tipo'])
            except (usuarios.DoesNotExist, tipoLicencias.DoesNotExist):
                datos = {'message': "Usuario or Tipo not found..."}
                return JsonResponse(datos, status=404)

            licencia.conductor = conductor_instance
            licencia.tipo = tipo_instance

            licencia.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Licencia not found..."}
            return JsonResponse(datos, status=404)

    def delete(self, request, pk=0):
        licenciasview = list(licencias.objects.filter(num=pk).values())
        if len(licenciasview) > 0:
            licencias.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Licencia not found..."}
        return JsonResponse(datos)

    def format_date(self, date_str):
        if date_str == '0000-00-00':
            return 'Sin fecha'
        elif isinstance(date_str, date):
            # Si ya es un objeto datetime.date, no necesitas hacer nada
            return date_str.strftime('%d/%m/%Y')
        else:
            # Si es una cadena, conviértela a objeto datetime.date y luego formatea
            date_obj = datetime.strptime(str(date_str), '%Y-%m-%d').date()
            return date_obj.strftime('%d/%m/%Y')

    def parse_date(self, date_str):
        date_obj = datetime.strptime(date_str, '%d/%m/%Y').date()
        return date_obj

#//////////////////////////////////////////////////////////////////////////////////////////////////////

#APIs TIPOSLICENCIAS

class TiposLicenciasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)  
    
    def get(self, request, pk=0):
        tiposlicencias = []
        if (pk > 0):
            tiposlicencias = list(tipoLicencias.objects.filter(num=pk).values())
            if len(tiposlicencias) > 0:
                tipolicencia = tiposlicencias[0]
                datos = {'message': "Succes", 'tipolicencia': tipolicencia}
            else:
                datos = {'message': "tipolicencia not found..."}
            return JsonResponse(datos)
        else:
            tiposlicencias = list(tipoLicencias.objects.values())
            if len(tiposlicencias) > 0:
                datos = {'message': "Succes", 'tiposlicencias': tiposlicencias}
            else:
                datos = {'message': "tiposlicencias not found..."}
            return JsonResponse(datos)
        
    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        tipoLicencias.objects.create(nombre=jd['nombre'])
        datos = {'message':"Succes"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        tiposlicencias = []
        jd = json.loads(request.body, strict=False)
        tiposlicencias = list(tipoLicencias.objects.filter(num=pk).values())
        if len(tiposlicencias) > 0:
            tipolicencia = tipoLicencias.objects.get(num=pk)
            tipolicencia.nombre = jd['nombre']
            tipolicencia.save()
            datos = {'message':"Succes"}
            return JsonResponse(datos)
        else:
            datos = {'message': "tipolicencia not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        tiposlicencias = list(tipoLicencias.objects.filter(num=pk).values())
        if len(tiposlicencias) > 0:
            tipoLicencias.objects.filter(num=pk).delete()
            datos = {'message':"Succes"}
        else:
            datos = {'message': "tiposlicencias not found..."}
        return JsonResponse(datos)
#//////////////////////////////////////////////////////////////////////////////////////////////////////



#//////////////////////////////////////////////////////////////////////////////////////////////////////

class RolesView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)  
    
    def get(self, request, pk=0):
        rolesview = []
        if (pk > 0):
            rolesview = list(roles.objects.filter(num=pk).values())
            if len(rolesview) > 0:
                rol = rolesview[0]
                datos = {'message': "Succes", 'rol': rol}
            else:
                datos = {'message': "rol not found..."}
            return JsonResponse(datos)
        else:
            rolesview = list(roles.objects.values())
            if len(rolesview) > 0:
                datos = {'message': "Succes", 'rolesview': rolesview}
            else:
                datos = {'message': "rolesview not found..."}
            return JsonResponse(datos)
        
    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        roles.objects.create(nombre=jd['nombre'])
        datos = {'message':"Succes"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        rolesview = []
        jd = json.loads(request.body, strict=False)
        rolesview = list(roles.objects.filter(num=pk).values())
        if len(rolesview) > 0:
            rol = roles.objects.get(num=pk)
            rol.nombre = jd['nombre']
            rol.save()
            datos = {'message':"Succes"}
            return JsonResponse(datos)
        else:
            datos = {'message': "rol not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        tiposlicencias = list(roles.objects.filter(num=pk).values())
        if len(tiposlicencias) > 0:
            roles.objects.filter(num=pk).delete()
            datos = {'message':"Succes"}
        else:
            datos = {'message': "rol not found..."}
        return JsonResponse(datos)

class ParadasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        paradas_view = []
        if pk > 0:
            paradas_view = list(paradas.objects.filter(num=pk).values())
            if len(paradas_view) > 0:
                parada = paradas_view[0]
                datos = {'message': "Success", 'parada': parada}
            else:
                datos = {'message': "Parada not found..."}
            return JsonResponse(datos)
        else:
            paradas_view = list(paradas.objects.values())
            if len(paradas_view) > 0:
                datos = {'message': "Success", 'paradas_view': paradas_view}
            else:
                datos = {'message': "Paradas not found..."}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)
        paradas.objects.create(
            nombre=jd['nombre'],
            dirColonia=jd['dirColonia'],
            dirCalle=jd['dirCalle'],
            dirNumero=jd['dirNumero']
        )
        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        paradas_view = []
        jd = json.loads(request.body, strict=False)
        paradas_view = list(paradas.objects.filter(num=pk).values())
        if len(paradas_view) > 0:
            parada = paradas.objects.get(num=pk)
            parada.nombre = jd['nombre']
            parada.dirColonia = jd['dirColonia']
            parada.dirCalle = jd['dirCalle']
            parada.dirNumero = jd['dirNumero']
            parada.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Parada not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        paradas_view = list(paradas.objects.filter(num=pk).values())
        if len(paradas_view) > 0:
            paradas.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Parada not found..."}
        return JsonResponse(datos)

#SOLITARIOS:
class TiposCoberturaView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        tipos_cobertura = []
        if pk > 0:
            tipos_cobertura = list(tiposCoberturas.objects.filter(num=pk).values())
            if len(tipos_cobertura) > 0:
                tipo_cobertura = tipos_cobertura[0]
                datos = {'message': "Success", 'tipo_cobertura': tipo_cobertura}
            else:
                datos = {'message': "Tipo de cobertura no encontrado..."}
            return JsonResponse(datos)
        else:
            tipos_cobertura = list(tiposCoberturas.objects.values())
            if len(tipos_cobertura) > 0:
                datos = {'message': "Success", 'tipos_cobertura': tipos_cobertura}
            else:
                datos = {'message': "Tipos de cobertura no encontrados..."}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)
        tiposCoberturas.objects.create(
            nombre=jd['nombre']
        )
        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        tipos_cobertura = []
        jd = json.loads(request.body, strict=False)
        tipos_cobertura = list(tiposCoberturas.objects.filter(num=pk).values())
        if len(tipos_cobertura) > 0:
            tipo_cobertura = tiposCoberturas.objects.get(num=pk)
            tipo_cobertura.nombre = jd['nombre']
            tipo_cobertura.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Tipo de cobertura no encontrado..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        tipos_cobertura = list(tiposCoberturas.objects.filter(num=pk).values())
        if len(tipos_cobertura) > 0:
            tiposCoberturas.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Tipo de cobertura no encontrado..."}
        return JsonResponse(datos)

class TalleresView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        talleres_view = []
        if pk > 0:
            talleres_view = list(talleres.objects.filter(num=pk).values())
            if len(talleres_view) > 0:
                taller = talleres_view[0]
                datos = {'message': "Success", 'taller': taller}
            else:
                datos = {'message': "Taller no encontrado..."}
            return JsonResponse(datos)
        else:
            talleres_view = list(talleres.objects.values())
            if len(talleres_view) > 0:
                datos = {'message': "Success", 'talleres_view': talleres_view}
            else:
                datos = {'message': "Talleres no encontrados..."}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)
        talleres.objects.create(
            nombre=jd['nombre'],
            correo=jd['correo'],
            numTel=jd['numTel'],
            dirCalle=jd['dirCalle'],
            dirNumero=jd['dirNumero'],
            dirColonia=jd['dirColonia']
        )
        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        talleres_view = []
        jd = json.loads(request.body, strict=False)
        talleres_view = list(talleres.objects.filter(num=pk).values())
        if len(talleres_view) > 0:
            taller = talleres.objects.get(num=pk)
            taller.nombre = jd['nombre']
            taller.correo = jd['correo']
            taller.numTel = jd['numTel']
            taller.dirCalle = jd['dirCalle']
            taller.dirNumero = jd['dirNumero']
            taller.dirColonia = jd['dirColonia']
            taller.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Taller no encontrado..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        talleres_view = list(talleres.objects.filter(num=pk).values())
        if len(talleres_view) > 0:
            talleres.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Taller no encontrado..."}
        return JsonResponse(datos)

class AseguradorasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        aseguradoras_view = []
        if pk > 0:
            aseguradoras_view = list(aseguradoras.objects.filter(num=pk).values())
            if len(aseguradoras_view) > 0:
                aseguradora = aseguradoras_view[0]
                datos = {'message': "Success", 'aseguradora': aseguradora}
            else:
                datos = {'message': "Aseguradora no encontrada..."}
            return JsonResponse(datos)
        else:
            aseguradoras_view = list(aseguradoras.objects.values())
            if len(aseguradoras_view) > 0:
                datos = {'message': "Success", 'aseguradoras_view': aseguradoras_view}
            else:
                datos = {'message': "Aseguradoras no encontradas..."}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)
        aseguradoras.objects.create(
            nombre=jd['nombre'],
            dirColonia=jd['dirColonia'],
            dirCalle=jd['dirCalle'],
            dirNumero=jd['dirNumero'],
            numTel=jd['numTel'],
            correo=jd['correo']
        )
        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        aseguradoras_view = []
        jd = json.loads(request.body, strict=False)
        aseguradoras_view = list(aseguradoras.objects.filter(num=pk).values())
        if len(aseguradoras_view) > 0:
            aseguradora = aseguradoras.objects.get(num=pk)
            aseguradora.nombre = jd['nombre']
            aseguradora.dirColonia = jd['dirColonia']
            aseguradora.dirCalle = jd['dirCalle']
            aseguradora.dirNumero = jd['dirNumero']
            aseguradora.numTel = jd['numTel']
            aseguradora.correo = jd['correo']
            aseguradora.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Aseguradora no encontrada..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        aseguradoras_view = list(aseguradoras.objects.filter(num=pk).values())
        if len(aseguradoras_view) > 0:
            aseguradoras.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Aseguradora no encontrada..."}
        return JsonResponse(datos)

#FIN SOLITARIOS
#//////////////////////////////////////////////////////////////////////////////////////////////////////

#APIs RUTAS

class RutasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # def get(self, request, pk=0):
    #     rutasview = []
    #     if pk > 0:
    #         rutasview = list(rutas.objects.filter(num=pk).values())
    #         if len(rutasview) > 0:
    #             ruta = rutasview[0]
    #             datos = {'message': "Success", 'ruta': ruta}
    #         else:
    #             datos = {'message': "Ruta not found..."}
    #         return JsonResponse(datos)
    #     else:
    #         rutasview = list(rutas.objects.values())
    #         if len(rutasview) > 0:
    #             datos = {'message': "Success", 'rutas': rutasview}
    #         else:
    #             datos = {'message': "Rutas not found..."}
    #         return JsonResponse(datos)

    def get(self, request, pk=0):
        rutas_data = []
        if pk > 0:
            ruta = rutas.objects.filter(num=pk).values('fechaHoraInicio', 'fechaHoraFin', 'comentarios', 'finalizada', 'vehiculo__numSerie').first()
            if ruta:
                ruta['fechaHoraInicio'] = self.format_date(ruta['fechaHoraInicio'])
                ruta['fechaHoraFin'] = self.format_date(ruta['fechaHoraFin'])
                datos = {'message': "Success", 'ruta': ruta}
            else:
                datos = {'message': "Ruta not found..."}
        else:
            rutas_data = rutas.objects.values('num', 'fechaHoraInicio', 'fechaHoraFin', 'comentarios', 'finalizada', 'vehiculo__numSerie')
            for ruta in rutas_data:
                ruta['fechaHoraInicio'] = self.format_date(ruta['fechaHoraInicio'])
                ruta['fechaHoraFin'] = self.format_date(ruta['fechaHoraFin'])
            datos = {'message': "Success", 'rutas': list(rutas_data)}

        return JsonResponse(datos)

    def format_date(self, date_str):
        if isinstance(date_str, datetime):
            return date_str.strftime('%d/%m/%Y')
        elif isinstance(date_str, date):
            return date_str.strftime('%d/%m/%Y')
        else:
            try:
                date_obj = datetime.strptime(str(date_str), '%Y-%m-%d %H:%M:%S')
                return date_obj.strftime('%d/%m/%Y')
            except ValueError:
                return date_str

    def post(self, request):
        jd = json.loads(request.body)

        vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])

        # Crear una instancia de ruta con las instancias obtenidas
        ruta = rutas.objects.create(
            fechaHoraInicio=jd['fechaHoraInicio'],
            fechaHoraFin=jd['fechaHoraFin'],
            comentarios=jd['comentarios'],
            finalizada=jd['finalizada'],
            vehiculo=vehiculo_instance,
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        rutasview = []
        jd = json.loads(request.body)
        rutasview = list(rutas.objects.filter(num=pk).values())

        if len(rutasview) > 0:
            ruta = rutas.objects.get(num=pk)
            ruta.fechaHoraInicio = jd['fechaHoraInicio']
            ruta.fechaHoraFin = jd['fechaHoraFin']
            ruta.comentarios = jd['comentarios']
            ruta.finalizada = jd['finalizada']

            # Obtén instancias de vehiculos
            try:
                vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
            except vehiculos.DoesNotExist:
                datos = {'message': "Vehiculo not found..."}
                return JsonResponse(datos, status=404)

            ruta.vehiculo = vehiculo_instance
            ruta.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Ruta not found..."}
            return JsonResponse(datos, status=404)

    def delete(self, request, pk=0):
        rutasview = list(rutas.objects.filter(num=pk).values())
        if len(rutasview) > 0:
            rutas.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Ruta not found..."}
        return JsonResponse(datos)
    
class RutasCompletadasCountView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        try:
            # Obtén la fecha actual y la fecha hace 30 días
            fecha_actual = datetime.now()
            fecha_hace_30_dias = fecha_actual - timedelta(days=30)

            # Realiza el COUNT de las rutas completadas en los últimos 30 días
            rutas_completadas_count = rutas.objects.filter(fechaHoraFin__gte=fecha_hace_30_dias, finalizada=True).count()

            datos = {'message': 'Success', 'rutas_completadas_count': rutas_completadas_count}
            return JsonResponse(datos)
        except Exception as e:
            datos = {'message': f'Error: {str(e)}'}
            return JsonResponse(datos, status=500)
#//////////////////////////////////////////////////////////////////////////////////////////////////////

#APIs CARGASCOMBUSTIBLE

# class CargasCombustibleView(View):
#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=0):
#         cargas_combustible_view = []
#         if pk > 0:
#             cargas_combustible_view = list(cargasCombustible.objects.filter(num=pk).values())
#             if len(cargas_combustible_view) > 0:
#                 carga_combustible = cargas_combustible_view[0]
#                 datos = {'message': "Success", 'carga_combustible': carga_combustible}
#             else:
#                 datos = {'message': "Carga de combustible not found..."}
#             return JsonResponse(datos)
#         else:
#             cargas_combustible_view = list(cargasCombustible.objects.values())
#             if len(cargas_combustible_view) > 0:
#                 datos = {'message': "Success", 'cargas_combustible': cargas_combustible_view}
#             else:
#                 datos = {'message': "Cargas de combustible not found..."}
#             return JsonResponse(datos)

#     def post(self, request):
#         jd = json.loads(request.body)

#         vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])

#         carga_combustible = cargasCombustible.objects.create(
#             numFolio=jd['numFolio'],
#             litrosCargados=jd['litrosCargados'],
#             fecha=jd['fecha'],
#             kilometraje=jd['kilometraje'],
#             comentarios=jd['comentarios'],
#             costoTotal=jd['costoTotal'],
#             vehiculo=vehiculo_instance,
#         )

#         datos = {'message': "Success"}
#         return JsonResponse(datos)
    
#     def put(self, request, pk=0):
#         cargascomview = []
#         jd = json.loads(request.body)
#         cargascomview = list(cargasCombustible.objects.filter(num=pk).values())

#         if len(cargascomview) > 0:
#             cargacomb = cargasCombustible.objects.get(num=pk)
#             cargacomb.numFolio = jd['numFolio']
#             cargacomb.litrosCargados = jd['litrosCargados']
#             cargacomb.fecha = jd['fecha']
#             cargacomb.kilometraje = jd['kilometraje']
#             cargacomb.comentarios = jd['comentarios']
#             cargacomb.costoTotal = jd['costoTotal']                       

#             # Obtén instancias de vehiculos
#             try:
#                 vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
#             except vehiculos.DoesNotExist:
#                 datos = {'message': "Vehiculo not found..."}
#                 return JsonResponse(datos, status=404)

#             cargacomb.vehiculo = vehiculo_instance
#             cargacomb.save()
#             datos = {'message': "Success"}
#             return JsonResponse(datos)
#         else:
#             datos = {'message': "Ruta not found..."}
#             return JsonResponse(datos, status=404)

#     def delete(self, request, pk=0):
#         cargas_combustible_view = list(cargasCombustible.objects.filter(num=pk).values())
#         if len(cargas_combustible_view) > 0:
#             cargasCombustible.objects.filter(num=pk).delete()
#             datos = {'message': "Success"}
#         else:
#             datos = {'message': "Carga de combustible not found..."}
#         return JsonResponse(datos)

class CargasCombustibleView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        cargas_combustible_view = []
        if pk > 0:
            cargas_combustible_view = list(cargasCombustible.objects.filter(num=pk).values('numFolio', 'litrosCargados', 'fecha', 'kilometraje', 'comentarios', 'costoTotal', 'vehiculo__numSerie'))
            if len(cargas_combustible_view) > 0:
                carga_combustible = cargas_combustible_view[0]
                carga_combustible['fecha'] = self.format_date(carga_combustible['fecha'])
                datos = {'message': "Success", 'carga_combustible': carga_combustible}
            else:
                datos = {'message': "Carga de combustible not found..."}
            return JsonResponse(datos)
        else:
            cargas_combustible_view = list(cargasCombustible.objects.values('num', 'numFolio', 'litrosCargados', 'fecha', 'kilometraje', 'comentarios', 'costoTotal', 'vehiculo__numSerie'))
            for carga_combustible in cargas_combustible_view:
                carga_combustible['fecha'] = self.format_date(carga_combustible['fecha'])
            if len(cargas_combustible_view) > 0:
                datos = {'message': "Success", 'cargas_combustible': cargas_combustible_view}
            else:
                datos = {'message': "Cargas de combustible not found..."}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)

        vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])

        carga_combustible = cargasCombustible.objects.create(
            numFolio=jd['numFolio'],
            litrosCargados=jd['litrosCargados'],
            fecha=jd['fecha'],
            kilometraje=jd['kilometraje'],
            comentarios=jd['comentarios'],
            costoTotal=jd['costoTotal'],
            vehiculo=vehiculo_instance,
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        cargascomview = []
        jd = json.loads(request.body)
        cargascomview = list(cargasCombustible.objects.filter(num=pk).values())

        if len(cargascomview) > 0:
            cargacomb = cargasCombustible.objects.get(num=pk)
            cargacomb.numFolio = jd['numFolio']
            cargacomb.litrosCargados = jd['litrosCargados']
            cargacomb.fecha = jd['fecha']
            cargacomb.kilometraje = jd['kilometraje']
            cargacomb.comentarios = jd['comentarios']
            cargacomb.costoTotal = jd['costoTotal']                       

            # Obtén instancias de vehiculos
            try:
                vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
            except vehiculos.DoesNotExist:
                datos = {'message': "Vehiculo not found..."}
                return JsonResponse(datos, status=404)

            cargacomb.vehiculo = vehiculo_instance
            cargacomb.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Ruta not found..."}
            return JsonResponse(datos, status=404)

    def delete(self, request, pk=0):
        cargas_combustible_view = list(cargasCombustible.objects.filter(num=pk).values())
        if len(cargas_combustible_view) > 0:
            cargasCombustible.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Carga de combustible not found..."}
        return JsonResponse(datos)

    def format_date(self, date_obj):
        # Verificar si la entrada ya es un objeto datetime.date
        if isinstance(date_obj, date):
            # Formatear la fecha en el formato deseado (día-mes-año)
            formatted_date = date_obj.strftime('%d/%m/%Y')
            return formatted_date
        else:
            # Convertir la cadena de fecha a un objeto de fecha
            date_obj = datetime.strptime(date_obj, '%Y-%m-%d').date()
            # Formatear la fecha en el formato deseado (día-mes-año)
            formatted_date = date_obj.strftime('%d/%m/%Y')
            return formatted_date

# #APIs Infracciones

# class InfraccionesView(View):
#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=0):
#         infracciones_view = []
#         if pk > 0:
#             infracciones_view = list(infracciones.objects.filter(num=pk).values())
#             if len(infracciones_view) > 0:
#                 infraccion = infracciones_view[0]
#                 datos = {'message': "Success", 'infraccion': infraccion}
#             else:
#                 datos = {'message': "Infracción not found..."}
#             return JsonResponse(datos)
#         else:
#             infracciones_view = list(infracciones.objects.values())
#             if len(infracciones_view) > 0:
#                 datos = {'message': "Success", 'infracciones': infracciones_view}
#             else:
#                 datos = {'message': "Infracciones not found..."}
#             return JsonResponse(datos)

#     def post(self, request):
#         jd = json.loads(request.body)

#         try:
#             vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
#             conductor_instance = usuarios.objects.get(pk=jd['conductor'])
#         except (vehiculos.DoesNotExist, usuarios.DoesNotExist):
#             datos = {'message': "Vehiculo or Conductor not found..."}
#             return JsonResponse(datos, status=404)

#         infraccion = infracciones.objects.create(
#             fechahora=jd['fechahora'],
#             fecha=jd['fecha'],
#             dirCalle=jd['dirCalle'],
#             dirNumero=jd['dirNumero'],
#             dirColonia=jd['dirColonia'],
#             monto=jd['monto'],
#             fechaLimitePago=jd['fechaLimitePago'],
#             esConductorResponsable=jd['esConductorResponsable'],
#             vehiculo=vehiculo_instance,
#             conductor=conductor_instance,
#         )

#         datos = {'message': "Success"}
#         return JsonResponse(datos)

#     def put(self, request, pk=0):
#         infracciones_view = []
#         jd = json.loads(request.body)
#         infracciones_view = list(infracciones.objects.filter(num=pk).values())

#         if len(infracciones_view) > 0:
#             infraccion = infracciones.objects.get(num=pk)
#             infraccion.fechahora = jd['fechahora']
#             infraccion.fecha = jd['fecha']
#             infraccion.dirCalle = jd['dirCalle']
#             infraccion.dirNumero = jd['dirNumero']
#             infraccion.dirColonia = jd['dirColonia']
#             infraccion.monto = jd['monto']
#             infraccion.fechaLimitePago = jd['fechaLimitePago']
#             infraccion.esConductorResponsable = jd['esConductorResponsable']

#             try:
#                 vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
#                 conductor_instance = usuarios.objects.get(pk=jd['conductor'])
#             except (vehiculos.DoesNotExist, usuarios.DoesNotExist):
#                 datos = {'message': "Vehiculo or Conductor not found..."}
#                 return JsonResponse(datos, status=404)

#             infraccion.vehiculo = vehiculo_instance
#             infraccion.conductor = conductor_instance

#             infraccion.save()
#             datos = {'message': "Success"}
#             return JsonResponse(datos)
#         else:
#             datos = {'message': "Infracción not found..."}
#             return JsonResponse(datos, status=404)

#     def delete(self, request, pk=0):
#         infracciones_view = list(infracciones.objects.filter(num=pk).values())
#         if len(infracciones_view) > 0:
#             infracciones.objects.filter(num=pk).delete()
#             datos = {'message': "Success"}
#         else:
#             datos = {'message': "Infracción not found..."}
#         return JsonResponse(datos)

class InfraccionesView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        infracciones_view = []
        if pk > 0:
            infracciones_view = list(infracciones.objects.filter(num=pk).values('fechahora', 'dirCalle', 'dirNumero', 'dirColonia', 'monto', 'fechaLimitePago', 'esConductorResponsable', 'descripcion', 'vehiculo__numSerie', 'conductor__nombrePila', 'conductor__apPaterno', 'conductor__apMaterno'))
            if len(infracciones_view) > 0:
                infraccion = infracciones_view[0]
                infraccion['fechahora'] = self.format_date(infraccion['fechahora'])
                infraccion['fechaLimitePago'] = self.format_date(infraccion['fechaLimitePago'])
                # Concatenar nombre completo del conductor
                infraccion['nombreConductor'] = f"{infraccion['conductor__nombrePila']} {infraccion['conductor__apPaterno']} {infraccion['conductor__apMaterno']}"
                datos = {'message': "Success", 'infraccion': infraccion}
            else:
                datos = {'message': "Infracción not found..."}
            return JsonResponse(datos)
        else:
            infracciones_view = list(infracciones.objects.values('num', 'fechahora', 'dirCalle', 'dirNumero', 'dirColonia', 'monto', 'fechaLimitePago', 'esConductorResponsable', 'descripcion', 'vehiculo__numSerie', 'conductor__nombrePila', 'conductor__apPaterno', 'conductor__apMaterno'))
            for infraccion in infracciones_view:
                infraccion['fechahora'] = self.format_date(infraccion['fechahora'])
                infraccion['fechaLimitePago'] = self.format_date(infraccion['fechaLimitePago'])
                # Concatenar nombre completo del conductor
                infraccion['nombreConductor'] = f"{infraccion['conductor__nombrePila']} {infraccion['conductor__apPaterno']} {infraccion['conductor__apMaterno']}"
            if len(infracciones_view) > 0:
                datos = {'message': "Success", 'infracciones': infracciones_view}
            else:
                datos = {'message': "Infracciones not found..."}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)

        try:
            vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
            conductor_instance = usuarios.objects.get(pk=jd['conductor'])
        except (vehiculos.DoesNotExist, usuarios.DoesNotExist):
            datos = {'message': "Vehiculo or Conductor not found..."}
            return JsonResponse(datos, status=404)

        # Formatear fechas
        fechahora = self.parse_date(jd['fechahora'])
        fechaLimitePago = self.parse_date(jd['fechaLimitePago'], has_time=False)

        infraccion = infracciones.objects.create(
            fechahora=fechahora,
            dirCalle=jd['dirCalle'],
            dirNumero=jd['dirNumero'],
            dirColonia=jd['dirColonia'],
            monto=jd['monto'],
            fechaLimitePago=fechaLimitePago,
            esConductorResponsable=jd['esConductorResponsable'],
            descripcion=jd['descripcion'],
            vehiculo=vehiculo_instance,
            conductor=conductor_instance,
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        infracciones_view = []
        jd = json.loads(request.body)
        infracciones_view = list(infracciones.objects.filter(num=pk).values())

        if len(infracciones_view) > 0:
            infraccion = infracciones.objects.get(num=pk)
            infraccion.fechahora = self.parse_date(jd['fechahora'])
            infraccion.dirCalle = jd['dirCalle']
            infraccion.dirNumero = jd['dirNumero']
            infraccion.dirColonia = jd['dirColonia']
            infraccion.monto = jd['monto']
            infraccion.fechaLimitePago = self.parse_date(jd['fechaLimitePago'], has_time=False)
            infraccion.esConductorResponsable = jd['esConductorResponsable']

            try:
                vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
                conductor_instance = usuarios.objects.get(pk=jd['conductor'])
            except (vehiculos.DoesNotExist, usuarios.DoesNotExist):
                datos = {'message': "Vehiculo or Conductor not found..."}
                return JsonResponse(datos, status=404)

            infraccion.vehiculo = vehiculo_instance
            infraccion.conductor = conductor_instance

            infraccion.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Infracción not found..."}
            return JsonResponse(datos, status=404)

    def delete(self, request, pk=0):
        infracciones_view = list(infracciones.objects.filter(num=pk).values())
        if len(infracciones_view) > 0:
            infracciones.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Infracción not found..."}
        return JsonResponse(datos)

    def format_date(self, date_str):
        if date_str == '0000-00-00':
            return 'Sin fecha. Revisa e ingresa una fecha!'
        elif isinstance(date_str, datetime):
            return date_str.strftime('%d/%m/%Y %H:%M')
        elif isinstance(date_str, date):
            return date_str.strftime('%d/%m/%Y')
        else:
            date_obj = datetime.strptime(str(date_str), '%Y-%m-%d %H:%M:%S')
            return date_obj.strftime('%d/%m/%Y %H:%M')

    def parse_date(self, date_str, has_time=True):
        if has_time:
            date_obj = datetime.strptime(date_str, '%d/%m/%Y %H:%M')
        else:
            date_obj = datetime.strptime(date_str, '%d/%m/%Y')
        return date_obj
#//////////////////////////////////////////////////////////////////////////////////////////////////////
    
#APIs SESIONESMANTENIMIENTO:

class SesionesMantView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # def get(self, request, pk=0):
    #     sesiones_view = []
    #     if pk > 0:
    #         sesiones_view = list(sesionesMantenimiento.objects.filter(num=pk).values())
    #         if len(sesiones_view) > 0:
    #             sesion = sesiones_view[0]
    #             datos = {'message': "Success", 'sesion': sesion}
    #         else:
    #             datos = {'message': "Sesión not found..."}
    #         return JsonResponse(datos)
    #     else:
    #         sesiones_view = list(sesionesMantenimiento.objects.values())
    #         if len(sesiones_view) > 0:
    #             datos = {'message': "Success", 'sesiones': sesiones_view}
    #         else:
    #             datos = {'message': "Sesiones not found..."}
    #         return JsonResponse(datos)

    def get(self, request, pk=0):
        sesiones_data = []
        if pk > 0:
            sesion = sesionesMantenimiento.objects.filter(num=pk).values('num', 'fecha', 'kilometraje', 'costoTotal', 'descripcion', 'vehiculo__numSerie', 'taller__nombre').first()
            if sesion:
                sesion['fecha'] = self.format_date(sesion['fecha'])
                datos = {'message': "Success", 'sesion': sesion}
            else:
                datos = {'message': "Sesión not found..."}
        else:
            sesiones_data = sesionesMantenimiento.objects.values('num', 'fecha', 'kilometraje', 'costoTotal', 'descripcion', 'vehiculo__numSerie', 'taller__nombre')
            for sesion in sesiones_data:
                sesion['fecha'] = self.format_date(sesion['fecha'])
            datos = {'message': "Success", 'sesiones': list(sesiones_data)}

        return JsonResponse(datos)
    
    def format_date(self, date_str):
        if isinstance(date_str, datetime):
            return date_str.strftime('%d/%m/%Y')
        elif isinstance(date_str, date):
            return date_str.strftime('%d/%m/%Y')
        else:
            try:
                date_obj = datetime.strptime(str(date_str), '%Y-%m-%d %H:%M:%S')
                return date_obj.strftime('%d/%m/%Y')
            except ValueError:
                return date_str

    def post(self, request):
        jd = json.loads(request.body)

        try:
            vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
            taller_instance = talleres.objects.get(pk=jd['taller'])
        except (vehiculos.DoesNotExist, talleres.DoesNotExist):
            datos = {'message': "Vehiculo or Taller not found..."}
            return JsonResponse(datos, status=404)

        sesion = sesionesMantenimiento.objects.create(
            fecha=jd['fecha'],
            kilometraje=jd['kilometraje'],
            costoTotal=jd['costoTotal'],
            descripcion=jd['descripcion'],
            vehiculo=vehiculo_instance,
            taller=taller_instance,
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        sesiones_view = []
        jd = json.loads(request.body)
        sesiones_view = list(sesionesMantenimiento.objects.filter(num=pk).values())

        if len(sesiones_view) > 0:
            sesion = sesionesMantenimiento.objects.get(num=pk)
            sesion.fecha = jd['fecha']
            sesion.kilometraje = jd['kilometraje']
            sesion.costoTotal = jd['costoTotal']
            sesion.descripcion = jd['descripcion']

            try:
                vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
                taller_instance = talleres.objects.get(pk=jd['taller'])
            except (vehiculos.DoesNotExist, talleres.DoesNotExist):
                datos = {'message': "Vehiculo or Taller not found..."}
                return JsonResponse(datos, status=404)

            sesion.vehiculo = vehiculo_instance
            sesion.taller = taller_instance

            sesion.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Sesión not found..."}
            return JsonResponse(datos, status=404)

    def delete(self, request, pk=0):
        sesiones_view = list(sesionesMantenimiento.objects.filter(num=pk).values())
        if len(sesiones_view) > 0:
            sesionesMantenimiento.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Sesión not found..."}
        return JsonResponse(datos)
#//////////////////////////////////////////////////////////////////////////////////////////////////////

#APIs DOCUMENTOSVEHICULOS

# class DocumentosVehiculosView(View):

#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=0):
#         documentos_view = []
#         if pk > 0:
#             documentos_view = list(documentosVehiculos.objects.filter(num=pk).values())
#             if len(documentos_view) > 0:
#                 documento = documentos_view[0]
#                 datos = {'message': "Success", 'documento': documento}
#             else:
#                 datos = {'message': "Documento not found..."}
#             return JsonResponse(datos)
#         else:
#             documentos_view = list(documentosVehiculos.objects.values())
#             if len(documentos_view) > 0:
#                 datos = {'message': "Success", 'documentos': documentos_view}
#             else:
#                 datos = {'message': "Documentos not found..."}
#             return JsonResponse(datos)

#     def post(self, request):
#         jd = json.loads(request.body)

#         try:
#             vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
#         except vehiculos.DoesNotExist:
#             datos = {'message': "Vehiculo not found..."}
#             return JsonResponse(datos, status=404)

#         documento = documentosVehiculos.objects.create(
#             tipo=jd['tipo'],
#             fechaEmision=jd['fechaEmision'],
#             fechaVencimiento=jd['fechaVencimiento'],
#             comentarios=jd['comentarios'],
#             vehiculo=vehiculo_instance,
#         )

#         datos = {'message': "Success"}
#         return JsonResponse(datos)

#     def put(self, request, pk=0):
#         documentos_view = []
#         jd = json.loads(request.body)
#         documentos_view = list(documentosVehiculos.objects.filter(num=pk).values())

#         if len(documentos_view) > 0:
#             documento = documentosVehiculos.objects.get(num=pk)
#             documento.tipo = jd['tipo']
#             documento.fechaEmision = jd['fechaEmision']
#             documento.fechaVencimiento = jd['fechaVencimiento']
#             documento.comentarios = jd['comentarios']

#             try:
#                 vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
#             except vehiculos.DoesNotExist:
#                 datos = {'message': "Vehiculo not found..."}
#                 return JsonResponse(datos, status=404)

#             documento.vehiculo = vehiculo_instance
#             documento.save()
#             datos = {'message': "Success"}
#             return JsonResponse(datos)
#         else:
#             datos = {'message': "Documento not found..."}
#             return JsonResponse(datos, status=404)

#     def delete(self, request, pk=0):
#         documentos_view = list(documentosVehiculos.objects.filter(num=pk).values())
#         if len(documentos_view) > 0:
#             documentosVehiculos.objects.filter(num=pk).delete()
#             datos = {'message': "Success"}
#         else:
#             datos = {'message': "Documento not found..."}
#         return JsonResponse(datos)

class DocumentosVehiculosView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        documentos_view = []
        if pk > 0:
            documentos_view = list(documentosVehiculos.objects.filter(num=pk).values())
            if len(documentos_view) > 0:
                documento = documentos_view[0]
                # Formatear fechas
                documento['fechaEmision'] = self.format_date(documento['fechaEmision'])
                documento['fechaVencimiento'] = self.format_date(documento['fechaVencimiento'])

                # Obtener el número de serie del vehículo
                vehiculo_id = documento.get('vehiculo_id')
                if vehiculo_id:
                    try:
                        vehiculo = vehiculos.objects.get(pk=vehiculo_id)
                        documento['numSerieVehiculo'] = vehiculo.numSerie
                    except vehiculos.DoesNotExist:
                        documento['numSerieVehiculo'] = None
                else:
                    documento['numSerieVehiculo'] = None

                datos = {'message': "Success", 'documento': documento}
            else:
                datos = {'message': "Documento not found..."}
            return JsonResponse(datos)
        else:
            documentos_view = list(documentosVehiculos.objects.values())
            for documento in documentos_view:
                # Formatear fechas
                documento['fechaEmision'] = self.format_date(documento['fechaEmision'])
                documento['fechaVencimiento'] = self.format_date(documento['fechaVencimiento'])

                # Obtener el número de serie del vehículo
                vehiculo_id = documento.get('vehiculo_id')
                if vehiculo_id:
                    try:
                        vehiculo = vehiculos.objects.get(pk=vehiculo_id)
                        documento['numSerieVehiculo'] = vehiculo.numSerie
                    except vehiculos.DoesNotExist:
                        documento['numSerieVehiculo'] = None
                else:
                    documento['numSerieVehiculo'] = None

            if len(documentos_view) > 0:
                datos = {'message': "Success", 'documentos': documentos_view}
            else:
                datos = {'message': "Documentos not found..."}
            return JsonResponse(datos)

    def post(self, request):
        jd = json.loads(request.body)

        try:
            vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
        except vehiculos.DoesNotExist:
            datos = {'message': "Vehiculo not found..."}
            return JsonResponse(datos, status=404)

        # Formatear fecha de emisión y vencimiento
        fecha_emision = self.parse_date(jd['fechaEmision'])
        fecha_vencimiento = self.parse_date(jd['fechaVencimiento'])

        documento = documentosVehiculos.objects.create(
            tipo=jd['tipo'],
            fechaEmision=fecha_emision,
            fechaVencimiento=fecha_vencimiento,
            comentarios=jd['comentarios'],
            vehiculo=vehiculo_instance,
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        documentos_view = []
        jd = json.loads(request.body)
        documentos_view = list(documentosVehiculos.objects.filter(num=pk).values())

        if len(documentos_view) > 0:
            documento = documentosVehiculos.objects.get(num=pk)
            documento.tipo = jd['tipo']

            # Formatear fecha de emisión y vencimiento
            fecha_emision = self.parse_date(jd['fechaEmision'])
            fecha_vencimiento = self.parse_date(jd['fechaVencimiento'])

            documento.fechaEmision = fecha_emision
            documento.fechaVencimiento = fecha_vencimiento
            documento.comentarios = jd['comentarios']

            try:
                vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
            except vehiculos.DoesNotExist:
                datos = {'message': "Vehiculo not found..."}
                return JsonResponse(datos, status=404)

            documento.vehiculo = vehiculo_instance
            documento.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Documento not found..."}
            return JsonResponse(datos, status=404)

    def delete(self, request, pk=0):
        documentos_view = list(documentosVehiculos.objects.filter(num=pk).values())
        if len(documentos_view) > 0:
            documentosVehiculos.objects.filter(num=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Documento not found..."}
        return JsonResponse(datos)

    from datetime import datetime, date

    def format_date(self, date_str):
        if date_str == '0000-00-00':
            return 'Sin fecha'
        elif isinstance(date_str, date):
            # Si ya es un objeto datetime.date, no necesitas hacer nada
            return date_str.strftime('%d/%m/%Y')
        else:
            # Si es una cadena, conviértela a objeto datetime.date y luego formatea
            date_obj = datetime.strptime(str(date_str), '%Y-%m-%d').date()
            return date_obj.strftime('%d/%m/%Y')
        
    def parse_date(self, date_str):
        date_obj = datetime.strptime(date_str, '%d/%m/%Y').date()
        return date_obj
#//////////////////////////////////////////////////////////////////////////////////////////////////////

#APIs POLIZASSEGUROS

# def get(self, request, pk=0):
#         polizas_view = []
#         if pk > 0:
#             polizas_view = list(polizasSeguros.objects.filter(numPoliza=pk).values())
#             if len(polizas_view) > 0:
#                 poliza = polizas_view[0]
#                 datos = {'message': "Success", 'poliza': poliza}
#             else:
#                 datos = {'message': "Poliza not found..."}
#             return JsonResponse(datos)
#         else:
#             polizas_view = list(polizasSeguros.objects.values())
#             if len(polizas_view) > 0:
#                 datos = {'message': "Success", 'polizas': polizas_view}
#             else:
#                 datos = {'message': "Polizas not found..."}
#             return JsonResponse(datos)

class PolizasSegView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, pk=0):
        polizas_data = []
        if pk > 0:
            poliza = polizasSeguros.objects.filter(numPoliza=pk).values('numPoliza', 'aseguradora__nombre', 'fechaInicio', 'fechaVencimiento', 'montoCobertura', 'costo', 'frecuenciaPago', 'vehiculo__numSerie', 'cobertura__nombre').first()
            if poliza:
                poliza['fechaInicio'] = self.format_date(poliza['fechaInicio'])
                poliza['fechaVencimiento'] = self.format_date(poliza['fechaVencimiento'])
                datos = {'message': "Success", 'poliza': poliza} 
            else:
                datos = {'message': "Poliza not found..."}
        else:
            polizas_data = polizasSeguros.objects.values('numPoliza', 'aseguradora__nombre', 'fechaInicio', 'fechaVencimiento', 'montoCobertura', 'costo', 'frecuenciaPago', 'vehiculo__numSerie', 'cobertura__nombre').order_by('numPoliza')
            for poliza in polizas_data:
                poliza['fechaInicio'] = self.format_date(poliza['fechaInicio'])
                poliza['fechaVencimiento'] = self.format_date(poliza['fechaVencimiento'])
            datos = {'message': "Success", 'polizas': list(polizas_data)}

        return JsonResponse(datos)

    def format_date(self, date_str):
        if isinstance(date_str, datetime):
            return date_str.strftime('%d/%m/%Y')
        elif isinstance(date_str, date):  # Agregamos manejo para objetos date
            return date_str.strftime('%d/%m/%Y')
        else:
            try:
                date_obj = datetime.strptime(str(date_str), '%Y-%m-%d %H:%M:%S')
                return date_obj.strftime('%d/%m/%Y')
            except ValueError:
                return date_str  # Devolver la cadena original si la conversión falla

    def post(self, request):
        jd = json.loads(request.body)

        try:
            vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
        except vehiculos.DoesNotExist:
            datos = {'message': "Vehiculo not found..."}
            return JsonResponse(datos, status=404)

        try:
            aseguradora_instance = aseguradoras.objects.get(pk=jd['aseguradora'])
        except aseguradoras.DoesNotExist:
            datos = {'message': "Aseguradora not found..."}
            return JsonResponse(datos, status=404)

        try:
            cobertura_instance = tiposCoberturas.objects.get(num=jd['cobertura'])
        except tiposCoberturas.DoesNotExist:
            datos = {'message': "Cobertura not found..."}
            return JsonResponse(datos, status=404)

        poliza = polizasSeguros.objects.create(
            fechaInicio=jd['fechaInicio'],
            fechaVencimiento=jd['fechaVencimiento'],
            montoCobertura=jd['montoCobertura'],
            costo=jd['costo'],
            frecuenciaPago=jd['frecuenciaPago'],
            vehiculo=vehiculo_instance,
            aseguradora=aseguradora_instance,
            cobertura=cobertura_instance,
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        polizas_view = []
        jd = json.loads(request.body)
        polizas_view = list(polizasSeguros.objects.filter(numPoliza=pk).values())

        if len(polizas_view) > 0:
            poliza = polizasSeguros.objects.get(numPoliza=pk)
            poliza.fechaInicio = jd['fechaInicio']
            poliza.fechaVencimiento = jd['fechaVencimiento']
            poliza.montoCobertura = jd['montoCobertura']
            poliza.costo = jd['costo']
            poliza.frecuenciaPago = jd['frecuenciaPago']

            try:
                vehiculo_instance = vehiculos.objects.get(pk=jd['vehiculo'])
            except vehiculos.DoesNotExist:
                datos = {'message': "Vehiculo not found..."}
                return JsonResponse(datos, status=404)

            try:
                aseguradora_instance = aseguradoras.objects.get(pk=jd['aseguradora'])
            except aseguradoras.DoesNotExist:
                datos = {'message': "Aseguradora not found..."}
                return JsonResponse(datos, status=404)

            try:
                cobertura_instance = tiposCoberturas.objects.get(num=jd['cobertura'])
            except tiposCoberturas.DoesNotExist:
                datos = {'message': "Cobertura not found..."}
                return JsonResponse(datos, status=404)

            poliza.vehiculo = vehiculo_instance
            poliza.aseguradora = aseguradora_instance
            poliza.cobertura = cobertura_instance
            poliza.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Poliza not found..."}
            return JsonResponse(datos, status=404)

    def delete(self, request, pk=0):
        polizas_view = list(polizasSeguros.objects.filter(numPoliza=pk).values())
        if len(polizas_view) > 0:
            polizasSeguros.objects.filter(numPoliza=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Poliza not found..."}
        return JsonResponse(datos)
    
class PolizasNoVencidasCountView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        # Obtén la fecha actual
        fecha_actual = date.today()

        # Realiza el COUNT de las pólizas que no han vencido
        polizas_no_vencidas_count = polizasSeguros.objects.filter(fechaVencimiento__gte=fecha_actual).count()

        datos = {'message': 'Success', 'polizas_no_vencidas_count': polizas_no_vencidas_count}
        return JsonResponse(datos)
#//////////////////////////////////////////////////////////////////////////////////////////////////////

#APIs RECLAMACIONESSEGUROS
class ReclamacionesSegView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # def get(self, request, pk=0):
    #     reclamaciones_view = []
    #     if pk > 0:
    #         reclamaciones_view = list(reclamacionesSeguros.objects.filter(numFolio=pk).values())
    #         if len(reclamaciones_view) > 0:
    #             reclamacion = reclamaciones_view[0]
    #             datos = {'message': "Success", 'reclamacion': reclamacion}
    #         else:
    #             datos = {'message': "Reclamacion not found..."}
    #         return JsonResponse(datos)
    #     else:
    #         reclamaciones_view = list(reclamacionesSeguros.objects.values())
    #         if len(reclamaciones_view) > 0:
    #             datos = {'message': "Success", 'reclamaciones': reclamaciones_view}
    #         else:
    #             datos = {'message': "Reclamaciones not found..."}
    #         return JsonResponse(datos)

    def get(self, request, pk=0):
        reclamaciones_data = []
        if pk > 0:
            reclamacion = reclamacionesSeguros.objects.filter(numFolio=pk).values(
                'fecha', 'descripcionIncidente', 'montoReclamo', 'numPoliza__numPoliza',
                'reclamacionesestadosrec__estado__descripcion', 'reclamacionesestadosrec__fechacambio'
            ).first()
            if reclamacion:
                reclamacion['fecha'] = self.format_date(reclamacion['fecha'])
                datos = {'message': "Success", 'reclamacion': reclamacion}
            else:
                datos = {'message': "Reclamacion not found..."}
        else:
            reclamaciones_data = reclamacionesSeguros.objects.values(
                'numFolio', 'fecha', 'descripcionIncidente', 'montoReclamo', 'numPoliza__numPoliza',
                'reclamacionesestadosrec__estado__descripcion', 'reclamacionesestadosrec__fechacambio'
            )
            for reclamacion in reclamaciones_data:
                reclamacion['fecha'] = self.format_date(reclamacion['fecha'])
            datos = {'message': "Success", 'reclamaciones': list(reclamaciones_data)}

        return JsonResponse(datos)

    def format_date(self, date_str):
        if isinstance(date_str, datetime):
            return date_str.strftime('%d/%m/%Y')
        elif isinstance(date_str, date):
            return date_str.strftime('%d/%m/%Y')
        else:
            try:
                date_obj = datetime.strptime(str(date_str), '%Y-%m-%d %H:%M:%S')
                return date_obj.strftime('%d/%m/%Y')
            except ValueError:
                return date_str

    def post(self, request):
        jd = json.loads(request.body)

        try:
            poliza_instance = polizasSeguros.objects.get(numPoliza=jd['numPoliza'])
        except polizasSeguros.DoesNotExist:
            datos = {'message': "Poliza not found..."}
            return JsonResponse(datos, status=404)

        reclamacion = reclamacionesSeguros.objects.create(
            fecha=jd['fecha'],
            descripcionIncidente=jd['descripcionIncidente'],
            montoReclamo=jd.get('montoReclamo', None),
            numPoliza=poliza_instance,
        )

        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, pk=0):
        reclamaciones_view = []
        jd = json.loads(request.body)
        reclamaciones_view = list(reclamacionesSeguros.objects.filter(numFolio=pk).values())

        if len(reclamaciones_view) > 0:
            reclamacion = reclamacionesSeguros.objects.get(numFolio=pk)
            reclamacion.fecha = jd['fecha']
            reclamacion.descripcionIncidente = jd['descripcionIncidente']
            reclamacion.montoReclamo = jd.get('montoReclamo', None)

            try:
                poliza_instance = polizasSeguros.objects.get(numPoliza=jd['numPoliza'])
            except polizasSeguros.DoesNotExist:
                datos = {'message': "Poliza not found..."}
                return JsonResponse(datos, status=404)

            reclamacion.numPoliza = poliza_instance
            reclamacion.save()
            datos = {'message': "Success"}
            return JsonResponse(datos)
        else:
            datos = {'message': "Reclamacion not found..."}
            return JsonResponse(datos, status=404)

    def delete(self, request, pk=0):
        reclamaciones_view = list(reclamacionesSeguros.objects.filter(numFolio=pk).values())
        if len(reclamaciones_view) > 0:
            reclamacionesSeguros.objects.filter(numFolio=pk).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Reclamacion not found..."}
        return JsonResponse(datos)

#APIS ESTADOS
class estadosLicenciasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)  
    
    def get(self, request, pk=0):
        estadoslicencias = []
        if (pk > 0):
            estadoslicencias = list(estadosLicencias.objects.filter(num=pk).values())
            if len(estadoslicencias) > 0:
                estadolicencia = estadoslicencias[0]
                datos = {'message': "Succes", 'estadolicencia': estadolicencia}
            else:
                datos = {'message': "estadolicencia not found..."}
            return JsonResponse(datos)
        else:
            estadoslicencias = list(estadosLicencias.objects.values())
            if len(estadoslicencias) > 0:
                datos = {'message': "Succes", 'estadoslicencias': estadoslicencias}
            else:
                datos = {'message': "estadoslicencias not found..."}
            return JsonResponse(datos)
        
    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        estadosLicencias.objects.create(num=jd['num'], descripcion=jd['descripcion'])
        datos = {'message':"Succes"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        estadoslicencias = []
        jd = json.loads(request.body, strict=False)
        estadoslicencias = list(estadosLicencias.objects.filter(num=pk).values())
        if len(estadoslicencias) > 0:
            estadolicencia = estadosLicencias.objects.get(num=pk)
            estadolicencia.num = jd['num']
            estadolicencia.descripcion = jd['descripcion']
            estadolicencia.save()
            datos = {'message':"Succes"}
            return JsonResponse(datos)
        else:
            datos = {'message': "estadoslicencias not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        estadoslicencias = list(estadosLicencias.objects.filter(num=pk).values())
        if len(estadoslicencias) > 0:
            estadosLicencias.objects.filter(num=pk).delete()
            datos = {'message':"Succes"}
        else:
            datos = {'message': "estadoslicencias not found..."}
        return JsonResponse(datos)
    
class estadosParadasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)  
    
    def get(self, request, pk=0):
        estadosparadas = []
        if (pk > 0):
            estadosparadas = list(estadosParadas.objects.filter(num=pk).values())
            if len(estadosparadas) > 0:
                estadoparada = estadosparadas[0]
                datos = {'message': "Succes", 'estadoparada': estadoparada}
            else:
                datos = {'message': "estadoparada not found..."}
            return JsonResponse(datos)
        else:
            estadosparadas = list(estadosParadas.objects.values())
            if len(estadosparadas) > 0:
                datos = {'message': "Succes", 'estadosparadas': estadosparadas}
            else:
                datos = {'message': "estadosparadas not found..."}
            return JsonResponse(datos)
        
    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        estadosParadas.objects.create(num=jd['num'], descripcion=jd['descripcion'])
        datos = {'message':"Succes"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        estadosparadas = []
        jd = json.loads(request.body, strict=False)
        estadosparadas = list(estadosParadas.objects.filter(num=pk).values())
        if len(estadosparadas) > 0:
            estadoparada = estadosParadas.objects.get(num=pk)
            estadoparada.num = jd['num']
            estadoparada.descripcion = jd['descripcion']
            estadoparada.save()
            datos = {'message':"Succes"}
            return JsonResponse(datos)
        else:
            datos = {'message': "estadosparadas not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        estadosparadas = list(estadosParadas.objects.filter(num=pk).values())
        if len(estadosparadas) > 0:
            estadosParadas.objects.filter(num=pk).delete()
            datos = {'message':"Succes"}
        else:
            datos = {'message': "estadosparadas not found..."}
        return JsonResponse(datos)
    
class estadosRutasView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)  
    
    def get(self, request, pk=0):
        estadosrutas = []
        if (pk > 0):
            estadosrutas = list(estadosRutas.objects.filter(num=pk).values())
            if len(estadosrutas) > 0:
                estadoruta = estadosrutas[0]
                datos = {'message': "Succes", 'estadoruta': estadoruta}
            else:
                datos = {'message': "estadoruta not found..."}
            return JsonResponse(datos)
        else:
            estadosrutas = list(estadosRutas.objects.values())
            if len(estadosrutas) > 0:
                datos = {'message': "Succes", 'estadosrutas': estadosrutas}
            else:
                datos = {'message': "estadosrutas not found..."}
            return JsonResponse(datos)
        
    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        estadosRutas.objects.create(num=jd['num'], descripcion=jd['descripcion'])
        datos = {'message':"Succes"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        estadosrutas = []
        jd = json.loads(request.body, strict=False)
        estadosrutas = list(estadosRutas.objects.filter(num=pk).values())
        if len(estadosrutas) > 0:
            estadoruta = estadosRutas.objects.get(num=pk)
            estadoruta.num = jd['num']
            estadoruta.descripcion = jd['descripcion']
            estadoruta.save()
            datos = {'message':"Succes"}
            return JsonResponse(datos)
        else:
            datos = {'message': "estadosrutas not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        estadosrutas = list(estadosRutas.objects.filter(num=pk).values())
        if len(estadosrutas) > 0:
            estadosRutas.objects.filter(num=pk).delete()
            datos = {'message':"Succes"}
        else:
            datos = {'message': "estadosrutas not found..."}
        return JsonResponse(datos)
    
class estadosReclamacionesView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)  
    
    def get(self, request, pk=0):
        estadosreclamaciones = []
        if (pk > 0):
            estadosreclamaciones = list(estadosReclamaciones.objects.filter(num=pk).values())
            if len(estadosreclamaciones) > 0:
                estadoreclamacion = estadosreclamaciones[0]
                datos = {'message': "Succes", 'estadoreclamacion': estadoreclamacion}
            else:
                datos = {'message': "estadoreclamacion not found..."}
            return JsonResponse(datos)
        else:
            estadosreclamaciones = list(estadosReclamaciones.objects.values())
            if len(estadosreclamaciones) > 0:
                datos = {'message': "Succes", 'estadosreclamaciones': estadosreclamaciones}
            else:
                datos = {'message': "estadosreclamaciones not found..."}
            return JsonResponse(datos)
        
    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        estadosReclamaciones.objects.create(num=jd['num'], descripcion=jd['descripcion'])
        datos = {'message':"Succes"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        estadosreclamaciones = []
        jd = json.loads(request.body, strict=False)
        estadosreclamaciones = list(estadosReclamaciones.objects.filter(num=pk).values())
        if len(estadosreclamaciones) > 0:
            estadoreclamacion = estadosReclamaciones.objects.get(num=pk)
            estadoreclamacion.num = jd['num']
            estadoreclamacion.descripcion = jd['descripcion']
            estadoreclamacion.save()
            datos = {'message':"Succes"}
            return JsonResponse(datos)
        else:
            datos = {'message': "estadosreclamaciones not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        estadosreclamaciones = list(estadosReclamaciones.objects.filter(num=pk).values())
        if len(estadosreclamaciones) > 0:
            estadosReclamaciones.objects.filter(num=pk).delete()
            datos = {'message':"Succes"}
        else:
            datos = {'message': "estadosreclamaciones not found..."}
        return JsonResponse(datos)
    
class estadoInfraccionView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)  
    
    def get(self, request, pk=0):
        estadosinfracciones = []
        if (pk > 0):
            estadosinfracciones = list(estadoInfraccion.objects.filter(num=pk).values())
            if len(estadosinfracciones) > 0:
                estadoinfraccion = estadosinfracciones[0]
                datos = {'message': "Succes", 'estadoinfraccion': estadoinfraccion}
            else:
                datos = {'message': "estadoinfraccion not found..."}
            return JsonResponse(datos)
        else:
            estadosinfracciones = list(estadoInfraccion.objects.values())
            if len(estadosinfracciones) > 0:
                datos = {'message': "Succes", 'estadosinfracciones': estadosinfracciones}
            else:
                datos = {'message': "estadosinfracciones not found..."}
            return JsonResponse(datos)
        
    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        estadoInfraccion.objects.create(num=jd['num'], descripcion=jd['descripcion'])
        datos = {'message':"Succes"}
        return JsonResponse(datos)
    
    def put(self, request, pk=0):
        estadosinfracciones = []
        jd = json.loads(request.body, strict=False)
        estadosinfracciones = list(estadoInfraccion.objects.filter(num=pk).values())
        if len(estadosinfracciones) > 0:
            estadoinfraccion = estadoInfraccion.objects.get(num=pk)
            estadoinfraccion.num = jd['num']
            estadoinfraccion.descripcion = jd['descripcion']
            estadoinfraccion.save()
            datos = {'message':"Succes"}
            return JsonResponse(datos)
        else:
            datos = {'message': "estadoslicencias not found..."}
            return JsonResponse(datos)

    def delete(self, request, pk=0):
        estadosinfracciones = list(estadoInfraccion.objects.filter(num=pk).values())
        if len(estadosinfracciones) > 0:
            estadoInfraccion.objects.filter(num=pk).delete()
            datos = {'message':"Succes"}
        else:
            datos = {'message': "estadosinfracciones not found..."}
        return JsonResponse(datos)