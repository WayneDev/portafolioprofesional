from django.urls import path
from .views import *

app_name = "api"
urlpatterns = [
    # ////APIS GOD////

    # RUTAS API VEHICULOS
    path("vehiculos/", VehiculosView.as_view(), name='vehiculos_list'),  # Listar y procesar vehículos
    path("vehiculos/<int:num_serie>/", VehiculosView.as_view(), name='vehiculos_process'),
    # path("vehiculos_update/<int:pk>/", VehiculosView.as_view(), name='vehiculo_update'),
    path("countvehiculos/", VehiculosCountView.as_view(), name='vehiculos_count'),

    # RUTAS API MARCASVEHICULOS
    path("marcavehiculos/", MarcasVehiculosView.as_view(), name='marcasvehiculos_list'),  # Listar y procesar marcas de vehículos
    path('marcavehiculos/<int:pk>', MarcasVehiculosView.as_view(), name='marcasvehiculos_process'),

    # RUTAS API MODELOSVEHICULOS
    path("modelovehiculos/", ModelosVehiculosView.as_view(), name='modelosvehiculos_list'),  # Listar y procesar modelos de vehículos
    path("modelovehiculos/<int:pk>", ModelosVehiculosView.as_view(), name='modelosvehiculos_process'),

    # RUTAS API TIPOSVEHICULOS
    path("tiposvehiculos/", TiposVehiculosView.as_view(), name='tiposvehiculos_list'),  # Listar y procesar tipos de vehículos
    path("tiposvehiculos/<int:pk>", TiposVehiculosView.as_view(), name='tiposvehiculos_process'),

    # RUTAS API LICIENCIAS
    path("licencias/", LicenciasView.as_view(), name='licencias_list'),  # Listar y procesar licencias
    path("licencias/<int:pk>", LicenciasView.as_view(), name='licencias_process'),

    # RUTAS API TIPOSLICENCIAS
    path("tiposlicencias/", TiposLicenciasView.as_view(), name='tiposlicencias_list'),  # Listar y procesar tipos de licencias
    path("tiposlicencias/<int:pk>", TiposLicenciasView.as_view(), name='tiposlicencias_process'),

    # RUTAS API ROLES
    path("roles/", RolesView.as_view(), name='roles_list'),  # Listar y procesar roles
    path("roles/<int:pk>", RolesView.as_view(), name='roles_process'),

    # RUTAS API PARADAS
    path("paradas/", ParadasView.as_view(), name='paradas_list'),  # Listar y procesar paradas
    path("paradas/<int:pk>", ParadasView.as_view(), name='paradas_process'),

    # RUTAS API TIPOSCOBERTURA
    path("tiposcoberturas/", TiposCoberturaView.as_view(), name='tiposcoberturas_list'),  # Listar y procesar tipos de cobertura
    path("tiposcoberturas/<int:pk>", TiposCoberturaView.as_view(), name='tiposcoberturas_list_process'),

    # RUTAS API TALLERES
    path("talleres/", TalleresView.as_view(), name='talleres_list'),  # Listar y procesar talleres
    path("talleres/<int:pk>", TalleresView.as_view(), name='talleres_process'),

    # RUTAS API ASEGURADORAS
    path("aseguradoras/", AseguradorasView.as_view(), name='aseguradoras_list'),  # Listar y procesar aseguradoras
    path("aseguradoras/<int:pk>", AseguradorasView.as_view(), name='aseguradoras_process'),

    # RUTAS API RUTAS
    path("rutas/", RutasView.as_view(), name='rutas_list'),  # Listar y procesar rutas
    path("rutas/<int:pk>", RutasView.as_view(), name='rutas_process'),
    path("rutas_completadas/", RutasCompletadasCountView.as_view(), name='rutas_completadas'),
    

    # RUTAS API CARGASCOMBUSTIBLE
    path("cargascombustible/", CargasCombustibleView.as_view(), name='cargascombustible_list'),  # Listar y procesar cargas de combustible
    path("cargascombustible/<int:pk>", CargasCombustibleView.as_view(), name='cargascombustible_process'),

    # RUTAS API INFRACCIONES
    path("infracciones/", InfraccionesView.as_view(), name='infracciones_list'),  # Listar y procesar infracciones
    path("infracciones/<int:pk>", InfraccionesView.as_view(), name='infracciones_process'),

    # RUTAS API SESIONESMANTENIMIENTO
    path("sesionesmant/", SesionesMantView.as_view(), name='sesionesmant_list'),  # Listar y procesar sesiones de mantenimiento
    path("sesionesmant/<int:pk>", SesionesMantView.as_view(), name='sesionesmant_process'),

    # RUTAS API DOCUMENTOSVEHICULOS
    path("documentosvehicl/", DocumentosVehiculosView.as_view(), name='documentosvehicl_list'),  # Listar y procesar documentos de vehículos
    path("documentosvehicl/<int:pk>", DocumentosVehiculosView.as_view(), name='documentosvehicl_process'),

    # RUTAS API POLIZASSEGUROS
    path("polizasseg/", PolizasSegView.as_view(), name='polizasseg_list'),  # Listar y procesar pólizas de seguros
    path("polizasseg/<int:pk>", PolizasSegView.as_view(), name='polizasseg_process'),
    path("polizas_no_vencidas/", PolizasNoVencidasCountView.as_view, name='polizas_no_vencidas'),
    
    # RUTAS API RECLAMACIONESVEHICULOS
    path("reclamacionesseg/", ReclamacionesSegView.as_view(), name='reclamacionesseg_list'),  # Listar y procesar reclamaciones de vehículos
    path("reclamacionesseg/<int:pk>", ReclamacionesSegView.as_view(), name='reclamacionesseg_process'),

    # RUTAS PARA ESTADOS
    #ESTADOSRECLAMACIONES
    path("estadosreclamacion/", estadosReclamacionesView.as_view(), name='estadosreclamaciones_list'),
    path("estadosreclamacion/<int:pk>", estadosReclamacionesView.as_view(), name='estadosreclamaciones_process'),

    #ESTADOSLICENCIAS
    path("estadoslicencias/", estadosLicenciasView.as_view(), name='estadoslic_list'),
    path("estadoslicencias/<int:pk>", estadosLicenciasView.as_view(), name='estadoslic_process'),
]