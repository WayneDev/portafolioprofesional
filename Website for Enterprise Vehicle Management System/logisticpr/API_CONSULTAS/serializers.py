from rest_framework import serializers
from home.models import *

#SERIALIZACION DE TODOS LOS MODELOS.
class MarcaVehiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = marcasVehiculos
        fields = '__all__'

class RolSerializer(serializers.ModelSerializer):
    class Meta:
        model = roles
        fields = '__all__'

class ModeloVehiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = modelosVehiculos
        fields = '__all__'

class TipoVehiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = tiposVehiculos
        fields = '__all__'

class VehiculoSerializer(serializers.ModelSerializer):
    modelo = ModeloVehiculoSerializer()
    marca = MarcaVehiculoSerializer()
    tipo = TipoVehiculoSerializer()

    class Meta:
        model = vehiculos
        fields = '__all__'

class AseguradoraSerializer(serializers.ModelSerializer):
    class Meta:
        model = aseguradoras
        fields = '__all__'

class CargaCombustibleSerializer(serializers.ModelSerializer):
    vehiculo = VehiculoSerializer()

    class Meta:
        model = cargasCombustible
        fields = '__all__'

class DocumentoVehiculoSerializer(serializers.ModelSerializer):
    vehiculo = VehiculoSerializer()

    class Meta:
        model = documentosVehiculos
        fields = '__all__'

class EstadoInfraccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = estadoInfraccion
        fields = '__all__'

class EstadoLicenciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = estadosLicencias
        fields = '__all__'

class EstadoParadaSerializer(serializers.ModelSerializer):
    class Meta:
        model = estadosParadas
        fields = '__all__'

class EstadoReclamacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = estadosReclamaciones
        fields = '__all__'

class EstadoRutaSerializer(serializers.ModelSerializer):
    class Meta:
        model = estadosRutas
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = usuarios
        fields = '__all__'

class InfraccionSerializer(serializers.ModelSerializer):
    vehiculo = VehiculoSerializer()
    conductor = UserSerializer()

    class Meta:
        model = infracciones
        fields = '__all__'

class LicenciaSerializer(serializers.ModelSerializer):
    conductor = UserSerializer()
    tipo = serializers.StringRelatedField()

    class Meta:
        model = licencias
        fields = '__all__'

class ParadaSerializer(serializers.ModelSerializer):
    class Meta:
        model = paradas
        fields = '__all__'

class TipoLicenciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = tipoLicencias
        fields = '__all__'

class TipoCoberturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = tiposCoberturas
        fields = '__all__'

class TallerSerializer(serializers.ModelSerializer):
    class Meta:
        model = talleres
        fields = '__all__'

class PolizaSeguroSerializer(serializers.ModelSerializer):
    vehiculo = VehiculoSerializer()
    aseguradora = AseguradoraSerializer()
    cobertura = TipoCoberturaSerializer()

    class Meta:
        model = polizasSeguros
        fields = '__all__'

class ReclamacionSeguroSerializer(serializers.ModelSerializer):
    numPoliza = PolizaSeguroSerializer()

    class Meta:
        model = reclamacionesSeguros
        fields = '__all__'

class RutaSerializer(serializers.ModelSerializer):
    vehiculo = VehiculoSerializer()

    class Meta:
        model = rutas
        fields = '__all__'

class SesionMantenimientoSerializer(serializers.ModelSerializer):
    vehiculo = VehiculoSerializer()
    taller = TallerSerializer()

    class Meta:
        model = sesionesMantenimiento
        fields = '__all__'

class InfraccionEstadoInfraccionSerializer(serializers.ModelSerializer):
    infraccion = InfraccionSerializer()

    class Meta:
        model = InfraccionEstadoinfraccion
        fields = '__all__'

class LicenciaEstadoLicenciaSerializer(serializers.ModelSerializer):
    licencia = LicenciaSerializer()

    class Meta:
        model = LicenciasEstadoslic
        fields = '__all__'

class ParadaEstadoParadaSerializer(serializers.ModelSerializer):
    parada = ParadaSerializer()

    class Meta:
        model = ParadasEstadosparadas
        fields = '__all__'

class ReclamacionEstadoReclamacionSerializer(serializers.ModelSerializer):
    reclamacion = ReclamacionSeguroSerializer()

    class Meta:
        model = ReclamacionesEstadosrec
        fields = '__all__'

class RutaEstadoRutaSerializer(serializers.ModelSerializer):
    ruta = RutaSerializer()

    class Meta:
        model = RutasEstadosRutas
        fields = '__all__'

class RutaParadaSerializer(serializers.ModelSerializer):
    ruta = RutaSerializer()
    parada = ParadaSerializer()

    class Meta:
        model = RutasParadas
        fields = '__all__'

class VehiculoUsuarioSerializer(serializers.ModelSerializer):
    vehiculo = VehiculoSerializer()
    usuario = UserSerializer()

    class Meta:
        model = VehiculosUsuarios
        fields = '__all__'

#PARA LA CONSULTA 1
class VehiculoRutaSerializer(serializers.ModelSerializer):
    conductor = serializers.SerializerMethodField()
    num_serie_vehiculo = serializers.SerializerMethodField()
    nombre_marca = serializers.SerializerMethodField()
    nombre_modelo = serializers.SerializerMethodField()
    anio_vehiculo = serializers.SerializerMethodField()
    fecha_inicio_ruta = serializers.SerializerMethodField()
    paradas = serializers.SerializerMethodField()

    class Meta:
        model = rutas
        fields = ['num_serie_vehiculo', 'nombre_marca', 'nombre_modelo', 'anio_vehiculo',
                  'num', 'fechaHoraInicio', 'fechaHoraFin', 'comentarios', 'finalizada',
                  'conductor', 'fecha_inicio_ruta', 'paradas']

    def get_conductor(self, obj):
        vehiculo_usuario = VehiculosUsuarios.objects.get(vehiculo=obj.vehiculo)
        conductor = vehiculo_usuario.usuario
        return {
            'nombrePila': conductor.nombrePila,
            'apPaterno': conductor.apPaterno,
            'apMaterno': conductor.apMaterno,
            'numTel': conductor.numTel,
            'email': conductor.email,
        }

    def get_num_serie_vehiculo(self, obj):
        return obj.vehiculo.numSerie

    def get_nombre_marca(self, obj):
        return obj.vehiculo.marca.nombre

    def get_nombre_modelo(self, obj):
        return obj.vehiculo.modelo.nombre

    def get_anio_vehiculo(self, obj):
        return obj.vehiculo.fechaAdquisicion.year

    def get_fecha_inicio_ruta(self, obj):
        return obj.fechaHoraInicio

    def get_paradas(self, obj):
        # Obtener las paradas asociadas a la ruta a través del modelo intermedio RutasParadas
        rutas_paradas = RutasParadas.objects.filter(ruta=obj)
        # Serializar las paradas
        return [
            {
                'nombre': ruta_parada.parada.nombre,
                'direccion': f"{ruta_parada.parada.dirCalle}, {ruta_parada.parada.dirNumero}, {ruta_parada.parada.dirColonia}"
            }
            for ruta_parada in rutas_paradas
        ]

#PARA LA CONSULTA 2
class InfraccionesVehiculoSerializer(serializers.ModelSerializer):
    conductor = UserSerializer()
    estado = EstadoInfraccionSerializer(source='infraccionestadoinfraccion.estado')

    class Meta:
        model = infracciones
        fields = ['num', 'fechahora', 'dirCalle', 'dirNumero', 'dirColonia', 'monto', 'fechaLimitePago', 'esConductorResponsable', 'descripcion', 'conductor', 'estado']
    
    def get_nombre_conductor(self, obj):
        return f"{obj.conductor.nombrePila} {obj.conductor.apPaterno} {obj.conductor.apMaterno}"

    def get_estado_infraccion(self, obj):
        return obj.infraccionestadoinfraccion.estado.descripcion

#PARA LA CONSULTA 3