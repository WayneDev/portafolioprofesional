from django.apps import AppConfig


class ApiConsultasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'API_CONSULTAS'
