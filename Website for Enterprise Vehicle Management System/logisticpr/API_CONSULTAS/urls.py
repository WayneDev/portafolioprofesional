from django.urls import path
from . import views
from .views import *

app_name = "API_CONSULTAS"
urlpatterns = [
    #CONSULTA 1
    path('ruta_vehiculo/<int:num_serie>/', VehiculoRutaView.as_view(), name='vehiculo_ruta'),
    #CONSULTA 2
    path('infracciones_vehiculo/<int:num_vehiculo>/', InfraccionesVehiculoView.as_view(), name='infracciones_vehiculo'),
    #CONSULTA 3
    path('detalle_vehiculo/<int:num_serie>/', detalle_vehiculo, name='detalle_vehiculo'),
    #CONSULTA 4
    path('detalle_usuario/<int:usuario_id>/', detalle_usuario, name='detalle_usuario'),
    #CONSULTA 5
    path('detalle_cargas_combustible/<int:vehiculo_id>/', detalle_cargas_combustible, name='detalle_cargas_combustible'),
    #CONSULTA 6
    path('detalle_licencias/<int:tipo_licencia_id>/', detalle_licencias, name='detalle_licencias'),
    #CONSULTA 7
    path('detalle_reclamacion/<int:vehiculo_id>/', detalle_reclamacion, name='detalle_reclamacion'),
    #CONSULTA 8
    path('detalle_mantenimiento/<int:vehiculo_id>/', detalle_mantenimiento, name='detalle_mantenimiento'),
    #CONSULTA 9
    path('consulta_tipo_vehiculo/<int:tipo_id>/', consulta_tipo_vehiculo, name='consulta_tipo_vehiculo'),
    #CONSULTA 10
    path('consulta_conductores/', consulta_conductores, name='consulta_conductores'),
]