from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets, views
from home.models import *
from .serializers import *
from rest_framework import generics
from rest_framework.generics import RetrieveAPIView, ListAPIView
from django.db.models import F, ExpressionWrapper, fields, Value
from django.db.models.functions import TruncDate, Cast
from django.shortcuts import get_object_or_404
from django.db.models.functions import Concat, Cast, Extract
from django.db.models.fields import CharField
from django.db.models.functions import ExtractYear


#CONSULTA1
class VehiculoRutaView(APIView):
    def get(self, request, num_serie):
        try:
            # Obtener el vehículo por número de serie
            vehiculo = vehiculos.objects.get(numSerie=num_serie)
            
            # Obtener la información relacionada con la ruta
            vehiculo_usuario = VehiculosUsuarios.objects.get(vehiculo=vehiculo)
            ruta = rutas.objects.filter(vehiculo=vehiculo).latest('fechaHoraInicio')
            
            # Serializar la información
            serializer = VehiculoRutaSerializer(ruta)
            
            # Devolver la respuesta
            return Response(serializer.data, status=status.HTTP_200_OK)
        
        except vehiculos.DoesNotExist:
            return Response({"error": "Vehículo no encontrado"}, status=status.HTTP_404_NOT_FOUND)
        
        except VehiculosUsuarios.DoesNotExist:
            return Response({"error": "Vehículo no asociado a un usuario"}, status=status.HTTP_404_NOT_FOUND)
        
        except rutas.DoesNotExist:
            return Response({"error": "Ruta no encontrada para el vehículo"}, status=status.HTTP_404_NOT_FOUND)

#CONSULTA 2
class InfraccionesVehiculoView(APIView):
    def get(self, request, num_vehiculo):
        try:
            infracciones_vehiculo = infracciones.objects.filter(vehiculo__num=num_vehiculo)
            serializer = InfraccionesVehiculoSerializer(infracciones_vehiculo, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except infracciones.DoesNotExist:
            return Response({"message": "No se encontraron infracciones para el vehículo especificado."}, status=status.HTTP_404_NOT_FOUND)

#CONSULTA 3
def detalle_vehiculo(request, num_serie):
    data = vehiculos.objects.filter(numSerie=num_serie).annotate(
        NoSerie=F('numSerie'),
        Marca=F('marca__nombre'),
        Modelo=F('modelo__nombre'),
        AnioVehiculo=Cast(TruncDate('fechaAdquisicion'), output_field=models.DateField()),
        TipoVehiculo=F('tipo__nombre'),
        Documento=F('documentosvehiculos__tipo'),
        FechaEmisionDocumento=Cast(TruncDate('documentosvehiculos__fechaEmision'), output_field=models.DateField()),
        FechaVencimientoDocumento=Cast(TruncDate('documentosvehiculos__fechaVencimiento'), output_field=models.DateField()),
        Poliza=F('polizasseguros__numPoliza'),
        FechaInicioPoliza=Cast(TruncDate('polizasseguros__fechaInicio'), output_field=models.DateField()),
        FechaVencimientoPoliza=Cast(TruncDate('polizasseguros__fechaVencimiento'), output_field=models.DateField()),
        Cobertura=F('polizasseguros__montoCobertura'),
        DescripcionCobertura=F('polizasseguros__cobertura__nombre'),
        Aseguradora=F('polizasseguros__aseguradora__nombre')
    ).values('NoSerie', 'Marca', 'Modelo', 'AnioVehiculo', 'TipoVehiculo', 'Documento',
             'FechaEmisionDocumento', 'FechaVencimientoDocumento', 'Poliza', 'FechaInicioPoliza',
             'FechaVencimientoPoliza', 'Cobertura', 'DescripcionCobertura', 'Aseguradora').first()

    return JsonResponse(data, safe=False)

#CONSULTA 4
def detalle_usuario(request, usuario_id):
    data = usuarios.objects.filter(num=usuario_id).annotate(
        Conductor=Concat('nombrePila', Value(' '), 'apPaterno', Value(' '), 'apMaterno'),
        Telefono=F('numTel'),
        Correo=F('email'),
        Rol=F('rol__nombre'),
        Licencia=F('licencias__num'),
        FechaExpedicionLicencia=Cast('licencias__fechaExpedicion', output_field=CharField()),
        FechaVencimientoLicencia=Cast('licencias__fechaVencimiento', output_field=CharField()),
        TipoLicencia=F('licencias__tipo__nombre'),
        EstadoLicencia=F('licencias__licenciasestadoslic__estado__descripcion')
    ).values('Conductor', 'Telefono', 'Correo', 'Rol', 'Licencia', 'FechaExpedicionLicencia',
             'FechaVencimientoLicencia', 'TipoLicencia', 'EstadoLicencia').first()

    return JsonResponse(data, safe=False)

#CONSULTA 5
def detalle_cargas_combustible(request, vehiculo_id):
    data = cargasCombustible.objects.filter(vehiculo__num=vehiculo_id, fecha__month=6).annotate(
        NoSerie=F('vehiculo__numSerie'),
        Marca=F('vehiculo__marca__nombre'),
        Modelo=F('vehiculo__modelo__nombre'),
        Folio=F('numFolio'),
        FechaCargaCombustible=F('fecha'),
        LitrosCargados=F('litrosCargados'),
        Total=F('costoTotal')
    ).values('NoSerie', 'Marca', 'Modelo', 'Folio', 'FechaCargaCombustible', 'LitrosCargados', 'Total').order_by('FechaCargaCombustible')

    return JsonResponse(list(data), safe=False)

#CONSULTA 6
def detalle_licencias(request, tipo_licencia_id):
    data = licencias.objects.filter(tipo__num=tipo_licencia_id).annotate(
        TipoLicencia=F('tipo__nombre'),
        Conductor=Concat('conductor__nombrePila', F('conductor__apPaterno'), F('conductor__apMaterno')),
        Rol=F('conductor__rol__nombre'),
        EstadoLicencia=F('licenciasestadoslic__estado__descripcion')
    ).values('TipoLicencia', 'Conductor', 'Rol', 'EstadoLicencia')

    return JsonResponse(list(data), safe=False)

#CONSULTA 7
def detalle_reclamacion(request, vehiculo_id):
    data = reclamacionesSeguros.objects.filter(numPoliza__vehiculo__num=vehiculo_id).annotate(
        NoSerie=F('numPoliza__vehiculo__numSerie'),
        Marca=F('numPoliza__vehiculo__marca__nombre'),
        Modelo=F('numPoliza__vehiculo__modelo__nombre'),
        AnioVehiculo=Extract('numPoliza__vehiculo__fechaAdquisicion', 'year'),
        NumPoliza=F('numPoliza__numPoliza'),
        Aseguradora=F('numPoliza__aseguradora__nombre'),
        NumFolioReclamacion=F('numFolio'),
        FechaReclamacion=F('fecha'),
        DescripcionIncidente=F('descripcionIncidente'),
        MontoReclamo=F('montoReclamo'),
        EstadoReclamacion=F('reclamacionesestadosrec__estado__descripcion')
    ).values('NoSerie', 'Marca', 'Modelo', 'AnioVehiculo', 'NumPoliza', 'Aseguradora', 'NumFolioReclamacion', 'FechaReclamacion', 'DescripcionIncidente', 'MontoReclamo', 'EstadoReclamacion').order_by('fecha')

    return JsonResponse(list(data), safe=False)

#CONSULTA 8
def detalle_mantenimiento(request, vehiculo_id):
    data = sesionesMantenimiento.objects.filter(vehiculo__num=vehiculo_id).annotate(
        NoSerie=F('vehiculo__numSerie'),
        Marca=F('vehiculo__marca__nombre'),
        Modelo=F('vehiculo__modelo__nombre'),
        AnioVehiculo=Extract('vehiculo__fechaAdquisicion', 'year'),
        Taller=F('taller__nombre'),
        FechaMantenimiento=F('fecha'),
        DescripcionMantenimiento=F('descripcion'),
        Total=F('costoTotal')
    ).values('NoSerie', 'Marca', 'Modelo', 'AnioVehiculo', 'Taller', 'FechaMantenimiento', 'DescripcionMantenimiento', 'Total').order_by('fecha')

    return JsonResponse(list(data), safe=False)

#CONSULTA 9 
def consulta_tipo_vehiculo(request, tipo_id):
    data = vehiculos.objects.filter(tipo__num=tipo_id).annotate(
        TipoVehiculo=F('tipo__nombre'),
        NumSerie=F('numSerie'),
        Marca=F('marca__nombre'),
        Modelo=F('modelo__nombre'),
        AnioVehiculo=ExtractYear('fechaAdquisicion')
    ).values('TipoVehiculo', 'NumSerie', 'Marca', 'Modelo', 'AnioVehiculo')

    return JsonResponse(list(data), safe=False)

#CONSULTA 10
def consulta_conductores(request):
    conductores_data = usuarios.objects.filter(rol__nombre='Conductor').values(
        'rol__nombre', 'num', 'nombrePila', 'apPaterno', 'apMaterno', 'numTel', 'email'
    )

    return JsonResponse(list(conductores_data), safe=False)
