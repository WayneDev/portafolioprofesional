from django import forms
from .models import *
from django.forms import widgets

# ModelosVehiculos
class UpdateModelosVehiculosForm(forms.ModelForm):
    class Meta:
        model = modelosVehiculos
        fields = [
            'nombre',
            'consumoConbustible',
            'cantCarga',
            'capPasajero'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

class NewModelosVehiculosForm(forms.ModelForm):
    class Meta:
        model = modelosVehiculos
        fields = [
            'nombre',
            'consumoConbustible',
            'cantCarga',
            'capPasajero'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

# MarcasVehiculos
class UpdateMarcasVehiculosForm(forms.ModelForm):
    class Meta:
        model = marcasVehiculos
        fields = [
            'nombre'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

class NewMarcasVehiculosForm(forms.ModelForm):
    class Meta:
        model = marcasVehiculos
        fields = [
            'nombre'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

# TiposVehiculos
class UpdateTiposVehiculosForm(forms.ModelForm):
    class Meta:
        model = tiposVehiculos
        fields = [
            'nombre'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

class NewTiposVehiculosForm(forms.ModelForm):
    class Meta:
        model = tiposVehiculos
        fields = [
            'nombre'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

# Talleres
class UpdateTalleresForm(forms.ModelForm):
    class Meta:
        model = talleres
        fields = [
            'nombre',
            'correo',
            'numTel',
            'dirCalle',
            'dirNumero',
            'dirColonia'
        ]

class NewTalleresForm(forms.ModelForm):
    class Meta:
        model = talleres
        fields = [
            'nombre',
            'correo',
            'numTel',
            'dirCalle',
            'dirNumero',
            'dirColonia'
        ]

# Paradas
class UpdateParadasForm(forms.ModelForm):
    class Meta:
        model = paradas
        fields = [
            'nombre',
            'dirColonia',
            'dirCalle',
            'dirNumero'
        ]

class NewParadasForm(forms.ModelForm):
    class Meta:
        model = paradas
        fields = [
            'nombre',
            'dirColonia',
            'dirCalle',
            'dirNumero'
        ]

# Aseguradoras
class UpdateAseguradorasForm(forms.ModelForm):
    class Meta:
        model = aseguradoras
        fields = [
            'nombre',
            'dirColonia',
            'dirCalle',
            'dirNumero',
            'numTel',
            'correo'
        ]

class NewAseguradorasForm(forms.ModelForm):
    class Meta:
        model = aseguradoras
        fields = [
            'nombre',
            'dirColonia',
            'dirCalle',
            'dirNumero',
            'numTel',
            'correo'
        ]

# Roles
class UpdateRolesForm(forms.ModelForm):
    class Meta:
        model = roles
        fields = [
            'num',
            'nombre'
        ]

class NewRolesForm(forms.ModelForm):
    class Meta:
        model = roles
        fields = [
            'num',
            'nombre'
        ]

# TiposCoberturas
class UpdateTiposCoberturasForm(forms.ModelForm):
    class Meta:
        model = tiposCoberturas
        fields = [
            'num',
            'nombre'
        ]

class NewTiposCoberturasForm(forms.ModelForm):
    class Meta:
        model = tiposCoberturas
        fields = [
            'num',
            'nombre'
        ]

# EstadosRutas
class UpdateEstadosRutasForm(forms.ModelForm):
    class Meta:
        model = estadosRutas
        fields = [
            'num',
            'descripcion'
        ]

class NewEstadosRutasForm(forms.ModelForm):
    class Meta:
        model = estadosRutas
        fields = [
            'descripcion'
        ]

# EstadosReclamaciones
class UpdateEstadosReclamacionesForm(forms.ModelForm):
    class Meta:
        model = estadosReclamaciones
        fields = [
            'descripcion'
        ]

class NewEstadosReclamacionesForm(forms.ModelForm):
    class Meta:
        model = estadosReclamaciones
        fields = [
            'descripcion'
        ]

# EstadosParadas
class UpdateEstadosParadasForm(forms.ModelForm):
    class Meta:
        model = estadosParadas
        fields = [
            'descripcion'
        ]

class NewEstadosParadasForm(forms.ModelForm):
    class Meta:
        model = estadosParadas
        fields = [
            'descripcion'
        ]

# EstadosLicencias
class UpdateEstadosLicenciasForm(forms.ModelForm):
    class Meta:
        model = estadosLicencias
        fields = [
            'descripcion'
        ]

class NewEstadosLicenciasForm(forms.ModelForm):
    class Meta:
        model = estadosLicencias
        fields = [
            'descripcion'
        ]

# EstadoInfraccion
class UpdateEstadoInfraccionForm(forms.ModelForm):
    class Meta:
        model = estadoInfraccion
        fields = [
            'descripcion'
        ]

class NewEstadoInfraccionForm(forms.ModelForm):
    class Meta:
        model = estadoInfraccion
        fields = [
            'descripcion'
        ]

# Vehiculos
class UpdateVehiculosForm(forms.ModelForm):
    class Meta:
        model = vehiculos
        fields = [
            'numSerie',
            'placas',
            'kilometraje',
            'fechaAdquisicion',
            'usaDiesel',
            'modelo',
            'marca',
            'tipo'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

class NewVehiculosForm(forms.ModelForm):
    class Meta:
        model = vehiculos
        fields = [
            'numSerie',
            'placas',
            'kilometraje',
            'fechaAdquisicion',
            'usaDiesel',
            'modelo',
            'marca',
            'tipo'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

# DocumentosVehiculos
class UpdateDocumentosVehiculosForm(forms.ModelForm):
    class Meta:
        model = documentosVehiculos
        fields = [
            'tipo',
            'fechaEmision',
            'fechaVencimiento',
            'comentarios',
            'vehiculo'
        ]

class NewDocumentosVehiculosForm(forms.ModelForm):
    class Meta:
        model = documentosVehiculos
        fields = [
            'tipo',
            'fechaEmision',
            'fechaVencimiento',
            'comentarios',
            'vehiculo'
        ]

# CargasCombustible
class UpdateCargasCombustibleForm(forms.ModelForm):
    class Meta:
        model = cargasCombustible
        fields = [
            'numFolio',
            'litrosCargados',
            'fecha',
            'kilometraje',
            'comentarios',
            'costoTotal',
            'vehiculo'
        ]

class NewCargasCombustibleForm(forms.ModelForm):
    class Meta:
        model = cargasCombustible
        fields = [
            'numFolio',
            'litrosCargados',
            'fecha',
            'kilometraje',
            'comentarios',
            'costoTotal',
            'vehiculo'
        ]

# Rutas
class UpdateRutasForm(forms.ModelForm):
    class Meta:
        model = rutas
        fields = [
            'fechaHoraInicio',
            'fechaHoraFin',
            'comentarios',
            'finalizada',
            'vehiculo'
        ]

class NewRutasForm(forms.ModelForm):
    class Meta:
        model = rutas
        fields = [
            'fechaHoraInicio',
            'fechaHoraFin',
            'comentarios',
            'finalizada',
            'vehiculo'
        ]

# PolizasSeguros
class UpdatePolizasSegurosForm(forms.ModelForm):
    class Meta:
        model = polizasSeguros
        fields = [
            'fechaInicio',
            'fechaVencimiento',
            'montoCobertura',
            'costo',
            'frecuenciaPago',
            'vehiculo',
            'aseguradora',
            'cobertura'
        ]

class NewPolizasSegurosForm(forms.ModelForm):
    class Meta:
        model = polizasSeguros
        fields = [
            'fechaInicio',
            'fechaVencimiento',
            'montoCobertura',
            'costo',
            'frecuenciaPago',
            'vehiculo',
            'aseguradora',
            'cobertura'
        ]

# ReclamacionesSeguros
class UpdateReclamacionesSegurosForm(forms.ModelForm):
    class Meta:
        model = reclamacionesSeguros
        fields = [
            'fecha',
            'descripcionIncidente',
            'montoReclamo',
            'numPoliza'
        ]

class NewReclamacionesSegurosForm(forms.ModelForm):
    class Meta:
        model = reclamacionesSeguros
        fields = [
            'fecha',
            'descripcionIncidente',
            'montoReclamo',
            'numPoliza'
        ]

# SesionesMantenimiento
class UpdateSesionesMantenimientoForm(forms.ModelForm):
    class Meta:
        model = sesionesMantenimiento
        fields = [
            'fecha',
            'kilometraje',
            'costoTotal',
            'descripcion',
            'vehiculo',
            'taller'
        ]

class NewSesionesMantenimientoForm(forms.ModelForm):
    class Meta:
        model = sesionesMantenimiento
        fields = [
            'fecha',
            'kilometraje',
            'costoTotal',
            'descripcion',
            'vehiculo',
            'taller'
        ]

# Usuarios

# Licencias
class NewLicenciaForm(forms.ModelForm):
    class Meta:
        model = licencias
        fields = [
            'fechaExpedicion',
            'fechaVencimiento',
            'comentarios',
            'conductor',
            'tipo'
        ]

class UpdateLicenciaForm(forms.ModelForm):
    class Meta:
        model = licencias
        fields = [
            'fechaExpedicion',
            'fechaVencimiento',
            'comentarios',
            'conductor',
            'tipo'
        ]

# TipoLicencias
class NewTipoLicenciaForm(forms.ModelForm):
    class Meta:
        model = tipoLicencias
        fields = [
            'nombre'
        ]

class UpdateTipoLicenciaForm(forms.ModelForm):
    class Meta:
        model = tipoLicencias
        fields = [
            'nombre'
        ]

# Infracciones
class NewInfraccionForm(forms.ModelForm):
    class Meta:
        model = infracciones
        fields = [
            'fechahora',
            'dirCalle',
            'dirNumero',
            'dirColonia',
            'monto',
            'fechaLimitePago',
            'esConductorResponsable',
            'vehiculo',
            'conductor'
        ]

class UpdateInfraccionForm(forms.ModelForm):
    class Meta:
        model = infracciones
        fields = [
            'fechahora',
            'dirCalle',
            'dirNumero',
            'dirColonia',
            'monto',
            'fechaLimitePago',
            'esConductorResponsable',
            'vehiculo',
            'conductor'
        ]