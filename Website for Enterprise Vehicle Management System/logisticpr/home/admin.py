from django.contrib import admin
from django.db import models
from .models import *

# ModelosVehiculos
@admin.register(modelosVehiculos)
class ModelosVehiculosAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre', 'consumoConbustible', 'cantCarga', 'capPasajero']

# MarcasVehiculos
@admin.register(marcasVehiculos)
class MarcasVehiculosAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre']

# TiposVehiculos
@admin.register(tiposVehiculos)
class TiposVehiculosAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre']

# Talleres
@admin.register(talleres)
class TalleresAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre', 'correo', 'numTel', 'dirCalle', 'dirNumero', 'dirColonia']

# Paradas
@admin.register(paradas)
class ParadasAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre', 'dirColonia', 'dirCalle', 'dirNumero']

# Aseguradoras
@admin.register(aseguradoras)
class AseguradorasAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre', 'dirColonia', 'dirCalle', 'dirNumero', 'numTel', 'correo']

# Roles
@admin.register(roles)
class RolesAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre']

# TiposCoberturas
@admin.register(tiposCoberturas)
class TiposCoberturasAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre']

# EstadosRutas
@admin.register(estadosRutas)
class EstadosRutasAdmin(admin.ModelAdmin):
    list_display = ['num', 'descripcion']

# EstadosReclamaciones
@admin.register(estadosReclamaciones)
class EstadosReclamacionesAdmin(admin.ModelAdmin):
    list_display = ['num', 'descripcion']

# EstadosParadas
@admin.register(estadosParadas)
class EstadosParadasAdmin(admin.ModelAdmin):
    list_display = ['num', 'descripcion']

# EstadosLicencias
@admin.register(estadosLicencias)
class EstadosLicenciasAdmin(admin.ModelAdmin):
    list_display = ['num', 'descripcion']

# EstadoInfraccion
@admin.register(estadoInfraccion)
class EstadoInfraccionAdmin(admin.ModelAdmin):
    list_display = ['num', 'descripcion']

# Vehiculos
@admin.register(vehiculos)
class VehiculosAdmin(admin.ModelAdmin):
    list_display = ['num', 'numSerie', 'placas', 'kilometraje', 'fechaAdquisicion', 'usaDiesel', 'modelo', 'marca', 'tipo']

# DocumentosVehiculos
@admin.register(documentosVehiculos)
class DocumentosVehiculosAdmin(admin.ModelAdmin):
    list_display = ['num', 'tipo', 'fechaEmision', 'fechaVencimiento', 'comentarios', 'vehiculo']

# CargasCombustible
@admin.register(cargasCombustible)
class CargasCombustibleAdmin(admin.ModelAdmin):
    list_display = ['num', 'numFolio', 'litrosCargados', 'fecha', 'kilometraje', 'comentarios', 'costoTotal', 'vehiculo']

# Rutas
@admin.register(rutas)
class RutasAdmin(admin.ModelAdmin):
    list_display = ['num', 'fechaHoraInicio', 'fechaHoraFin', 'comentarios', 'finalizada', 'vehiculo']

# PolizasSeguros
@admin.register(polizasSeguros)
class PolizasSegurosAdmin(admin.ModelAdmin):
    list_display = ['numPoliza', 'fechaInicio', 'fechaVencimiento', 'montoCobertura', 'costo', 'frecuenciaPago', 'vehiculo', 'aseguradora', 'cobertura']

# ReclamacionesSeguros
@admin.register(reclamacionesSeguros)
class ReclamacionesSegurosAdmin(admin.ModelAdmin):
    list_display = ['numFolio', 'fecha', 'descripcionIncidente', 'montoReclamo', 'numPoliza']

# SesionesMantenimiento
@admin.register(sesionesMantenimiento)
class SesionesMantenimientoAdmin(admin.ModelAdmin):
    list_display = ['num', 'fecha', 'kilometraje', 'costoTotal', 'descripcion', 'vehiculo', 'taller']

# Licencias
@admin.register(licencias)
class LicenciasAdmin(admin.ModelAdmin):
    list_display = ['num', 'fechaExpedicion', 'fechaVencimiento', 'comentarios', 'conductor', 'tipo']

# TipoLicencias
@admin.register(tipoLicencias)
class TipoLicenciasAdmin(admin.ModelAdmin):
    list_display = ['num', 'nombre']

# Infracciones
@admin.register(infracciones)
class InfraccionesAdmin(admin.ModelAdmin):
    list_display = ['num', 'fechahora', 'dirCalle', 'dirNumero', 'dirColonia', 'monto', 'fechaLimitePago', 'esConductorResponsable', 'descripcion', 'vehiculo', 'conductor']

# InfraccionEstadoinfraccion
@admin.register(InfraccionEstadoinfraccion)
class InfraccionEstadoinfraccionAdmin(admin.ModelAdmin):
    list_display = ['infraccion', 'estado', 'fechacambio']

# LicenciasEstadoslic
@admin.register(LicenciasEstadoslic)
class LicenciasEstadoslicAdmin(admin.ModelAdmin):
    list_display = ['licencia', 'estado', 'fechacambio']

# ParadasEstadosparadas
@admin.register(ParadasEstadosparadas)
class ParadasEstadosparadasAdmin(admin.ModelAdmin):
    list_display = ['parada', 'estado', 'fechacambio']

# ReclamacionesEstadosrec
@admin.register(ReclamacionesEstadosrec)
class ReclamacionesEstadosrecAdmin(admin.ModelAdmin):
    list_display = ['reclamacion', 'estado', 'fechacambio']

# RutasEstadosRutas
@admin.register(RutasEstadosRutas)
class RutasEstadosrutasAdmin(admin.ModelAdmin):
    list_display = ['ruta', 'estado', 'fechacambio']

# RutasParadas
@admin.register(RutasParadas)
class RutasParadasAdmin(admin.ModelAdmin):
    list_display = ['ruta', 'parada', 'numparada', 'finalizada']

# VehiculosUsuarios
@admin.register(VehiculosUsuarios)
class VehiculosUsuariosAdmin(admin.ModelAdmin):
    list_display = ['vehiculo', 'usuario']
