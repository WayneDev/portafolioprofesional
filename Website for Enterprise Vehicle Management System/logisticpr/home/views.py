from typing import Any
from django import http
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http.response import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.views import generic, View
from django.views.generic import ListView
from django.urls import reverse_lazy
from home.models import *
from home.forms import *
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
import json

# Create your views here.
# def index(request):
#   return render(request, "home/index.html", {})

#/////////////////ANTIGUO//////////////////////
#PLANTILLA RETORNO
class Base(generic.View):
    template_name="base/base.html"
    context= {}

    def get(self, request):
        return render(request, self.template_name, self.context)

#PLANTILLA PARA VEHICULO
#HOME/VEHICULO
class NewVehiculo(generic.CreateView):
    template_name = "home/new_vehiculo.html"
    model = vehiculos
    form_class = NewVehiculosForm
    success_url = reverse_lazy("home:vehiculoindex")#Aqui es a donde quieras retornar cuando se delete

class ListarVehiculo(generic.View):
    template_name = "home/vehiculos.html"
    context = {}

    def get(self, request):
        self.context = {
            "vehiculo": vehiculos.objects.all()
        }
        return render(request, self.template_name, self.context)
    
class DetailVehiculo(generic.View):
    template_name = "home/detail_vehiculo.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "vehiculo": get_object_or_404(vehiculos, numSerie=pk)
        }
        return render(request, self.template_name, self.context)

class DeleteVehiculo(generic.DeleteView):
    template_name = "home/delete_vehiculo.html"
    model = vehiculos
    success_url = reverse_lazy("home:vehiculoindex")
    success_message = "Vehiculo was deleted successfully."

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)

class UpdateVehiculo(generic.UpdateView):
    template_name = "home/update_vehiculo.html"
    model = vehiculos
    form_class = UpdateVehiculosForm
    success_url = reverse_lazy("home:vehiculoindex")

    def get_object(self, queryset=None):
        return get_object_or_404(vehiculos, numSerie=self.kwargs['pk'])
    

#PLANTILLA PARA MARCAVEHICULO
class NewMarcaVehiculo(generic.CreateView):
    template_name = "home/new_marcavehiculo.html"
    model = marcasVehiculos
    form_class = NewMarcasVehiculosForm
    success_url = reverse_lazy("home:marcavehiculoindex")

class ListarMarcaVehiculo(generic.View):
    template_name = "home/marcavehiculos.html"
    context = {}

    def get(self, request):
        self.context = {
            "marcavehiculo": marcasVehiculos.objects.all()
        }
        return render(request, self.template_name, self.context)

class DetailMarcaVehiculo(generic.View):
    template_name = "home/detail_marcavehiculo.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "marcavehiculo": get_object_or_404(marcasVehiculos, num=pk)
        }
        return render(request, self.template_name, self.context)

class DeleteMarcaVehiculo(generic.DeleteView):
    template_name = "home/delete_marcavehiculo.html"
    model = marcasVehiculos
    success_url = reverse_lazy("home:marcavehiculoindex")
    
class UpdateMarcaVehiculo(generic.UpdateView):
    template_name = "home/update_marcavehiculo.html"
    model = marcasVehiculos
    form_class = UpdateMarcasVehiculosForm
    success_url = reverse_lazy("home:marcavehiculoindex")
    

#PLANTILLA PARA MODELOVEHICULO
class NewModeloVehiculo(generic.CreateView):
    template_name = "home/new_modelovehiculo.html"
    model = modelosVehiculos
    form_class = NewModelosVehiculosForm
    success_url = reverse_lazy("home:modelovehiculoindex")

class ListarModeloVehiculo(generic.View):
    template_name = "home/modelovehiculos.html"
    context = {}

    def get(self, request):
        self.context = {
            "modelovehiculo": modelosVehiculos.objects.all()
        }
        return render(request, self.template_name, self.context)

class DetailModeloVehiculo(generic.View):
    template_name = "home/detail_modelovehiculo.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "modelovehiculo": get_object_or_404(modelosVehiculos, num=pk)
        }
        return render(request, self.template_name, self.context)

class DeleteModeloVehiculo(generic.DeleteView):
    template_name = "home/delete_modelovehiculo.html"
    model = modelosVehiculos
    success_url = reverse_lazy("home:modelovehiculoindex")
    
class UpdateModeloVehiculo(generic.UpdateView):
    template_name = "home/update_modelovehiculo.html"
    model = modelosVehiculos
    form_class = UpdateModelosVehiculosForm
    success_url = reverse_lazy("home:modelovehiculoindex")