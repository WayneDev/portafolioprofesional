from django.urls import path
from home import views
from home.views import *
from django.views.generic import TemplateView, CreateView, View, ListView, DetailView, DeleteView, UpdateView

app_name = "home"
urlpatterns = [
    # VISTAS PARA VEHICULOS
    path('', views.Base.as_view(), name="base"),
    path('vehiculo/', views.ListarVehiculo.as_view(), name='vehiculoindex'),
    path('new/vehiculo/', views.NewVehiculo.as_view(), name='new-vehiculo'),
    path('delete/vehiculo/<str:pk>/', views.DeleteVehiculo.as_view(), name='delete-vehiculo'),
    path('update/vehiculo/<str:pk>/', views.UpdateVehiculo.as_view(), name='update-vehiculo'),
    path('detail/vehiculo/<str:pk>/', views.DetailVehiculo.as_view(), name='detail-vehiculo'),

    # VISTAS PARA MODELOVEHICULO
    path('modelovehiculo/', views.ListarModeloVehiculo.as_view(), name='modelovehiculoindex'),
    path('new/modelovehiculo/', views.NewModeloVehiculo.as_view(), name='new-modelovehiculo'),
    path('update/modelovehiculo/<int:pk>/', views.UpdateModeloVehiculo.as_view(), name='update-modelovehiculo'),
    path('delete/modelovehiculo/<int:pk>/', views.DeleteModeloVehiculo.as_view(), name='delete-modelovehiculo'),
    path('detail/modelovehiculo/<int:pk>/', views.DetailModeloVehiculo.as_view(), name='detail-modelovehiculo'),

    # VISTAS PARA MARCAVEHICULO
    path('marcavehiculo/', views.ListarMarcaVehiculo.as_view(), name='marcavehiculoindex'),
    path('new/marcavehiculo/', views.NewMarcaVehiculo.as_view(), name='new-marcavehiculo'),
    path('delete/marcavehiculo/<int:pk>/', views.DeleteMarcaVehiculo.as_view(), name='delete-marcavehiculo'),
    path('update/marcavehiculo/<int:pk>/', views.UpdateMarcaVehiculo.as_view(), name='update-marcavehiculo'),
    path('detail/marcavehiculo/<int:pk>/', views.DetailMarcaVehiculo.as_view(), name='detail-marcavehiculo'),
]


