# Importaciones de Django para modelos y funciones relacionadas con la autenticación.
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
# Importación de modelos específicos del proyecto.s
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from datetime import time, timedelta
from django.dispatch import receiver

class marcasVehiculos(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)

class roles(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)

class modelosVehiculos(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    consumoConbustible = models.IntegerField()
    cantCarga = models.IntegerField()
    capPasajero = models.IntegerField()

class tiposVehiculos(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)

class vehiculos(models.Model):
    num = models.AutoField(primary_key=True)
    numSerie = models.IntegerField(unique=True)
    placas = models.CharField(max_length=7)
    kilometraje = models.IntegerField()
    fechaAdquisicion = models.DateField()
    usaDiesel = models.BooleanField()
    modelo = models.ForeignKey(modelosVehiculos, on_delete=models.CASCADE, null=False)
    marca = models.ForeignKey(marcasVehiculos, on_delete=models.CASCADE, null=False)
    tipo = models.ForeignKey(tiposVehiculos, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return str(self.numSerie)
    
class aseguradoras(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    dirColonia = models.CharField(max_length=50)
    dirCalle = models.CharField(max_length=50)
    dirNumero = models.CharField(max_length=50)
    numTel = models.CharField(max_length=15)
    correo = models.CharField(max_length=100)

class cargasCombustible(models.Model):
    num = models.AutoField(primary_key=True)
    numFolio = models.IntegerField()
    litrosCargados = models.DecimalField(max_digits=10, decimal_places=2)
    fecha = models.DateField()
    kilometraje = models.IntegerField()
    comentarios = models.TextField()
    costoTotal = models.DecimalField(max_digits=10, decimal_places=2)
    vehiculo = models.ForeignKey(vehiculos, on_delete=models.CASCADE, null=False)

class documentosVehiculos(models.Model):
    num = models.AutoField(primary_key=True)
    tipo = models.CharField(max_length=50)
    fechaEmision = models.DateField()
    fechaVencimiento = models.DateField()
    comentarios = models.TextField()
    vehiculo = models.ForeignKey(vehiculos, on_delete=models.CASCADE, null=False)

#ESTADOS
class estadoInfraccion(models.Model):
    num = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=10)

class estadosLicencias(models.Model):
    num = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=10)

class estadosParadas(models.Model):
    num = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=10)

class estadosReclamaciones(models.Model):
    num = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=10)

class estadosRutas(models.Model):
    num = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=10)
#FIN ESTADOS

# Definición del administrador de usuario personalizado.
class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        # Validación para asegurarse de que se proporcione un correo electrónico.
        if not email:
            raise ValueError("The email is not given.")
        
        # Normaliza el correo electrónico para garantizar consistencia.
        email = self.normalize_email(email)
        
        # Crea un nuevo usuario con los campos proporcionados.
        user = self.model(email=email, **extra_fields)
        user.is_active = False
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, rol, **extra_fields):
        # Configuración predeterminada para un superusuario.
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        # Validación para asegurarse de que el superusuario tenga los atributos correctos.
        if not extra_fields.get('is_staff'):
            raise ValueError("Superuser must have is_staff = True")

        if not extra_fields.get('is_superuser'):
            raise ValueError("Superuser must have is_superuser = True")

        # Llama a la función create_user para crear un superusuario.
        return self.create_user(email, password, **extra_fields)

# Definición del modelo de usuario personalizado.
class usuarios(AbstractBaseUser):
    # Campos del modelo.
    num = models.AutoField(primary_key=True)
    email = models.EmailField(max_length=254, unique=True)
    password = models.CharField(max_length=128, null=True)
    nombrePila = models.CharField(max_length=255, null=True, blank=True)
    apPaterno = models.CharField(max_length=255, null=True, blank=True)
    apMaterno = models.CharField(max_length=255, null=True, blank=True)
    numTel = models.CharField(max_length=15, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    rol = models.ForeignKey(roles, on_delete=models.CASCADE, null=False, default=1)

    # Configuración para el sistema de autenticación personalizado.
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password', 'rol']

    # Asociación del administrador de usuario personalizado.
    objects = UserManager()

    # Método para representar el usuario como una cadena.
    def __str__(self):
        return self.email

    # Métodos para permisos de usuario (siempre devuelven True en este caso).
    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True

    
#MODELO INFRACCIONES
class infracciones(models.Model):
    num = models.AutoField(primary_key=True)
    fechahora = models.DateTimeField()
    dirCalle = models.CharField(max_length=50)
    dirNumero = models.CharField(max_length=20)
    dirColonia = models.CharField(max_length=50)
    monto = models.DecimalField(max_digits=10, decimal_places=2)
    fechaLimitePago = models.DateField()
    esConductorResponsable = models.BooleanField()
    descripcion = models.TextField()
    vehiculo = models.ForeignKey(vehiculos, on_delete=models.CASCADE, null=False)
    conductor = models.ForeignKey(usuarios, on_delete=models.CASCADE, null=False)

class licencias(models.Model):
    num = models.AutoField(primary_key=True)
    fechaExpedicion = models.DateField()
    fechaVencimiento = models.DateField()
    comentarios = models.TextField()
    conductor = models.ForeignKey(usuarios, on_delete=models.CASCADE, null=False)
    tipo = models.ForeignKey('tipoLicencias', on_delete=models.CASCADE, null=False)

class paradas(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    dirColonia = models.CharField(max_length=50)
    dirCalle = models.CharField(max_length=50)
    dirNumero = models.CharField(max_length=50)

class tipoLicencias(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=10)

class tiposCoberturas(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)

class talleres(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    correo = models.CharField(max_length=100)
    numTel = models.CharField(db_column='numTel', max_length=15)
    dirCalle = models.CharField(db_column='dirCalle', max_length=50)
    dirNumero = models.CharField(db_column='dirNumero', max_length=50)
    dirColonia = models.CharField(db_column='dirColonia', max_length=50)

class polizasSeguros(models.Model):
    numPoliza = models.AutoField(primary_key=True)
    fechaInicio = models.DateField()
    fechaVencimiento = models.DateField()
    montoCobertura = models.DecimalField(max_digits=10, decimal_places=2)
    costo = models.DecimalField(max_digits=10, decimal_places=2)
    frecuenciaPago = models.CharField(max_length=255)
    vehiculo = models.ForeignKey(vehiculos, on_delete=models.CASCADE, null=False)
    aseguradora = models.ForeignKey(aseguradoras, on_delete=models.CASCADE, null=False)
    cobertura = models.ForeignKey(tiposCoberturas, on_delete=models.CASCADE, null=False)

class reclamacionesSeguros(models.Model):
    numFolio = models.AutoField(primary_key=True)
    fecha = models.DateField()
    descripcionIncidente = models.TextField()
    montoReclamo = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    numPoliza = models.ForeignKey(polizasSeguros, on_delete=models.CASCADE, null=False)

class rutas(models.Model):
    num = models.AutoField(primary_key=True)
    fechaHoraInicio = models.DateTimeField()
    fechaHoraFin = models.DateTimeField()
    comentarios = models.TextField()
    finalizada = models.BooleanField(default=False)
    vehiculo = models.ForeignKey(vehiculos, on_delete=models.CASCADE, null=False)

class sesionesMantenimiento(models.Model):
    num = models.AutoField(primary_key=True)
    fecha = models.DateField()
    kilometraje = models.IntegerField()
    costoTotal = models.DecimalField(max_digits=10, decimal_places=2)
    descripcion = models.TextField()
    vehiculo = models.ForeignKey(vehiculos, on_delete=models.CASCADE, null=False)
    taller = models.ForeignKey(talleres, on_delete=models.CASCADE, null=False)

class InfraccionEstadoinfraccion(models.Model):
    infraccion = models.OneToOneField(infracciones, on_delete=models.CASCADE, primary_key=True)
    estado = models.ForeignKey(estadoInfraccion, on_delete=models.CASCADE)
    fechacambio = models.DateTimeField()

class LicenciasEstadoslic(models.Model):
    licencia = models.OneToOneField(licencias, on_delete=models.CASCADE, primary_key=True)
    estado = models.ForeignKey(estadosLicencias, on_delete=models.CASCADE)
    fechacambio = models.DateTimeField()

class ParadasEstadosparadas(models.Model):
    parada = models.OneToOneField(paradas, on_delete=models.CASCADE, primary_key=True)
    estado = models.IntegerField()
    fechacambio = models.DateTimeField()

class ReclamacionesEstadosrec(models.Model):
    reclamacion = models.OneToOneField(reclamacionesSeguros, on_delete=models.CASCADE, primary_key=True)  
    estado = models.ForeignKey(estadosReclamaciones, on_delete=models.CASCADE)
    fechacambio = models.DateTimeField()

class RutasEstadosRutas(models.Model):
    ruta = models.OneToOneField(rutas, on_delete=models.CASCADE, primary_key=True)  
    estado = models.ForeignKey(estadosRutas, on_delete=models.CASCADE)
    fechacambio = models.DateTimeField()

class RutasParadas(models.Model):
    ruta = models.OneToOneField(rutas,on_delete=models.CASCADE, primary_key=True)  
    parada = models.ForeignKey(paradas, on_delete=models.CASCADE)
    numparada = models.IntegerField()
    finalizada = models.IntegerField()

class VehiculosUsuarios(models.Model):
    vehiculo = models.OneToOneField(vehiculos, on_delete=models.CASCADE, primary_key=True)  
    usuario = models.ForeignKey(usuarios, on_delete=models.CASCADE)
