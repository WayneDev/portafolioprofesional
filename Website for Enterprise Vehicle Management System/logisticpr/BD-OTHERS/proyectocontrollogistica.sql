-- Active: 1700726387686@@127.0.0.1@3307@proyectocontrollogistica
-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-11-2023 a las 19:34:14
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectocontrollogistica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authtoken_token`
--

CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add content type', 4, 'add_contenttype'),
(14, 'Can change content type', 4, 'change_contenttype'),
(15, 'Can delete content type', 4, 'delete_contenttype'),
(16, 'Can view content type', 4, 'view_contenttype'),
(17, 'Can add session', 5, 'add_session'),
(18, 'Can change session', 5, 'change_session'),
(19, 'Can delete session', 5, 'delete_session'),
(20, 'Can view session', 5, 'view_session'),
(21, 'Can add Token', 6, 'add_token'),
(22, 'Can change Token', 6, 'change_token'),
(23, 'Can delete Token', 6, 'delete_token'),
(24, 'Can view Token', 6, 'view_token'),
(25, 'Can add token', 7, 'add_tokenproxy'),
(26, 'Can change token', 7, 'change_tokenproxy'),
(27, 'Can delete token', 7, 'delete_tokenproxy'),
(28, 'Can view token', 7, 'view_tokenproxy'),
(29, 'Can add auth token', 8, 'add_authtoken'),
(30, 'Can change auth token', 8, 'change_authtoken'),
(31, 'Can delete auth token', 8, 'delete_authtoken'),
(32, 'Can view auth token', 8, 'view_authtoken'),
(33, 'Can add usuarios', 9, 'add_usuarios'),
(34, 'Can change usuarios', 9, 'change_usuarios'),
(35, 'Can delete usuarios', 9, 'delete_usuarios'),
(36, 'Can view usuarios', 9, 'view_usuarios'),
(37, 'Can add aseguradoras', 10, 'add_aseguradoras'),
(38, 'Can change aseguradoras', 10, 'change_aseguradoras'),
(39, 'Can delete aseguradoras', 10, 'delete_aseguradoras'),
(40, 'Can view aseguradoras', 10, 'view_aseguradoras'),
(41, 'Can add estado infraccion', 11, 'add_estadoinfraccion'),
(42, 'Can change estado infraccion', 11, 'change_estadoinfraccion'),
(43, 'Can delete estado infraccion', 11, 'delete_estadoinfraccion'),
(44, 'Can view estado infraccion', 11, 'view_estadoinfraccion'),
(45, 'Can add estados licencias', 12, 'add_estadoslicencias'),
(46, 'Can change estados licencias', 12, 'change_estadoslicencias'),
(47, 'Can delete estados licencias', 12, 'delete_estadoslicencias'),
(48, 'Can view estados licencias', 12, 'view_estadoslicencias'),
(49, 'Can add estados paradas', 13, 'add_estadosparadas'),
(50, 'Can change estados paradas', 13, 'change_estadosparadas'),
(51, 'Can delete estados paradas', 13, 'delete_estadosparadas'),
(52, 'Can view estados paradas', 13, 'view_estadosparadas'),
(53, 'Can add estados reclamaciones', 14, 'add_estadosreclamaciones'),
(54, 'Can change estados reclamaciones', 14, 'change_estadosreclamaciones'),
(55, 'Can delete estados reclamaciones', 14, 'delete_estadosreclamaciones'),
(56, 'Can view estados reclamaciones', 14, 'view_estadosreclamaciones'),
(57, 'Can add estados rutas', 15, 'add_estadosrutas'),
(58, 'Can change estados rutas', 15, 'change_estadosrutas'),
(59, 'Can delete estados rutas', 15, 'delete_estadosrutas'),
(60, 'Can view estados rutas', 15, 'view_estadosrutas'),
(61, 'Can add marcas vehiculos', 16, 'add_marcasvehiculos'),
(62, 'Can change marcas vehiculos', 16, 'change_marcasvehiculos'),
(63, 'Can delete marcas vehiculos', 16, 'delete_marcasvehiculos'),
(64, 'Can view marcas vehiculos', 16, 'view_marcasvehiculos'),
(65, 'Can add modelos vehiculos', 17, 'add_modelosvehiculos'),
(66, 'Can change modelos vehiculos', 17, 'change_modelosvehiculos'),
(67, 'Can delete modelos vehiculos', 17, 'delete_modelosvehiculos'),
(68, 'Can view modelos vehiculos', 17, 'view_modelosvehiculos'),
(69, 'Can add paradas', 18, 'add_paradas'),
(70, 'Can change paradas', 18, 'change_paradas'),
(71, 'Can delete paradas', 18, 'delete_paradas'),
(72, 'Can view paradas', 18, 'view_paradas'),
(73, 'Can add polizas seguros', 19, 'add_polizasseguros'),
(74, 'Can change polizas seguros', 19, 'change_polizasseguros'),
(75, 'Can delete polizas seguros', 19, 'delete_polizasseguros'),
(76, 'Can view polizas seguros', 19, 'view_polizasseguros'),
(77, 'Can add reclamaciones seguros', 20, 'add_reclamacionesseguros'),
(78, 'Can change reclamaciones seguros', 20, 'change_reclamacionesseguros'),
(79, 'Can delete reclamaciones seguros', 20, 'delete_reclamacionesseguros'),
(80, 'Can view reclamaciones seguros', 20, 'view_reclamacionesseguros'),
(81, 'Can add roles', 21, 'add_roles'),
(82, 'Can change roles', 21, 'change_roles'),
(83, 'Can delete roles', 21, 'delete_roles'),
(84, 'Can view roles', 21, 'view_roles'),
(85, 'Can add talleres', 22, 'add_talleres'),
(86, 'Can change talleres', 22, 'change_talleres'),
(87, 'Can delete talleres', 22, 'delete_talleres'),
(88, 'Can view talleres', 22, 'view_talleres'),
(89, 'Can add tipo licencias', 23, 'add_tipolicencias'),
(90, 'Can change tipo licencias', 23, 'change_tipolicencias'),
(91, 'Can delete tipo licencias', 23, 'delete_tipolicencias'),
(92, 'Can view tipo licencias', 23, 'view_tipolicencias'),
(93, 'Can add tipos coberturas', 24, 'add_tiposcoberturas'),
(94, 'Can change tipos coberturas', 24, 'change_tiposcoberturas'),
(95, 'Can delete tipos coberturas', 24, 'delete_tiposcoberturas'),
(96, 'Can view tipos coberturas', 24, 'view_tiposcoberturas'),
(97, 'Can add tipos vehiculos', 25, 'add_tiposvehiculos'),
(98, 'Can change tipos vehiculos', 25, 'change_tiposvehiculos'),
(99, 'Can delete tipos vehiculos', 25, 'delete_tiposvehiculos'),
(100, 'Can view tipos vehiculos', 25, 'view_tiposvehiculos'),
(101, 'Can add vehiculos', 26, 'add_vehiculos'),
(102, 'Can change vehiculos', 26, 'change_vehiculos'),
(103, 'Can delete vehiculos', 26, 'delete_vehiculos'),
(104, 'Can view vehiculos', 26, 'view_vehiculos'),
(105, 'Can add paradas estadosparadas', 27, 'add_paradasestadosparadas'),
(106, 'Can change paradas estadosparadas', 27, 'change_paradasestadosparadas'),
(107, 'Can delete paradas estadosparadas', 27, 'delete_paradasestadosparadas'),
(108, 'Can view paradas estadosparadas', 27, 'view_paradasestadosparadas'),
(109, 'Can add sesiones mantenimiento', 28, 'add_sesionesmantenimiento'),
(110, 'Can change sesiones mantenimiento', 28, 'change_sesionesmantenimiento'),
(111, 'Can delete sesiones mantenimiento', 28, 'delete_sesionesmantenimiento'),
(112, 'Can view sesiones mantenimiento', 28, 'view_sesionesmantenimiento'),
(113, 'Can add rutas', 29, 'add_rutas'),
(114, 'Can change rutas', 29, 'change_rutas'),
(115, 'Can delete rutas', 29, 'delete_rutas'),
(116, 'Can view rutas', 29, 'view_rutas'),
(117, 'Can add licencias', 30, 'add_licencias'),
(118, 'Can change licencias', 30, 'change_licencias'),
(119, 'Can delete licencias', 30, 'delete_licencias'),
(120, 'Can view licencias', 30, 'view_licencias'),
(121, 'Can add infracciones', 31, 'add_infracciones'),
(122, 'Can change infracciones', 31, 'change_infracciones'),
(123, 'Can delete infracciones', 31, 'delete_infracciones'),
(124, 'Can view infracciones', 31, 'view_infracciones'),
(125, 'Can add documentos vehiculos', 32, 'add_documentosvehiculos'),
(126, 'Can change documentos vehiculos', 32, 'change_documentosvehiculos'),
(127, 'Can delete documentos vehiculos', 32, 'delete_documentosvehiculos'),
(128, 'Can view documentos vehiculos', 32, 'view_documentosvehiculos'),
(129, 'Can add cargas combustible', 33, 'add_cargascombustible'),
(130, 'Can change cargas combustible', 33, 'change_cargascombustible'),
(131, 'Can delete cargas combustible', 33, 'delete_cargascombustible'),
(132, 'Can view cargas combustible', 33, 'view_cargascombustible'),
(133, 'Can add vehiculos usuarios', 34, 'add_vehiculosusuarios'),
(134, 'Can change vehiculos usuarios', 34, 'change_vehiculosusuarios'),
(135, 'Can delete vehiculos usuarios', 34, 'delete_vehiculosusuarios'),
(136, 'Can view vehiculos usuarios', 34, 'view_vehiculosusuarios'),
(137, 'Can add rutas paradas', 35, 'add_rutasparadas'),
(138, 'Can change rutas paradas', 35, 'change_rutasparadas'),
(139, 'Can delete rutas paradas', 35, 'delete_rutasparadas'),
(140, 'Can view rutas paradas', 35, 'view_rutasparadas'),
(141, 'Can add rutas estados rutas', 36, 'add_rutasestadosrutas'),
(142, 'Can change rutas estados rutas', 36, 'change_rutasestadosrutas'),
(143, 'Can delete rutas estados rutas', 36, 'delete_rutasestadosrutas'),
(144, 'Can view rutas estados rutas', 36, 'view_rutasestadosrutas'),
(145, 'Can add reclamaciones estadosrec', 37, 'add_reclamacionesestadosrec'),
(146, 'Can change reclamaciones estadosrec', 37, 'change_reclamacionesestadosrec'),
(147, 'Can delete reclamaciones estadosrec', 37, 'delete_reclamacionesestadosrec'),
(148, 'Can view reclamaciones estadosrec', 37, 'view_reclamacionesestadosrec'),
(149, 'Can add licencias estadoslic', 38, 'add_licenciasestadoslic'),
(150, 'Can change licencias estadoslic', 38, 'change_licenciasestadoslic'),
(151, 'Can delete licencias estadoslic', 38, 'delete_licenciasestadoslic'),
(152, 'Can view licencias estadoslic', 38, 'view_licenciasestadoslic'),
(153, 'Can add infraccion estadoinfraccion', 39, 'add_infraccionestadoinfraccion'),
(154, 'Can change infraccion estadoinfraccion', 39, 'change_infraccionestadoinfraccion'),
(155, 'Can delete infraccion estadoinfraccion', 39, 'delete_infraccionestadoinfraccion'),
(156, 'Can view infraccion estadoinfraccion', 39, 'view_infraccionestadoinfraccion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2023-11-28 04:46:18.944341', '13', 'aaaa.asda@gmail.com', 3, '', 9, 1),
(2, '2023-11-28 04:46:18.947766', '12', 'asdsa.asda@gmail.com', 3, '', 9, 1),
(3, '2023-11-28 04:46:18.950422', '11', 'joseph.asda@gmail.com', 3, '', 9, 1),
(4, '2023-11-28 04:46:18.954017', '9', 'user@example.com', 3, '', 9, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(6, 'authtoken', 'token'),
(7, 'authtoken', 'tokenproxy'),
(4, 'contenttypes', 'contenttype'),
(10, 'home', 'aseguradoras'),
(33, 'home', 'cargascombustible'),
(32, 'home', 'documentosvehiculos'),
(11, 'home', 'estadoinfraccion'),
(12, 'home', 'estadoslicencias'),
(13, 'home', 'estadosparadas'),
(14, 'home', 'estadosreclamaciones'),
(15, 'home', 'estadosrutas'),
(31, 'home', 'infracciones'),
(39, 'home', 'infraccionestadoinfraccion'),
(30, 'home', 'licencias'),
(38, 'home', 'licenciasestadoslic'),
(16, 'home', 'marcasvehiculos'),
(17, 'home', 'modelosvehiculos'),
(18, 'home', 'paradas'),
(27, 'home', 'paradasestadosparadas'),
(19, 'home', 'polizasseguros'),
(37, 'home', 'reclamacionesestadosrec'),
(20, 'home', 'reclamacionesseguros'),
(21, 'home', 'roles'),
(29, 'home', 'rutas'),
(36, 'home', 'rutasestadosrutas'),
(35, 'home', 'rutasparadas'),
(28, 'home', 'sesionesmantenimiento'),
(22, 'home', 'talleres'),
(23, 'home', 'tipolicencias'),
(24, 'home', 'tiposcoberturas'),
(25, 'home', 'tiposvehiculos'),
(9, 'home', 'usuarios'),
(26, 'home', 'vehiculos'),
(34, 'home', 'vehiculosusuarios'),
(8, 'knox', 'authtoken'),
(5, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'home', '0001_initial', '2023-11-28 00:37:29.360909'),
(2, 'contenttypes', '0001_initial', '2023-11-28 00:37:29.389185'),
(3, 'admin', '0001_initial', '2023-11-28 00:37:29.484212'),
(4, 'admin', '0002_logentry_remove_auto_add', '2023-11-28 00:37:29.521223'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2023-11-28 00:37:29.530366'),
(6, 'contenttypes', '0002_remove_content_type_name', '2023-11-28 00:37:29.582483'),
(7, 'auth', '0001_initial', '2023-11-28 00:37:29.843012'),
(8, 'auth', '0002_alter_permission_name_max_length', '2023-11-28 00:37:29.898919'),
(9, 'auth', '0003_alter_user_email_max_length', '2023-11-28 00:37:29.908584'),
(10, 'auth', '0004_alter_user_username_opts', '2023-11-28 00:37:29.916231'),
(11, 'auth', '0005_alter_user_last_login_null', '2023-11-28 00:37:29.925168'),
(12, 'auth', '0006_require_contenttypes_0002', '2023-11-28 00:37:29.929283'),
(13, 'auth', '0007_alter_validators_add_error_messages', '2023-11-28 00:37:29.937887'),
(14, 'auth', '0008_alter_user_username_max_length', '2023-11-28 00:37:29.946614'),
(15, 'auth', '0009_alter_user_last_name_max_length', '2023-11-28 00:37:29.954892'),
(16, 'auth', '0010_alter_group_name_max_length', '2023-11-28 00:37:30.008374'),
(17, 'auth', '0011_update_proxy_permissions', '2023-11-28 00:37:30.033463'),
(18, 'auth', '0012_alter_user_first_name_max_length', '2023-11-28 00:37:30.043671'),
(19, 'authtoken', '0001_initial', '2023-11-28 00:37:30.109546'),
(20, 'authtoken', '0002_auto_20160226_1747', '2023-11-28 00:37:30.150664'),
(21, 'authtoken', '0003_tokenproxy', '2023-11-28 00:37:30.155762'),
(22, 'knox', '0001_initial', '2023-11-28 00:37:30.223572'),
(23, 'knox', '0002_auto_20150916_1425', '2023-11-28 00:37:30.308816'),
(24, 'knox', '0003_auto_20150916_1526', '2023-11-28 00:37:30.359633'),
(25, 'knox', '0004_authtoken_expires', '2023-11-28 00:37:30.375212'),
(26, 'knox', '0005_authtoken_token_key', '2023-11-28 00:37:30.401571'),
(27, 'knox', '0006_auto_20160818_0932', '2023-11-28 00:37:30.472026'),
(28, 'knox', '0007_auto_20190111_0542', '2023-11-28 00:37:30.488064'),
(29, 'knox', '0008_remove_authtoken_salt', '2023-11-28 00:37:30.507948'),
(30, 'sessions', '0001_initial', '2023-11-28 00:37:30.534989'),
(31, 'home', '0002_rename_correo_usuarios_email_and_more', '2023-11-28 00:42:44.216040'),
(32, 'home', '0003_alter_usuarios_is_active', '2023-11-28 03:35:21.413376'),
(33, 'home', '0004_alter_usuarios_rol', '2023-11-28 04:14:29.299738');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('05gjq94ghnx3modaice44gcfbpee6vl9', '.eJxVjEEOwiAQRe_C2pChMBRduu8ZCAOMVA0kpV0Z765NutDtf-_9l_BhW4vfel78nMRFDOL0u1GIj1x3kO6h3pqMra7LTHJX5EG7nFrKz-vh_h2U0Mu3ZsVKA-cUHKIiZ8kQR0VhyBpsNgCgkS0iAZPh84hOszKoNTgcnRLvD-s5NzU:1r7qDS:4AUe7dRDVxIohvplAKqTzTV028bH-SuEjj1MNzEpEdk', '2023-12-12 05:00:58.494133'),
('as35i7xk875h9kjid5ktkd7sa9fgqokf', 'e30:1r7qA2:r5_haWer4BKxguv3cVj8A0RIf-yiKqdtLB4VAc-BPB0', '2023-12-12 04:57:26.030767'),
('fifjcw2u0l7gdsvix50vlje3cukfyq4d', '.eJxVjDsOwjAQBe_iGlm28S-U9DmDtetd4wBypDipEHeHSCmgfTPzXiLBtta0dV7SROIitDj9bgj5wW0HdId2m2We27pMKHdFHrTLcSZ-Xg_376BCr986enSlnLVxlpGQydAQlAkqUnYmRwA0EXCwpnhgrZ0KtiiOnqnowEW8P_b3OJI:1r7pq2:nMoJJok3Qs0XeFLhzAb-wYyWv5wrHQzal14NWjWAiqA', '2023-12-12 04:36:46.935108'),
('z55spgten1404eu6yoy2n1gz0t08pkep', 'e30:1r7oX0:IhJY9Ti1L9o-RmH2j_28F7ssxcq_2IEyLfb3x2-_WYM', '2023-12-12 03:13:02.279687');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_aseguradoras`
--

CREATE TABLE `home_aseguradoras` (
  `num` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `dirColonia` varchar(50) NOT NULL,
  `dirCalle` varchar(50) NOT NULL,
  `dirNumero` varchar(50) NOT NULL,
  `numTel` varchar(15) NOT NULL,
  `correo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_aseguradoras`
--

INSERT INTO `home_aseguradoras` (`num`, `nombre`, `dirColonia`, `dirCalle`, `dirNumero`, `numTel`, `correo`) VALUES
(1, 'Qualitas', 'El Refugio', 'Lienxo charro', 'L-11', '664 208 3939', 'maclovio@qualitas.com.mx'),
(2, 'OTAY BORDER INSURANCE', 'Nueva Tijuana', 'Blvd. de las Bellas Artes', '19535-B', '664 979 0301', 'seguros@otayborder.com'),
(3, 'Seguros AXA', '20 de Noviembre', 'Blvd. de Las Americas', '3565', '664 381 7352', 'seguros@axa.com'),
(4, 'Safer Insurance Agency', 'Garita de Otay', 'Blvd. de las Bellas Artes', '1226-5', '664 647 5678', 'safer@insurance.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_cargascombustible`
--

CREATE TABLE `home_cargascombustible` (
  `num` int(11) NOT NULL,
  `numFolio` int(11) NOT NULL,
  `litrosCargados` decimal(10,2) NOT NULL,
  `fecha` date NOT NULL,
  `kilometraje` int(11) NOT NULL,
  `comentarios` longtext NOT NULL,
  `costoTotal` decimal(10,2) NOT NULL,
  `vehiculo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_cargascombustible`
--

INSERT INTO `home_cargascombustible` (`num`, `numFolio`, `litrosCargados`, `fecha`, `kilometraje`, `comentarios`, `costoTotal`, `vehiculo_id`) VALUES
(1, 17548, 10.00, '2023-03-03', 25256, '', 222.80, 1),
(2, 17698, 20.00, '2023-06-20', 34895, '', 445.60, 2),
(3, 17789, 10.00, '2023-06-15', 42800, '', 222.80, 2),
(4, 17796, 15.00, '2023-06-30', 37789, '', 334.20, 2),
(5, 17849, 80.00, '2023-07-30', 44689, '', 1931.04, 4),
(6, 17878, 20.00, '2023-11-15', 55250, '', 445.60, 7),
(7, 17987, 80.00, '2023-11-20', 43200, '', 1931.04, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_documentosvehiculos`
--

CREATE TABLE `home_documentosvehiculos` (
  `num` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `fechaEmision` date NOT NULL,
  `fechaVencimiento` date NOT NULL,
  `comentarios` longtext NOT NULL,
  `vehiculo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_documentosvehiculos`
--

INSERT INTO `home_documentosvehiculos` (`num`, `tipo`, `fechaEmision`, `fechaVencimiento`, `comentarios`, `vehiculo_id`) VALUES
(1, 'Tarjeta de Circulacion', '2023-01-16', '2024-01-16', '', 1),
(2, 'Certificado de Registro', '2023-01-16', '2024-01-16', '', 1),
(3, 'Tarjeta de Circulacion', '2023-04-04', '2024-04-04', '', 4),
(4, 'Certificado de Registro', '2023-04-04', '2024-04-04', '', 4),
(5, 'Tarjeta de Circulacion', '2023-11-03', '2024-11-03', '', 11),
(6, 'Certificado de Registro', '2023-11-03', '2024-11-03', '', 11),
(7, 'Tarjeta de Circulacion', '2023-06-21', '2024-06-21', '', 18),
(8, 'Certificado de Registro', '2023-06-21', '2024-06-21', '', 18),
(9, 'Tarjeta de Circulacion', '2023-02-28', '2024-02-29', '', 2),
(10, 'Certificado de Registro', '2023-02-28', '2024-02-29', '', 2),
(11, 'Tarjeta de Circulacion', '2023-05-23', '2024-05-23', '', 5),
(12, 'Certificado de Registro', '2023-05-23', '2024-05-23', '', 5),
(13, 'Tarjeta de Circulacion', '2023-11-12', '2024-11-12', '', 12),
(14, 'Certificado de Registro', '2023-11-12', '2024-11-12', '', 12),
(15, 'Tarjeta de Circulacion', '2023-07-04', '2024-07-04', '', 19),
(16, 'Certificado de Registro', '2023-07-04', '2024-07-04', '', 19),
(17, 'Tarjeta de Circulacion', '2023-07-18', '2024-07-18', '', 7),
(18, 'Certificado de Registro', '2023-07-18', '2024-07-18', '', 7),
(19, 'Tarjeta de Circulacion', '2023-02-11', '2024-02-11', '', 14),
(20, 'Certificado de Registro', '2023-02-11', '2024-02-11', '', 14),
(21, 'Certificado Vehicular', '2023-11-02', '2024-11-02', '', 24),
(22, 'Tarjeta de circulacion', '2023-11-02', '2024-11-02', '', 24),
(23, 'Documentos de Importacion', '2023-11-15', '2024-11-15', '', 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_estadoinfraccion`
--

CREATE TABLE `home_estadoinfraccion` (
  `num` int(11) NOT NULL,
  `descripcion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_estadoinfraccion`
--

INSERT INTO `home_estadoinfraccion` (`num`, `descripcion`) VALUES
(1, 'Terminado'),
(2, 'En proceso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_estadoslicencias`
--

CREATE TABLE `home_estadoslicencias` (
  `num` int(11) NOT NULL,
  `descripcion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_estadoslicencias`
--

INSERT INTO `home_estadoslicencias` (`num`, `descripcion`) VALUES
(1, 'Vigente'),
(2, 'Vencida');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_estadosparadas`
--

CREATE TABLE `home_estadosparadas` (
  `num` int(11) NOT NULL,
  `descripcion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_estadosparadas`
--

INSERT INTO `home_estadosparadas` (`num`, `descripcion`) VALUES
(1, 'Terminado'),
(2, 'En preceso'),
(3, 'Pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_estadosreclamaciones`
--

CREATE TABLE `home_estadosreclamaciones` (
  `num` int(11) NOT NULL,
  `descripcion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_estadosreclamaciones`
--

INSERT INTO `home_estadosreclamaciones` (`num`, `descripcion`) VALUES
(1, 'Terminado'),
(2, 'En preceso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_estadosrutas`
--

CREATE TABLE `home_estadosrutas` (
  `num` int(11) NOT NULL,
  `descripcion` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_estadosrutas`
--

INSERT INTO `home_estadosrutas` (`num`, `descripcion`) VALUES
(1, 'Terminado'),
(2, 'En proceso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_infracciones`
--

CREATE TABLE `home_infracciones` (
  `num` int(11) NOT NULL,
  `fechahora` datetime(6) NOT NULL,
  `dirCalle` varchar(50) NOT NULL,
  `dirNumero` varchar(20) NOT NULL,
  `dirColonia` varchar(50) NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `fechaLimitePago` date NOT NULL,
  `esConductorResponsable` tinyint(1) NOT NULL,
  `descripcion` longtext NOT NULL,
  `conductor_id` int(11) NOT NULL,
  `vehiculo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_infracciones`
--

INSERT INTO `home_infracciones` (`num`, `fechahora`, `dirCalle`, `dirNumero`, `dirColonia`, `monto`, `fechaLimitePago`, `esConductorResponsable`, `descripcion`, `conductor_id`, `vehiculo_id`) VALUES
(1, '2023-11-22 08:00:00.000000', 'Blvd. Casa Blanca', '19901', 'Buenos Aires Sur', 10000.00, '2023-12-02', 1, 'Obstruccion de una avenida, al provocar choque', 3, 2),
(2, '2023-11-25 10:25:00.000000', 'Parque Baja California', '751', 'Costa Hermosa', 15000.00, '2023-12-05', 1, 'Pasarse un semaforo en rojo', 3, 2),
(3, '2023-11-20 15:00:00.000000', 'De las Ferias', '10551', 'Herradura', 5000.00, '2023-11-30', 1, 'No realizar alto en una señalacion', 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_infraccionestadoinfraccion`
--

CREATE TABLE `home_infraccionestadoinfraccion` (
  `infraccion_id` int(11) NOT NULL,
  `fechacambio` datetime(6) NOT NULL,
  `estado_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_infraccionestadoinfraccion`
--

INSERT INTO `home_infraccionestadoinfraccion` (`infraccion_id`, `fechacambio`, `estado_id`) VALUES
(1, '2023-11-25 00:00:00.000000', 2),
(2, '2023-11-30 00:00:00.000000', 2),
(3, '2023-11-30 00:00:00.000000', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_licencias`
--

CREATE TABLE `home_licencias` (
  `num` int(11) NOT NULL,
  `fechaExpedicion` date NOT NULL,
  `fechaVencimiento` date NOT NULL,
  `comentarios` longtext NOT NULL,
  `conductor_id` int(11) NOT NULL,
  `tipo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_licencias`
--

INSERT INTO `home_licencias` (`num`, `fechaExpedicion`, `fechaVencimiento`, `comentarios`, `conductor_id`, `tipo_id`) VALUES
(1, '2020-01-15', '2025-01-15', '', 3, 1),
(2, '2020-10-30', '2025-10-30', '', 4, 2),
(3, '2022-04-16', '2027-04-16', '', 6, 3),
(4, '2019-05-19', '2024-05-19', '', 7, 1),
(5, '2023-08-24', '2028-08-24', '', 9, 3),
(6, '2019-08-29', '2024-08-29', '', 10, 2),
(7, '2021-07-18', '2026-07-18', '', 11, 1),
(8, '2021-11-15', '2026-11-15', '', 14, 3),
(9, '2025-01-16', '2026-01-16', 'Sin comentarios', 3, 1),
(10, '2026-01-17', '2027-01-16', 'Sin comentarios', 3, 1);

--
-- Disparadores `home_licencias`
--
DELIMITER $$
CREATE TRIGGER `verificarFechaLicencia` BEFORE INSERT ON `home_licencias` FOR EACH ROW BEGIN
        DECLARE fechalicenciaVencio DATE;

        set fechalicenciaVencio = ( SELECT MAX(fechaVencimiento)
                                    FROM home_licencias
                                    WHERE conductor_id = new.conductor_id);

        IF(new.fechaExpedicion < fechalicenciaVencio)
        THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT= "Licencia vigente, no se puede renovar";
        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_licenciasestadoslic`
--

CREATE TABLE `home_licenciasestadoslic` (
  `licencia_id` int(11) NOT NULL,
  `fechacambio` datetime(6) NOT NULL,
  `estado_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_licenciasestadoslic`
--

INSERT INTO `home_licenciasestadoslic` (`licencia_id`, `fechacambio`, `estado_id`) VALUES
(1, '0000-00-00 00:00:00.000000', 1),
(2, '0000-00-00 00:00:00.000000', 1),
(3, '0000-00-00 00:00:00.000000', 1),
(4, '0000-00-00 00:00:00.000000', 1),
(5, '0000-00-00 00:00:00.000000', 1),
(6, '0000-00-00 00:00:00.000000', 1),
(7, '0000-00-00 00:00:00.000000', 1),
(8, '0000-00-00 00:00:00.000000', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_marcasvehiculos`
--

CREATE TABLE `home_marcasvehiculos` (
  `num` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_marcasvehiculos`
--

INSERT INTO `home_marcasvehiculos` (`num`, `nombre`) VALUES
(1, 'Honda'),
(2, 'Nissan'),
(3, 'Chevrolet'),
(4, 'Toyota'),
(5, 'Volvo'),
(6, 'Kenworth');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_modelosvehiculos`
--

CREATE TABLE `home_modelosvehiculos` (
  `num` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `consumoConbustible` int(11) NOT NULL,
  `cantCarga` int(11) NOT NULL,
  `capPasajero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_modelosvehiculos`
--

INSERT INTO `home_modelosvehiculos` (`num`, `nombre`, `consumoConbustible`, `cantCarga`, `capPasajero`) VALUES
(1, 'CB50X', 20, 200, 2),
(2, 'March', 15, 400, 4),
(3, 'Mariz', 18, 400, 4),
(4, 'Tacoma', 8, 600, 5),
(5, 'VNL', 4, 20000, 3),
(6, 'W900', 4, 20000, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_paradas`
--

CREATE TABLE `home_paradas` (
  `num` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `dirColonia` varchar(50) NOT NULL,
  `dirCalle` varchar(50) NOT NULL,
  `dirNumero` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_paradas`
--

INSERT INTO `home_paradas` (`num`, `nombre`, `dirColonia`, `dirCalle`, `dirNumero`) VALUES
(1, 'El Florido 1ra Seccion', 'El Florido 1ra y 2da Secc', 'Blvd. Los Olivos', '11110'),
(2, 'Aduana', 'Cuauhtemoc', 'Ramp,Ramp Xicotencatl', '12'),
(3, 'San Ysidro', 'San Ysidro', 'Camino De La Plaza', '4330'),
(4, 'El florido 2da Seccion', 'El Florido 1ra y 2da Secc', 'Blvd. Los Olivos', '11111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_paradasestadosparadas`
--

CREATE TABLE `home_paradasestadosparadas` (
  `parada_id` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fechacambio` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_paradasestadosparadas`
--

INSERT INTO `home_paradasestadosparadas` (`parada_id`, `estado`, `fechacambio`) VALUES
(1, 1, '2023-06-15 12:00:00.000000'),
(2, 1, '2023-07-01 15:00:00.000000'),
(3, 1, '2023-08-06 10:00:00.000000'),
(4, 2, '2023-11-16 11:00:00.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_polizasseguros`
--

CREATE TABLE `home_polizasseguros` (
  `numPoliza` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaVencimiento` date NOT NULL,
  `montoCobertura` decimal(10,2) NOT NULL,
  `costo` decimal(10,2) NOT NULL,
  `frecuenciaPago` varchar(255) NOT NULL,
  `aseguradora_id` int(11) NOT NULL,
  `cobertura_id` int(11) NOT NULL,
  `vehiculo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_polizasseguros`
--

INSERT INTO `home_polizasseguros` (`numPoliza`, `fechaInicio`, `fechaVencimiento`, `montoCobertura`, `costo`, `frecuenciaPago`, `aseguradora_id`, `cobertura_id`, `vehiculo_id`) VALUES
(1, '2023-01-16', '2024-01-16', 200000.00, 600.00, 'Mensual', 4, 4, 1),
(2, '2023-04-04', '2024-04-04', 20000.00, 500.00, 'Mensual', 3, 2, 4),
(3, '2023-11-03', '2024-11-03', 20000.00, 500.00, 'Mensual', 3, 2, 11),
(4, '2023-06-21', '2024-06-21', 500000.00, 800.00, 'Mensual', 3, 1, 18),
(5, '2023-02-28', '2024-02-29', 200000.00, 600.00, 'Mensual', 2, 4, 2),
(6, '2023-05-23', '2024-05-23', 20000.00, 500.00, 'Mensual', 2, 2, 5),
(7, '2023-11-12', '2024-11-12', 10000.00, 300.00, 'Mensual', 1, 3, 12),
(8, '2023-11-02', '2024-11-02', 500000.00, 800.00, 'Mensual', 2, 1, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_reclamacionesestadosrec`
--

CREATE TABLE `home_reclamacionesestadosrec` (
  `reclamacion_id` int(11) NOT NULL,
  `fechacambio` datetime(6) NOT NULL,
  `estado_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_reclamacionesestadosrec`
--

INSERT INTO `home_reclamacionesestadosrec` (`reclamacion_id`, `fechacambio`, `estado_id`) VALUES
(1, '2023-05-12 00:00:00.000000', 1),
(2, '2023-11-20 00:00:00.000000', 1),
(3, '2023-07-30 00:00:00.000000', 1),
(4, '2023-11-15 00:00:00.000000', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_reclamacionesseguros`
--

CREATE TABLE `home_reclamacionesseguros` (
  `numFolio` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `descripcionIncidente` longtext NOT NULL,
  `montoReclamo` decimal(10,2) DEFAULT NULL,
  `numPoliza_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_reclamacionesseguros`
--

INSERT INTO `home_reclamacionesseguros` (`numFolio`, `fecha`, `descripcionIncidente`, `montoReclamo`, `numPoliza_id`) VALUES
(1, '2023-05-12', 'Lesion por choque, auto con exceso de velocidad golpeo al vehiculo empresarial', 10000.00, 1),
(2, '2023-06-12', 'El conducto golpeo un poste de luz ocasionandose una contusion', 10000.00, 1),
(3, '2020-06-12', 'Lesion por choque, auto con exceso de velocidad golpeo al vehiculo empresarial', 10000.00, 5),
(4, '2023-11-20', 'Choque automovilistico ocasionado por el usuario', 15000.00, 1),
(5, '2023-07-30', 'Lesion por choque, auto con exceso de velocidad golpeo al vehiculo empresarial', 8000.00, 5),
(6, '2023-11-15', 'Robo de las llantas del automovil empresarial', 8000.00, 1);

--
-- Disparadores `home_reclamacionesseguros`
--
DELIMITER $$
CREATE TRIGGER `verificaFechaRecSeguro` BEFORE INSERT ON `home_reclamacionesseguros` FOR EACH ROW BEGIN
        DECLARE fechaInicioPoliza DATE;
        DECLARE fechaFinalPoliza DATE;

        set fechaInicioPoliza = (   SELECT fechaInicio
                                    FROM home_polizasseguros
                                    WHERE numPoliza = new.numPoliza_id);
        set fechaFinalPoliza = (SELECT fechaVencimiento
                                FROM home_polizasseguros
                                WHERE numPoliza = new.numPoliza_id);

        IF(new.fecha <= fechaInicioPoliza)
        THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT= "Fecha incorrecta";
        ELSE
            IF (new.fecha > fechaFinalPoliza)
            THEN                
                SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT= "Poliza Vencida";
            END IF;
        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_roles`
--

CREATE TABLE `home_roles` (
  `num` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_roles`
--

INSERT INTO `home_roles` (`num`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Conductor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_rutas`
--

CREATE TABLE `home_rutas` (
  `num` int(11) NOT NULL,
  `fechaHoraInicio` datetime(6) NOT NULL,
  `fechaHoraFin` datetime(6) NOT NULL,
  `comentarios` longtext NOT NULL,
  `finalizada` tinyint(1) NOT NULL,
  `vehiculo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_rutas`
--

INSERT INTO `home_rutas` (`num`, `fechaHoraInicio`, `fechaHoraFin`, `comentarios`, `finalizada`, `vehiculo_id`) VALUES
(1, '2023-06-15 10:00:00.000000', '2023-06-15 12:00:00.000000', 'Completada', 1, 2),
(2, '2023-06-15 12:00:00.000000', '2023-06-15 01:00:00.000000', 'Completada', 1, 2),
(3, '2023-06-15 13:00:00.000000', '2023-06-15 15:00:00.000000', 'Completada', 1, 2),
(4, '2023-06-15 15:00:00.000000', '2023-06-15 20:00:00.000000', 'Completada', 1, 2),
(5, '2023-11-20 09:00:00.000000', '2023-11-30 09:00:00.000000', '', 0, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_rutasestadosrutas`
--

CREATE TABLE `home_rutasestadosrutas` (
  `ruta_id` int(11) NOT NULL,
  `fechacambio` datetime(6) NOT NULL,
  `estado_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_rutasestadosrutas`
--

INSERT INTO `home_rutasestadosrutas` (`ruta_id`, `fechacambio`, `estado_id`) VALUES
(1, '2023-06-15 12:00:00.000000', 1),
(2, '2023-07-01 15:00:00.000000', 1),
(3, '2023-08-06 10:00:00.000000', 1),
(4, '2023-11-16 11:00:00.000000', 1),
(5, '0000-00-00 00:00:00.000000', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_rutasparadas`
--

CREATE TABLE `home_rutasparadas` (
  `ruta_id` int(11) NOT NULL,
  `numparada` int(11) NOT NULL,
  `finalizada` int(11) NOT NULL,
  `parada_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_rutasparadas`
--

INSERT INTO `home_rutasparadas` (`ruta_id`, `numparada`, `finalizada`, `parada_id`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 4),
(3, 3, 1, 2),
(4, 4, 0, 3),
(5, 2, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_sesionesmantenimiento`
--

CREATE TABLE `home_sesionesmantenimiento` (
  `num` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `kilometraje` int(11) NOT NULL,
  `costoTotal` decimal(10,2) NOT NULL,
  `descripcion` longtext NOT NULL,
  `taller_id` int(11) NOT NULL,
  `vehiculo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_sesionesmantenimiento`
--

INSERT INTO `home_sesionesmantenimiento` (`num`, `fecha`, `kilometraje`, `costoTotal`, `descripcion`, `taller_id`, `vehiculo_id`) VALUES
(1, '2023-05-30', 42800, 5000.00, 'Cambio de frenos delanteros como los traseros ya que tenian desgaste de las pastillas y discos', 1, 2),
(2, '2023-11-20', 28700, 10000.00, 'Reemplazo de los amortiguadores ya que se encontró una fugas de líquido y tiene poca capacidad para absorber impactos.', 3, 2),
(3, '2023-05-15', 33750, 8000.00, 'Se ajusto y se realizo pruebas de frenado para garantizar un rendimiento óptimo.', 2, 14),
(4, '2023-07-26', 14325, 1000.00, 'Inspección para verificar que la presión, la alineación y el desgaste se mantengan en niveles adecuados. ', 4, 2),
(5, '2023-11-17', 50999, 2000.00, 'Se realizo un análisis exhaustivo de la presión de aire, la banda de rodadura y la alineación.', 2, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_talleres`
--

CREATE TABLE `home_talleres` (
  `num` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `numTel` varchar(15) NOT NULL,
  `dirCalle` varchar(50) NOT NULL,
  `dirNumero` varchar(50) NOT NULL,
  `dirColonia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_talleres`
--

INSERT INTO `home_talleres` (`num`, `nombre`, `correo`, `numTel`, `dirCalle`, `dirNumero`, `dirColonia`) VALUES
(1, 'OTAY', 'tallermecanico@tijuana.com', '664 319 6049', 'Av. Alejandro Von Humboldt', '34', 'Nueva Tijuana'),
(2, 'Huertas', 'tallerhuertas@gmail.com', '664 629 5534', 'Av. Somelie', '4805', 'Puerta del Sol'),
(3, 'Rodriguez', 'tallerrodriguez@hotmail.com', '664 360 2076', 'Calle Jacinto', '24300', 'La Morita'),
(4, 'Fixcartj', 'fixcartj@gmail.com', '664 212 4018', 'Las Californias', '23109', 'El Pipila'),
(5, 'Garage 88', 'tallergarage88@hotmail.com', '663 204 4083', 'Cto. De las Acacias', '9254 6C', 'El Refugio'),
(6, 'JQ POWER Auto-Service', 'jqpower@gmail.com', '664 331 0241', 'Josefa Ortiz de Dominguez', '495741', 'Mariano Matamoros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_tipolicencias`
--

CREATE TABLE `home_tipolicencias` (
  `num` int(11) NOT NULL,
  `nombre` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_tipolicencias`
--

INSERT INTO `home_tipolicencias` (`num`, `nombre`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'D');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_tiposcoberturas`
--

CREATE TABLE `home_tiposcoberturas` (
  `num` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_tiposcoberturas`
--

INSERT INTO `home_tiposcoberturas` (`num`, `nombre`) VALUES
(1, 'Total'),
(2, 'Colision'),
(3, 'Robo'),
(4, 'Lesiones personales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_tiposvehiculos`
--

CREATE TABLE `home_tiposvehiculos` (
  `num` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_tiposvehiculos`
--

INSERT INTO `home_tiposvehiculos` (`num`, `nombre`) VALUES
(1, 'Moto'),
(2, 'Carro'),
(3, 'Camioneta'),
(4, 'Tractocamion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_usuarios`
--

CREATE TABLE `home_usuarios` (
  `password` varchar(128) DEFAULT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `email` varchar(254) NOT NULL,
  `nombrePila` varchar(255) DEFAULT NULL,
  `apPaterno` varchar(255) DEFAULT NULL,
  `apMaterno` varchar(255) DEFAULT NULL,
  `numTel` varchar(15) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `rol_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_usuarios`
--

INSERT INTO `home_usuarios` (`password`, `last_login`, `num`, `email`, `nombrePila`, `apPaterno`, `apMaterno`, `numTel`, `created_at`, `updated_at`, `is_staff`, `is_superuser`, `is_active`, `rol_id`) VALUES
('pbkdf2_sha256$600000$JkXjNhKu2mcASHyqyKATSo$2qpXhKmDJyK10cH+KnZEwN+whdCBm7JB1bfA/4jXdTw=', '2023-11-28 04:36:46.930142', 1, '0322103790@ut-tijuana.edu.mx', NULL, NULL, NULL, '', '2023-11-28 00:49:52.659822', '2023-11-28 00:49:52.659853', 1, 1, 1, 1),
('pbkdf2_sha256$600000$9b1JaMdvIARet7WRAd23zl$ESK8swww5qhCv0191+wj2mH7bkvkOEdcdUumKWzlfRE=', '2023-11-28 05:00:58.489416', 2, 'cesargr@gmail.com', 'Cesar', 'Guitierrez', 'Ramirez', '6662178950', '2023-11-28 04:52:35.611089', '2023-11-28 04:52:35.611134', 1, 1, 1, 1),
('pbkdf2_sha256$600000$lfNtAdcEyJhdvjhU846Ody$UKwiu2NZFOq+HgiRU24Nhi1kViYNdJsSl2k0FPPbsaQ=', NULL, 3, 'hugovz@gmail.com', 'Hugo', 'Velazquez', 'Castillo', '6645091823', '2023-11-28 04:53:54.003812', '2023-11-28 04:53:54.003849', 0, 0, 0, 2),
('pbkdf2_sha256$600000$Ngbri3FzDm4FZmDhO9tU6E$RDBlZ+GXrdDaxmPDZS6z/TgCEotZPuFgS2Vfarf3BpI=', NULL, 4, 'sofiadv@gmail.com', 'Sofia', 'Diaz', 'Vargas', '6647643210', '2023-11-28 04:54:33.626657', '2023-11-28 04:54:33.626694', 0, 0, 0, 2),
('pbkdf2_sha256$600000$4SVamTeXYMOMcGv7Jj5YLr$mO7+Gha5GyQetH0WlXzXU8cBEkEu6mZL1bzBJgWZeEY=', NULL, 5, 'hernestoup@gmail.com', 'Hernesto', 'Urias', 'Peña', '6641357892', '2023-11-28 04:54:50.602862', '2023-11-28 04:54:50.602898', 1, 1, 1, 1),
('pbkdf2_sha256$600000$TxYZKr0utanY6OqJTgM61t$rQ5dX1JLihycMAAVE38aQBEUy9E0gNNdHf0+5z0pQ7Q=', NULL, 6, 'alisonmv@gmail.com', 'Alison', 'Mejia', 'Valenzuela', '6648901245', '2023-11-28 04:55:04.926309', '2023-11-28 04:55:04.926344', 0, 0, 0, 2),
('pbkdf2_sha256$600000$75dIGigDw872KzyIUQZkth$g3je762MhrXIk1sGLab4Fv+rvKEMgbbX7LfV2DI/6UM=', NULL, 7, 'fernandalg@gmail.com', 'Fernanda', 'Lopez', 'Garcia', '6646732581', '2023-11-28 04:55:20.374066', '2023-11-28 04:55:20.374102', 0, 0, 0, 2),
('pbkdf2_sha256$600000$2Di0kLPr7ato1PEddFAdJ7$WMP0/nQkUbSSKQ83y94rPVISUAtpkhw1oGWGLRKtTKU=', NULL, 8, 'estebanms@gmail.com', 'Esteban', 'Martinez', 'Santos', '6644321987', '2023-11-28 04:55:34.555017', '2023-11-28 04:55:34.555052', 1, 1, 1, 1),
('pbkdf2_sha256$600000$FjGkW2gO9s51YVdzFBkx3O$rWhqLrJKwj01GvQESXJLUHnqdB8v8C6+GxU4DRH7MQ4=', NULL, 9, 'liampb@gmail.com', 'Liam', 'Patel', 'Brown', '6642098173', '2023-11-28 04:55:47.262562', '2023-11-28 04:55:47.262603', 0, 0, 0, 2),
('pbkdf2_sha256$600000$ZIgOzh56oPRmLc7vbmxIEw$cnzS9bsG2c7Mu4jeJdR5I3mRjWpyLLTJ7WWnVCuTK8c=', NULL, 10, 'isabelladc@gmail.com', 'Isabella', 'Dubois', 'Chang', '6647583921', '2023-11-28 04:55:59.233379', '2023-11-28 04:55:59.233413', 0, 0, 0, 2),
('pbkdf2_sha256$600000$LO6WhUw5QImd4fZ09aBlAz$HP3CmoicazoAsiX2CE1avwlv1g6pLQyfRl4gmQrkVj4=', NULL, 11, 'noahga@gmail.com', 'Noah', 'Garcia', 'Almeida', '6644658712', '2023-11-28 04:56:14.243766', '2023-11-28 04:56:14.243801', 0, 0, 0, 2),
('pbkdf2_sha256$600000$HMWFD4G7x7Eu8UfaED1I6q$rvlzhik83DXTzTdyWy7IFvcsMHRzG9n9YHsjwK1lcCo=', NULL, 12, 'ethanml@gmail.com', 'Ethan', 'Muller', 'Li', '6641234567', '2023-11-28 04:56:26.874757', '2023-11-28 04:56:26.874818', 1, 1, 1, 1),
('pbkdf2_sha256$600000$4s6I3zN7wrRk7CoR4nb7VX$RO+Ul/cxwOpxdBDor6bMZbXTNGRf1r4dAhIvkg+EEic=', NULL, 13, 'lucastw@gmail.com', 'Lucas', 'Taylor', 'Williams', '6648901723', '2023-11-28 04:56:38.020012', '2023-11-28 04:56:38.020046', 1, 1, 1, 1),
('pbkdf2_sha256$600000$JQ6yAxoMpbdc0CAeOJ9Y8W$R20cN6YLgjHjYJ2tGJ0coMnfw7QAMaU6fvASEQT89as=', NULL, 14, 'avada@gmail.com', 'Ava', 'Davis', 'Anderson', '6646758490', '2023-11-28 04:56:48.874145', '2023-11-28 04:56:48.874180', 0, 0, 0, 2),
('pbkdf2_sha256$600000$lROloH6vQrlAe7rtxKbwMo$Rkce1cw9Y8ih2qsWv9ooiwDHkfp/KaYAilGJT34qgl0=', NULL, 15, 'jacksonsf@gmail.com', 'Jackson', 'Smith', 'Fischer', '6643129078', '2023-11-28 04:56:58.463322', '2023-11-28 04:56:58.463356', 1, 1, 1, 1),
('pbkdf2_sha256$600000$vipvKEb4quAZkCBDs3NGYn$poL4UUIAL30UXU0zTQbALVkJ/IrnpTRuRavKOJwoWcA=', NULL, 16, 'miapt@gmail.com', 'Mia', 'Park', 'Tanaka', '6645647890', '2023-11-28 04:57:11.288354', '2023-11-28 04:57:11.288390', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_vehiculos`
--

CREATE TABLE `home_vehiculos` (
  `num` int(11) NOT NULL,
  `numSerie` int(11) NOT NULL,
  `placas` varchar(7) NOT NULL,
  `kilometraje` int(11) NOT NULL,
  `fechaAdquisicion` date NOT NULL,
  `usaDiesel` tinyint(1) NOT NULL,
  `marca_id` int(11) NOT NULL,
  `modelo_id` int(11) NOT NULL,
  `tipo_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_vehiculos`
--

INSERT INTO `home_vehiculos` (`num`, `numSerie`, `placas`, `kilometraje`, `fechaAdquisicion`, `usaDiesel`, `marca_id`, `modelo_id`, `tipo_id`) VALUES
(1, 872345619, 'ABC123X', 25000, '2023-01-15', 0, 1, 1, 1),
(2, 503218764, 'LMN456J', 42750, '2023-02-28', 0, 1, 1, 1),
(3, 946187230, 'UVW789Q', 58300, '2023-03-12', 0, 1, 1, 1),
(4, 215697804, 'XYZ345G', 15800, '2023-04-03', 0, 2, 2, 2),
(5, 798024615, '123ABCD', 37500, '2023-05-22', 0, 2, 2, 2),
(6, 324509781, 'HIJ456L', 22900, '2023-06-08', 0, 2, 2, 2),
(7, 670315824, '789PQRS', 46700, '2023-07-17', 0, 3, 3, 2),
(8, 158492367, 'NOP123U', 12450, '2023-08-30', 0, 3, 3, 2),
(9, 469253180, '456QRST', 40300, '2023-09-05', 0, 3, 3, 2),
(10, 108267409, 'VWX789Y', 28600, '2023-10-19', 0, 3, 3, 2),
(11, 238697154, '123LMN8', 55200, '2023-11-02', 0, 4, 4, 3),
(12, 567890432, 'XYZ456D', 18900, '2023-11-11', 0, 4, 4, 3),
(13, 934726508, 'HIJK678', 49800, '2023-01-25', 0, 4, 4, 3),
(14, 124530897, '345NOPQ', 33700, '2023-02-10', 0, 4, 4, 3),
(15, 865309247, 'ABC678Y', 27300, '2023-03-28', 0, 4, 4, 3),
(16, 430987652, 'VWX890D', 52100, '2023-04-15', 0, 4, 4, 3),
(17, 529384670, '123456H', 14200, '2023-05-05', 0, 4, 4, 3),
(18, 739461205, 'LMN789O', 39400, '2023-06-20', 1, 5, 5, 4),
(19, 298574630, 'TUVWXYZ', 44600, '2023-07-03', 1, 5, 5, 4),
(20, 321098765, '678ABCD', 21000, '2023-08-12', 1, 5, 5, 4),
(21, 975618237, 'NOP123Q', 36800, '2023-09-29', 1, 5, 5, 4),
(22, 458130296, '456UVWX', 50900, '2023-10-08', 1, 5, 5, 4),
(23, 102345678, 'BCDEFGH', 24300, '2023-11-18', 1, 6, 6, 4),
(24, 647582910, 'LMN678Q', 43100, '2023-11-01', 1, 6, 6, 4),
(25, 931276540, '789UVWX', 30500, '2023-01-07', 1, 6, 6, 4),
(26, 362401987, '1234ABC', 27300, '2023-02-19', 1, 6, 6, 4),
(27, 815947023, 'FGHIJK6', 47400, '2023-03-05', 1, 6, 6, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `home_vehiculosusuarios`
--

CREATE TABLE `home_vehiculosusuarios` (
  `vehiculo_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `home_vehiculosusuarios`
--

INSERT INTO `home_vehiculosusuarios` (`vehiculo_id`, `usuario_id`) VALUES
(2, 3),
(7, 4),
(22, 6),
(5, 9),
(11, 10),
(12, 11),
(6, 14);

--
-- Disparadores `home_vehiculosusuarios`
--
DELIMITER $$
CREATE TRIGGER `verificarVehiculos` BEFORE INSERT ON `home_vehiculosusuarios` FOR EACH ROW BEGIN
        DECLARE cantVehiculo INT;
        
        set cantVehiculo = (SELECT COUNT(v.num)
                            FROM home_vehiculos as v
                            WHERE num = new.vehiculo_id
                            AND v.num NOT IN (SELECT vehiculo_id
                                                FROM home_vehiculosusuarios));
        
        IF (cantVehiculo = 0)
        THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT= "El vehiculo no esta disponible ";
        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `knox_authtoken`
--

CREATE TABLE `knox_authtoken` (
  `digest` varchar(128) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `expiry` datetime(6) DEFAULT NULL,
  `token_key` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `knox_authtoken`
--

INSERT INTO `knox_authtoken` (`digest`, `created`, `user_id`, `expiry`, `token_key`) VALUES
('8edccf7970100f4afce0f3aab02b9433c1450d168a8e1aeecc7662e1ba1d37669d72f2b662ba9dc7618b9a73c212c1bd5d51a3dcdf39d87c1c3a3df8f4bdf965', '2023-11-28 05:00:58.486877', 2, '2023-11-29 05:00:58.486621', '500e6242');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_home_usuarios_num` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indices de la tabla `home_aseguradoras`
--
ALTER TABLE `home_aseguradoras`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_cargascombustible`
--
ALTER TABLE `home_cargascombustible`
  ADD PRIMARY KEY (`num`),
  ADD KEY `home_cargascombustib_vehiculo_id_e6ee647f_fk_home_vehi` (`vehiculo_id`);

--
-- Indices de la tabla `home_documentosvehiculos`
--
ALTER TABLE `home_documentosvehiculos`
  ADD PRIMARY KEY (`num`),
  ADD KEY `home_documentosvehic_vehiculo_id_3ad361ca_fk_home_vehi` (`vehiculo_id`);

--
-- Indices de la tabla `home_estadoinfraccion`
--
ALTER TABLE `home_estadoinfraccion`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_estadoslicencias`
--
ALTER TABLE `home_estadoslicencias`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_estadosparadas`
--
ALTER TABLE `home_estadosparadas`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_estadosreclamaciones`
--
ALTER TABLE `home_estadosreclamaciones`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_estadosrutas`
--
ALTER TABLE `home_estadosrutas`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_infracciones`
--
ALTER TABLE `home_infracciones`
  ADD PRIMARY KEY (`num`),
  ADD KEY `home_infracciones_conductor_id_5402cf69_fk_home_usuarios_num` (`conductor_id`),
  ADD KEY `home_infracciones_vehiculo_id_96f0232c_fk_home_vehiculos_num` (`vehiculo_id`);

--
-- Indices de la tabla `home_infraccionestadoinfraccion`
--
ALTER TABLE `home_infraccionestadoinfraccion`
  ADD PRIMARY KEY (`infraccion_id`),
  ADD KEY `home_infraccionestad_estado_id_45a1fb21_fk_home_esta` (`estado_id`);

--
-- Indices de la tabla `home_licencias`
--
ALTER TABLE `home_licencias`
  ADD PRIMARY KEY (`num`),
  ADD KEY `home_licencias_conductor_id_2b272cba_fk_home_usuarios_num` (`conductor_id`),
  ADD KEY `home_licencias_tipo_id_fcefb4b6_fk_home_tipolicencias_num` (`tipo_id`);

--
-- Indices de la tabla `home_licenciasestadoslic`
--
ALTER TABLE `home_licenciasestadoslic`
  ADD PRIMARY KEY (`licencia_id`),
  ADD KEY `home_licenciasestado_estado_id_70006fbd_fk_home_esta` (`estado_id`);

--
-- Indices de la tabla `home_marcasvehiculos`
--
ALTER TABLE `home_marcasvehiculos`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_modelosvehiculos`
--
ALTER TABLE `home_modelosvehiculos`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_paradas`
--
ALTER TABLE `home_paradas`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_paradasestadosparadas`
--
ALTER TABLE `home_paradasestadosparadas`
  ADD PRIMARY KEY (`parada_id`);

--
-- Indices de la tabla `home_polizasseguros`
--
ALTER TABLE `home_polizasseguros`
  ADD PRIMARY KEY (`numPoliza`),
  ADD KEY `home_polizasseguros_cobertura_id_8e2bf5bf_fk_home_tipo` (`cobertura_id`),
  ADD KEY `home_polizasseguros_vehiculo_id_91ff12fb_fk_home_vehiculos_num` (`vehiculo_id`),
  ADD KEY `home_polizasseguros_aseguradora_id_272e81f6_fk_home_aseg` (`aseguradora_id`);

--
-- Indices de la tabla `home_reclamacionesestadosrec`
--
ALTER TABLE `home_reclamacionesestadosrec`
  ADD PRIMARY KEY (`reclamacion_id`),
  ADD KEY `home_reclamacioneses_estado_id_ae797c79_fk_home_esta` (`estado_id`);

--
-- Indices de la tabla `home_reclamacionesseguros`
--
ALTER TABLE `home_reclamacionesseguros`
  ADD PRIMARY KEY (`numFolio`),
  ADD KEY `home_reclamacionesse_numPoliza_id_f2fa5642_fk_home_poli` (`numPoliza_id`);

--
-- Indices de la tabla `home_roles`
--
ALTER TABLE `home_roles`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_rutas`
--
ALTER TABLE `home_rutas`
  ADD PRIMARY KEY (`num`),
  ADD KEY `home_rutas_vehiculo_id_f57dcc7b_fk_home_vehiculos_num` (`vehiculo_id`);

--
-- Indices de la tabla `home_rutasestadosrutas`
--
ALTER TABLE `home_rutasestadosrutas`
  ADD PRIMARY KEY (`ruta_id`),
  ADD KEY `home_rutasestadosrut_estado_id_30522b98_fk_home_esta` (`estado_id`);

--
-- Indices de la tabla `home_rutasparadas`
--
ALTER TABLE `home_rutasparadas`
  ADD PRIMARY KEY (`ruta_id`),
  ADD KEY `home_rutasparadas_parada_id_fdeab7e2_fk_home_paradas_num` (`parada_id`);

--
-- Indices de la tabla `home_sesionesmantenimiento`
--
ALTER TABLE `home_sesionesmantenimiento`
  ADD PRIMARY KEY (`num`),
  ADD KEY `home_sesionesmanteni_taller_id_0435d372_fk_home_tall` (`taller_id`),
  ADD KEY `home_sesionesmanteni_vehiculo_id_0d4caade_fk_home_vehi` (`vehiculo_id`);

--
-- Indices de la tabla `home_talleres`
--
ALTER TABLE `home_talleres`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_tipolicencias`
--
ALTER TABLE `home_tipolicencias`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_tiposcoberturas`
--
ALTER TABLE `home_tiposcoberturas`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_tiposvehiculos`
--
ALTER TABLE `home_tiposvehiculos`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `home_usuarios`
--
ALTER TABLE `home_usuarios`
  ADD PRIMARY KEY (`num`),
  ADD UNIQUE KEY `correo` (`email`),
  ADD KEY `home_usuarios_rol_id_efb68dcd_fk_home_roles_num` (`rol_id`);

--
-- Indices de la tabla `home_vehiculos`
--
ALTER TABLE `home_vehiculos`
  ADD PRIMARY KEY (`num`),
  ADD UNIQUE KEY `numSerie` (`numSerie`),
  ADD KEY `home_vehiculos_marca_id_c4531f0d_fk_home_marcasvehiculos_num` (`marca_id`),
  ADD KEY `home_vehiculos_modelo_id_72e4d6db_fk_home_modelosvehiculos_num` (`modelo_id`),
  ADD KEY `home_vehiculos_tipo_id_8c01c884_fk_home_tiposvehiculos_num` (`tipo_id`);

--
-- Indices de la tabla `home_vehiculosusuarios`
--
ALTER TABLE `home_vehiculosusuarios`
  ADD PRIMARY KEY (`vehiculo_id`),
  ADD KEY `home_vehiculosusuarios_usuario_id_bf786966_fk_home_usuarios_num` (`usuario_id`);

--
-- Indices de la tabla `knox_authtoken`
--
ALTER TABLE `knox_authtoken`
  ADD PRIMARY KEY (`digest`),
  ADD KEY `knox_authtoken_user_id_e5a5d899_fk_home_usuarios_num` (`user_id`),
  ADD KEY `knox_authtoken_token_key_8f4f7d47` (`token_key`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `home_aseguradoras`
--
ALTER TABLE `home_aseguradoras`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `home_cargascombustible`
--
ALTER TABLE `home_cargascombustible`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=529;

--
-- AUTO_INCREMENT de la tabla `home_documentosvehiculos`
--
ALTER TABLE `home_documentosvehiculos`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `home_estadoinfraccion`
--
ALTER TABLE `home_estadoinfraccion`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `home_estadoslicencias`
--
ALTER TABLE `home_estadoslicencias`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `home_estadosparadas`
--
ALTER TABLE `home_estadosparadas`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `home_estadosreclamaciones`
--
ALTER TABLE `home_estadosreclamaciones`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `home_estadosrutas`
--
ALTER TABLE `home_estadosrutas`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `home_infracciones`
--
ALTER TABLE `home_infracciones`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `home_licencias`
--
ALTER TABLE `home_licencias`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=667;

--
-- AUTO_INCREMENT de la tabla `home_marcasvehiculos`
--
ALTER TABLE `home_marcasvehiculos`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `home_modelosvehiculos`
--
ALTER TABLE `home_modelosvehiculos`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `home_paradas`
--
ALTER TABLE `home_paradas`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `home_polizasseguros`
--
ALTER TABLE `home_polizasseguros`
  MODIFY `numPoliza` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `home_reclamacionesseguros`
--
ALTER TABLE `home_reclamacionesseguros`
  MODIFY `numFolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12490;

--
-- AUTO_INCREMENT de la tabla `home_roles`
--
ALTER TABLE `home_roles`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `home_rutas`
--
ALTER TABLE `home_rutas`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `home_sesionesmantenimiento`
--
ALTER TABLE `home_sesionesmantenimiento`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `home_talleres`
--
ALTER TABLE `home_talleres`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `home_tipolicencias`
--
ALTER TABLE `home_tipolicencias`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `home_tiposcoberturas`
--
ALTER TABLE `home_tiposcoberturas`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `home_tiposvehiculos`
--
ALTER TABLE `home_tiposvehiculos`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `home_usuarios`
--
ALTER TABLE `home_usuarios`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `home_vehiculos`
--
ALTER TABLE `home_vehiculos`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD CONSTRAINT `authtoken_token_user_id_35299eff_fk_home_usuarios_num` FOREIGN KEY (`user_id`) REFERENCES `home_usuarios` (`num`);

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_home_usuarios_num` FOREIGN KEY (`user_id`) REFERENCES `home_usuarios` (`num`);

--
-- Filtros para la tabla `home_cargascombustible`
--
ALTER TABLE `home_cargascombustible`
  ADD CONSTRAINT `home_cargascombustib_vehiculo_id_e6ee647f_fk_home_vehi` FOREIGN KEY (`vehiculo_id`) REFERENCES `home_vehiculos` (`num`);

--
-- Filtros para la tabla `home_documentosvehiculos`
--
ALTER TABLE `home_documentosvehiculos`
  ADD CONSTRAINT `home_documentosvehic_vehiculo_id_3ad361ca_fk_home_vehi` FOREIGN KEY (`vehiculo_id`) REFERENCES `home_vehiculos` (`num`);

--
-- Filtros para la tabla `home_infracciones`
--
ALTER TABLE `home_infracciones`
  ADD CONSTRAINT `home_infracciones_conductor_id_5402cf69_fk_home_usuarios_num` FOREIGN KEY (`conductor_id`) REFERENCES `home_usuarios` (`num`),
  ADD CONSTRAINT `home_infracciones_vehiculo_id_96f0232c_fk_home_vehiculos_num` FOREIGN KEY (`vehiculo_id`) REFERENCES `home_vehiculos` (`num`);

--
-- Filtros para la tabla `home_infraccionestadoinfraccion`
--
ALTER TABLE `home_infraccionestadoinfraccion`
  ADD CONSTRAINT `home_infraccionestad_estado_id_45a1fb21_fk_home_esta` FOREIGN KEY (`estado_id`) REFERENCES `home_estadoinfraccion` (`num`),
  ADD CONSTRAINT `home_infraccionestad_infraccion_id_4a46b54a_fk_home_infr` FOREIGN KEY (`infraccion_id`) REFERENCES `home_infracciones` (`num`);

--
-- Filtros para la tabla `home_licencias`
--
ALTER TABLE `home_licencias`
  ADD CONSTRAINT `home_licencias_conductor_id_2b272cba_fk_home_usuarios_num` FOREIGN KEY (`conductor_id`) REFERENCES `home_usuarios` (`num`),
  ADD CONSTRAINT `home_licencias_tipo_id_fcefb4b6_fk_home_tipolicencias_num` FOREIGN KEY (`tipo_id`) REFERENCES `home_tipolicencias` (`num`);

--
-- Filtros para la tabla `home_licenciasestadoslic`
--
ALTER TABLE `home_licenciasestadoslic`
  ADD CONSTRAINT `home_licenciasestado_estado_id_70006fbd_fk_home_esta` FOREIGN KEY (`estado_id`) REFERENCES `home_estadoslicencias` (`num`),
  ADD CONSTRAINT `home_licenciasestado_licencia_id_4640b80b_fk_home_lice` FOREIGN KEY (`licencia_id`) REFERENCES `home_licencias` (`num`);

--
-- Filtros para la tabla `home_paradasestadosparadas`
--
ALTER TABLE `home_paradasestadosparadas`
  ADD CONSTRAINT `home_paradasestadosp_parada_id_9d7b198b_fk_home_para` FOREIGN KEY (`parada_id`) REFERENCES `home_paradas` (`num`);

--
-- Filtros para la tabla `home_polizasseguros`
--
ALTER TABLE `home_polizasseguros`
  ADD CONSTRAINT `home_polizasseguros_aseguradora_id_272e81f6_fk_home_aseg` FOREIGN KEY (`aseguradora_id`) REFERENCES `home_aseguradoras` (`num`),
  ADD CONSTRAINT `home_polizasseguros_cobertura_id_8e2bf5bf_fk_home_tipo` FOREIGN KEY (`cobertura_id`) REFERENCES `home_tiposcoberturas` (`num`),
  ADD CONSTRAINT `home_polizasseguros_vehiculo_id_91ff12fb_fk_home_vehiculos_num` FOREIGN KEY (`vehiculo_id`) REFERENCES `home_vehiculos` (`num`);

--
-- Filtros para la tabla `home_reclamacionesestadosrec`
--
ALTER TABLE `home_reclamacionesestadosrec`
  ADD CONSTRAINT `home_reclamacioneses_estado_id_ae797c79_fk_home_esta` FOREIGN KEY (`estado_id`) REFERENCES `home_estadosreclamaciones` (`num`),
  ADD CONSTRAINT `home_reclamacioneses_reclamacion_id_154478f5_fk_home_recl` FOREIGN KEY (`reclamacion_id`) REFERENCES `home_reclamacionesseguros` (`numFolio`);

--
-- Filtros para la tabla `home_reclamacionesseguros`
--
ALTER TABLE `home_reclamacionesseguros`
  ADD CONSTRAINT `home_reclamacionesse_numPoliza_id_f2fa5642_fk_home_poli` FOREIGN KEY (`numPoliza_id`) REFERENCES `home_polizasseguros` (`numPoliza`);

--
-- Filtros para la tabla `home_rutas`
--
ALTER TABLE `home_rutas`
  ADD CONSTRAINT `home_rutas_vehiculo_id_f57dcc7b_fk_home_vehiculos_num` FOREIGN KEY (`vehiculo_id`) REFERENCES `home_vehiculos` (`num`);

--
-- Filtros para la tabla `home_rutasestadosrutas`
--
ALTER TABLE `home_rutasestadosrutas`
  ADD CONSTRAINT `home_rutasestadosrut_estado_id_30522b98_fk_home_esta` FOREIGN KEY (`estado_id`) REFERENCES `home_estadosrutas` (`num`),
  ADD CONSTRAINT `home_rutasestadosrutas_ruta_id_814523eb_fk_home_rutas_num` FOREIGN KEY (`ruta_id`) REFERENCES `home_rutas` (`num`);

--
-- Filtros para la tabla `home_rutasparadas`
--
ALTER TABLE `home_rutasparadas`
  ADD CONSTRAINT `home_rutasparadas_parada_id_fdeab7e2_fk_home_paradas_num` FOREIGN KEY (`parada_id`) REFERENCES `home_paradas` (`num`),
  ADD CONSTRAINT `home_rutasparadas_ruta_id_2a34efc2_fk_home_rutas_num` FOREIGN KEY (`ruta_id`) REFERENCES `home_rutas` (`num`);

--
-- Filtros para la tabla `home_sesionesmantenimiento`
--
ALTER TABLE `home_sesionesmantenimiento`
  ADD CONSTRAINT `home_sesionesmanteni_taller_id_0435d372_fk_home_tall` FOREIGN KEY (`taller_id`) REFERENCES `home_talleres` (`num`),
  ADD CONSTRAINT `home_sesionesmanteni_vehiculo_id_0d4caade_fk_home_vehi` FOREIGN KEY (`vehiculo_id`) REFERENCES `home_vehiculos` (`num`);

--
-- Filtros para la tabla `home_usuarios`
--
ALTER TABLE `home_usuarios`
  ADD CONSTRAINT `home_usuarios_rol_id_efb68dcd_fk_home_roles_num` FOREIGN KEY (`rol_id`) REFERENCES `home_roles` (`num`);

--
-- Filtros para la tabla `home_vehiculos`
--
ALTER TABLE `home_vehiculos`
  ADD CONSTRAINT `home_vehiculos_marca_id_c4531f0d_fk_home_marcasvehiculos_num` FOREIGN KEY (`marca_id`) REFERENCES `home_marcasvehiculos` (`num`),
  ADD CONSTRAINT `home_vehiculos_modelo_id_72e4d6db_fk_home_modelosvehiculos_num` FOREIGN KEY (`modelo_id`) REFERENCES `home_modelosvehiculos` (`num`),
  ADD CONSTRAINT `home_vehiculos_tipo_id_8c01c884_fk_home_tiposvehiculos_num` FOREIGN KEY (`tipo_id`) REFERENCES `home_tiposvehiculos` (`num`);

--
-- Filtros para la tabla `home_vehiculosusuarios`
--
ALTER TABLE `home_vehiculosusuarios`
  ADD CONSTRAINT `home_vehiculosusuari_vehiculo_id_1d72294a_fk_home_vehi` FOREIGN KEY (`vehiculo_id`) REFERENCES `home_vehiculos` (`num`),
  ADD CONSTRAINT `home_vehiculosusuarios_usuario_id_bf786966_fk_home_usuarios_num` FOREIGN KEY (`usuario_id`) REFERENCES `home_usuarios` (`num`);

--
-- Filtros para la tabla `knox_authtoken`
--
ALTER TABLE `knox_authtoken`
  ADD CONSTRAINT `knox_authtoken_user_id_e5a5d899_fk_home_usuarios_num` FOREIGN KEY (`user_id`) REFERENCES `home_usuarios` (`num`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*
LISTO
    1. Ruta de un vehiculo
        -Num Serie Vehiculo
        -Nombre Marca
        -Nombre modelo
        -Anio vehiculo
        -Nombre coductor
        -Fecha inicio ruta
        -Nombre de la parada
        -Direccion de la parada
*/
SELECT
    v.numSerie as NoSerie,
    mav.nombre as Marca,
    mov.nombre as Modelo,
    DATE_FORMAT(v.fechaAdquisicion,"%Y") as "Anio Vehiculo",
    CONCAT(u.nombrePila," ",u.apPaterno," ",u.apMaterno) as Conductor,
    DATE_FORMAT(r.fechaHoraInicio,"%d/%m/%Y") as "Fecha de ruta",
    DATE_FORMAT(r.fechaHoraFin,"%d/%m/%Y") as "Fecha fin ruta",
    p.nombre as Parada,
    CONCAT(p.dirColonia," ",p.dirCalle," ",p.dirNumero) as "Direccion parada"
FROM home_vehiculos as v
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num
    INNER JOIN home_vehiculosusuarios as vu ON vu.vehiculo_id = v.num
    INNER JOIN home_usuarios as u ON vu.usuario_id = u.num
    INNER JOIN home_rutas as r ON r.vehiculo_id = v.num
    INNER JOIN home_rutasparadas as rp ON rp.ruta_id = r.num
    INNER JOIN home_paradas as p ON rp.parada_id = p.num
WHERE v.num = 2;

/*
LISTO
    2. Infracciones que ha tenido un vehiculo
        -Num Serie Vehiculo
        -Nombre Marca
        -Nombre modelo
        -Anio vehiculo
        -Num Infraccion
        -Fecha infraccion
        -Direccion infraccion
        -nombre conductor
        -Descripcion estado Infraccion
        -Motivo de la infraccion
*/
SELECT
    v.numSerie as NoSerie,
    mav.nombre as Marca,
    mov.nombre as Modelo,
    DATE_FORMAT(v.fechaAdquisicion,"%Y") as "Anio Vehiculo",
    i.num as NumInfraccion,
    DATE_FORMAT(i.fechahora,"%d/%m/%Y") as "Fecha infraccion",
    i.descripcion as DestripcionMotivo,
    CONCAT(i.dirColonia," ",i.dirCalle," ",i.dirNumero) as "Direccion infraccion",
    CONCAT(u.nombrePila," ",u.apPaterno," ",u.apMaterno) as Conductor,
    ei.descripcion as "Estado Infraccion"
FROM home_vehiculos as v
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num
    INNER JOIN home_infracciones as i ON i.vehiculo_id = v.num
    INNER JOIN home_usuarios as u ON i.conductor_id = u.num
    INNER JOIN home_infraccionestadoinfraccion as iei ON iei.infraccion_id = i.num
    INNER JOIN home_estadoinfraccion as ei ON iei.estado_id = ei.num
WHERE v.num = 2;

/*
LISTO
    3. Información de un vehículo
        a. Número de serie del vehículo
        b. Nombre de la marca vehiculo
        c. Nombre del modelo vehiculo
        d. Año del vehículo
        e. nombre tipo vehículo
        f. Tipo de documento
        g. Fecha de emisión del tipo de documento
        h. Fecha de vencimiento del tipo de documento
        i. Número de póliza del seguro
        j. Fecha de inicio de la póliza
        k. Fecha de vencimiento de la póliza
        l. Monto de cobertura de la póliza
        m. Descripción del tipo de cobertura
        n. Nombre de la aseguradora
*/
SELECT
    v.numSerie as NoSerie,
    mav.nombre as Marca,
    mov.nombre as Modelo,
    DATE_FORMAT(v.fechaAdquisicion,"%Y") as "Anio Vehiculo",
    tv.nombre as "Tipo Vehiculo",
    dv.tipo as Documento,
    DATE_FORMAT(dv.fechaEmision,"%d/%m/%Y") as "Fecha Emision Documento",
    DATE_FORMAT(dv.fechaVencimiento,"%d/%m/%Y") as "Fecha Vencimiento Documento",
    ps.numPoliza as Poliza,
    DATE_FORMAT(ps.fechaInicio,"%d/%m/%Y") as "Fecha inicio Poliza",
    DATE_FORMAT(ps.fechaVencimiento,"%d/%m/%Y") as "Fecha vencimiento Poliza",
    ps.montoCobertura as Cobertura,
    tc.nombre as "Descripcion Cobertura",
    a.nombre as Aseguradora
FROM home_vehiculos as v
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num
    INNER JOIN home_tiposvehiculos as tv ON v.tipo_id = tv.num
    INNER JOIN home_documentosvehiculos as dv ON dv.vehiculo_id = v.num
    INNER JOIN home_polizasseguros as ps ON ps.vehiculo_id = v.num
    INNER JOIN home_tiposcoberturas as tc ON ps.cobertura_id = tc.num
    INNER JOIN home_aseguradoras as a ON ps.aseguradora_id = a.num
WHERE v.`numSerie` = 647582910;

/*
LISTO
    4. Información de un conductor
        a. Nombre completo del conductor, en una columna
        b. Número de teléfono
        c. Correo electrónico
        d. Descripción del rol del conductor
        e. Número de licencia
        f. Fecha de expedición de la licencia
        g. Fecha de vencimiento de la licencia
        h. Descripción del tipo de licencia
        i. Descripción de estado de la licencia
*/
SELECT
    CONCAT(u.nombrePila," ",u.apPaterno," ",u.apMaterno) as Conductor,
    u.numTel as Telefono,
    u.email as Correo,
    r.nombre as Rol,
    l.num as Licencia,
    DATE_FORMAT(l.fechaExpedicion,"%d/%m/%Y") as "Fecha Expidicion licencia",
    DATE_FORMAT(l.fechaVencimiento,"%d/%m/%Y") as "Fecha Vencimiento licencia",
    tl.nombre as "Tipo licencia",
    el.descripcion as "Estado licencia"
FROM home_usuarios as u
    INNER JOIN home_roles as r ON u.rol_id = r.num
    INNER JOIN home_licencias as l ON l.conductor_id = u.num
    INNER JOIN home_tipolicencias as tl ON l.tipo_id = tl.num
    INNER JOIN home_licenciasestadoslic as lel ON lel.licencia_id = l.num
    INNER JOIN home_estadoslicencias as el ON lel.estado_id = el.num
WHERE u.num = 3;

/*
LISTO
    5. Cargas de combustible de un vehículo en un mes determinado
        a. Número de serie del vehículo
        b. Nombre de la marca
        c. Nombre del modelo
        d. Número de folio de la carga del combustible
        e. Fecha de carga del combustible
        f. Litros cargados
        g. Costo total de la carga
*/
SELECT
    v.numSerie as NoSerie,
    mav.nombre as Marca,
    mov.nombre as Modelo,
    cc.numFolio as Folio,
    DATE_FORMAT(cc.fecha,"%d/%m/%Y") as "Fecha Carga Combustible",
    cc.litrosCargados as "Litros Cargados",
    cc.costoTotal as Total
FROM home_vehiculos as v 
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num
    INNER JOIN home_cargascombustible as cc ON cc.vehiculo_id = v.num
WHERE v.num = 2
AND MONTH(cc.fecha) = 6
ORDER BY cc.fecha;

/*
LISTO
    6. Conductores con el mismo tipo de licencia
        a. Descripción del tipo de licencia
        b. Nombre completo del conductor, en una columna
        c. Descripción del rol del conductor
        d. Descripción del estado de la licencia
*/
SELECT
    tl.nombre as "Tipo de licencia",
    CONCAT(u.nombrePila," ",u.apPaterno," ",u.apMaterno) as Conductor,
    r.nombre as Rol,
    el.descripcion as "Estado Licencia"
FROM home_usuarios as u
    INNER JOIN home_licencias as l ON l.conductor_id = u.num
    INNER JOIN home_tipolicencias as tl ON l.tipo_id = tl.num
    INNER JOIN home_roles as r ON u.rol_id = r.num
    INNER JOIN home_licenciasestadoslic as lel ON lel.licencia_id = l.num
    INNER JOIN home_estadoslicencias as el ON lel.estado_id = el.num
WHERE tl.num = 1;

/*
LISTO
    7. Reclamaciones de seguro realizadas hacia un vehículo
        a. Número de serie del vehículo
        b. Nombre de la marca
        c. Nombre del modelo
        d. Año del vehículo
        e. Número de la póliza del seguro
        f. Nombre de la aseguradora
        g. Número de folio de la reclamación
        h. Fecha de la reclamación
        i. Descripción del incidente
        j. Monto del reclamo
        k. Descripción del estado de la reclamación
*/
SELECT
    v.numSerie as NoSerie,
    mav.nombre as Marca,
    mov.nombre as Modelo,
    DATE_FORMAT(v.fechaAdquisicion,"%Y") as "Anio Vehiculo",
    ps.numPoliza as "Num Poliza",
    a.nombre as Aseguradora,
    rs.numFolio as "Num Folio Reclamacion",
    DATE_FORMAT(rs.fecha,"%d/%m/%Y") as "Fecha Reclamacion",
    rs.descripcionIncidente as "Descripcion del incidente",
    rs.montoReclamo as "Monto reclamo",
    er.descripcion as "Estado Reclamacion"
FROM home_vehiculos as v 
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num
    INNER JOIN home_polizasseguros as ps ON ps.vehiculo_id = v.num
    INNER JOIN home_aseguradoras as a ON ps.aseguradora_id = a.num
    INNER JOIN home_reclamacionesseguros as rs ON rs.numPoliza_id = ps.numPoliza
    INNER JOIN home_reclamacionesestadosrec as rer ON rer.reclamacion_id = rs.numFolio
    INNER JOIN home_estadosreclamaciones as er ON rer.estado_id = er.num
WHERE v.num = 1;

/*
LISTO
    8. Historial de mantenimiento de un vehículo
        a. Número de serie del vehículo
        b. Nombre de la marca
        c. Nombre del modelo
        d. Año del vehículo
        e. Nombre del taller donde se le dió el mantenimiento
        f. Fecha del mantenimiento
        g. Descripción del mantenimiento
        h. Costo total del mantenimiento
*/
SELECT
    v.numSerie as NoSerie,
    mav.nombre as Marca,
    mov.nombre as Modelo,
    DATE_FORMAT(v.fechaAdquisicion,"%Y") as "Anio Vehiculo",
    t.nombre as Taller,
    DATE_FORMAT(sm.fecha,"%d/%m/%Y") as "Fecha Mantenimiento",
    sm.descripcion as "Descripcion Mantenimiento",
    sm.costoTotal as Total
FROM home_vehiculos as v 
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num
    INNER JOIN home_sesionesmantenimiento as sm ON sm.vehiculo_id = v.num
    INNER JOIN home_talleres as t ON sm.taller_id = t.num
WHERE v.num = 2
ORDER BY sm.fecha;

/*
LISTO
    9. Vehículos del mismo tipo
        a. Descripción del tipo de vehículo
        b. Número de serie del vehículo
        c. Nombre de la marca
        d. Nombre del modelo
        año del vehiculo
*/
SELECT
    tv.nombre as "Tipo de vehiculo",
    v.numSerie as "Num Serie",
    mav.nombre as Marca,
    mov.nombre as Modelo,
    DATE_FORMAT(v.fechaAdquisicion,"%Y") as "Anio Vehiculo"
FROM home_vehiculos as v
    INNER JOIN home_tiposvehiculos as tv ON v.tipo_id = tv.num
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num
WHERE tv.num = 1;

/*
LISTO
    10. Conductores con el mismo tipo de licencia
        a. Descripción del tipo de licencia
        b. Nombre completo del conductor, en una columna
        c. Descripción del estado de la licencia
*/
SELECT
    tl.nombre as "Tipo licencia",
    CONCAT(u.nombrePila," ",u.apPaterno," ",u.apMaterno) as Conductor,
    el.descripcion as "Estado Licencia"
FROM home_usuarios as u
    INNER JOIN home_licencias as l ON l.conductor_id = u.num
    INNER JOIN home_tipolicencias as tl ON l.tipo_id = tl.num
    INNER JOIN home_licenciasestadoslic as lel ON lel.licencia_id = l.num
    INNER JOIN home_estadoslicencias as el ON lel.estado_id = el.num
WHERE tl.num = 1;

/*
LISTO
    11. Obtener todos los vehículos
*/
SELECT 
    numSerie, 
    placas, 
    kilometraje, 
    fechaAdquisicion, 
    usaDiesel, 
    modelo_id AS Modelo, 
    marca_id AS Marca, 
    tipo_id AS Tipo 
FROM home_vehiculos as v
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num 
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num 
    INNER JOIN home_tiposvehiculos as t ON v.tipo_id = t.num;

/*
LISTO
    12. Los vehiculos que  tiene cada Conductor
        -Num Serie de Vehiculo
        -Marca del Vehiculo
        -Modelo del Vehiculo
        -Num usuario
        -Nombre del Usuario completo
*/
SELECT
    v.numSerie as NoSerie,
    mav.nombre as Marca,
    mov.nombre as Modelo,
    u.num as NoUsuario,
    CONCAT(u.nombrePila," ",u.apPaterno," ",u.apMaterno) as Conductor
FROM home_vehiculos as v
    INNER JOIN home_modelosvehiculos as mov ON v.modelo_id = mov.num
    INNER JOIN home_marcasvehiculos as mav ON v.marca_id = mav.num
    INNER JOIN home_vehiculosusuarios as vu ON vu.vehiculo_id = v.num
    INNER JOIN home_usuarios as u ON vu.usuario_id = u.num;

---TRIGGERS
/*
LISTO
    1. Verificar que el vehículo esté disponible cuando se 
       le asigna un conductor.
*/
DELIMITER $$
    CREATE TRIGGER verificarVehiculos
    BEFORE INSERT ON home_vehiculosusuarios
    FOR EACH ROW
    BEGIN
        DECLARE cantVehiculo INT;
        
        set cantVehiculo = (SELECT COUNT(v.num)
                            FROM home_vehiculos as v
                            WHERE num = new.vehiculo_id
                            AND v.num NOT IN (SELECT vehiculo_id
                                                FROM home_vehiculosusuarios));
        
        IF (cantVehiculo = 0)
        THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT= "El vehiculo no esta disponible ";
        END IF;
    END;
DELIMITER;

DROP TRIGGER verificarVehiculos;

--INSERT bueno (sin ejecutar)
INSERT INTO home_vehiculosusuarios
VALUES (1,7);
--INSERT malo
INSERT INTO home_vehiculosusuarios
VALUES (1,3);

SELECT COUNT(v.num)
FROM home_vehiculos as v
WHERE num = 3
AND v.num NOT IN (SELECT vehiculo_id
                    FROM home_vehiculosusuarios)

SELECT  v.num
FROM home_vehiculos as v
WHERE v.num IN (SELECT vehiculo
                FROM vehiculos_usuarios)


/*
LISTO
    2. Verificar que la fecha de reclamación de un seguro se 
       encuentre dentro de la fecha de vigencia de la póliza 
       del vehículo, cuando se vaya a registrar.
*/

DELIMITER $$
    CREATE TRIGGER verificaFechaRecSeguro
    BEFORE INSERT ON home_reclamacionesseguros
    FOR EACH ROW
    BEGIN
        DECLARE fechaInicioPoliza DATE;
        DECLARE fechaFinalPoliza DATE;

        set fechaInicioPoliza = (   SELECT fechaInicio
                                    FROM home_polizasseguros
                                    WHERE numPoliza = new.numPoliza_id);
        set fechaFinalPoliza = (SELECT fechaVencimiento
                                FROM home_polizasseguros
                                WHERE numPoliza = new.numPoliza_id);

        IF(new.fecha <= fechaInicioPoliza)
        THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT= "Fecha incorrecta";
        ELSE
            IF (new.fecha > fechaFinalPoliza)
            THEN                
                SIGNAL SQLSTATE '45000'
                SET MESSAGE_TEXT= "Poliza Vencida";
            END IF;
        END IF;
    END;
DELIMITER;

DROP TRIGGER verificaFechaRecSeguro;
SELECT fechaInicio
FROM home_polizasseguros
WHERE numPoliza = 1;

--INSERT bueno (Sin ejecutar)
INSERT INTO home_reclamacionesseguros(numFolio, fecha, descripcionIncidente, montoReclamo, numPoliza_id) 
VALUES (7,'2023-06-12','Lesion por choque ocasionado por terceros',10000,1);

--INSERT malo Poliza vencida
INSERT INTO home_reclamacionesseguros(numFolio, fecha, descripcionIncidente, montoReclamo, numPoliza_id) 
VALUES (8,'2024-06-12','Lesion por choque ocasionado por terceros',10000,1);

--INSERT malo fecha incorrecta
INSERT INTO home_reclamacionesseguros(numFolio, fecha, descripcionIncidente, montoReclamo, numPoliza_id) 
VALUES (9,'2020-06-12','Lesion por choque ocasionado por terceros',10000,1);

/*
    3. Cuando se vaya a registrar una licencia de un conductor, 
       verificar que la fecha de expedición de la licencia sea 
       superior a la fecha de vencimiento de su licencia
       anterior.
*/
DELIMITER $$
    CREATE TRIGGER verificarFechaLicencia
    BEFORE INSERT ON home_licencias
    FOR EACH ROW
    BEGIN
        DECLARE fechalicenciaVencio DATE;

        set fechalicenciaVencio = ( SELECT MAX(fechaVencimiento)
                                    FROM home_licencias
                                    WHERE conductor_id = new.conductor_id);

        IF(new.fechaExpedicion < fechalicenciaVencio)
        THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT= "Licencia vigente, no se puede renovar";
        END IF;
    END;
DELIMITER;

SELECT MAX(fechaVencimiento)
FROM home_licencias
WHERE conductor_id = 3;

INSERT INTO home_licencias(num, fechaExpedicion, fechaVencimiento, comentarios, conductor_id, tipo_id) 
VALUES (9,'2025-01-16','2026-01-16','Sin comentarios',3,1);

--INSERT bueno
INSERT INTO home_licencias(num, fechaExpedicion, fechaVencimiento, comentarios, conductor_id, tipo_id) 
VALUES (10,'2026-01-17','2027-01-16','Sin comentarios',3,1);

--INSERT malo
INSERT INTO home_licencias(num, fechaExpedicion, fechaVencimiento, comentarios, conductor_id, tipo_id) 
VALUES (11,'2025-01-16','2026-01-16','Sin comentarios',3,1);