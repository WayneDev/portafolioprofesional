const BASE_URL = "http://127.0.0.1:8000/api";

function fetchData(url, method = 'GET', headers = {'Content-Type': 'application/json'}, body = null) {
  if (method === 'GET' && body) {
    url += '?' + new URLSearchParams(body).toString();
    body = null;
  }
 
  return fetch(url, {
    method: method,
    headers: headers,
    body: body
  }).then(response => response.json());
 }

export function getVehiculos() {
    return fetchData(`${BASE_URL}/vehiculos`);
}

export function getVehiculo(numSerie) {
  return fetchData(`${BASE_URL}/vehiculos/${numSerie}`);
 }

 //RETORNA 500
 export function UpdateVehiculo(numSerie) {
  return fetchData(`${BASE_URL}/vehiculos/${numSerie}, 'POST'`);
 }


export function getMarcas() {
  return fetchData(`${BASE_URL}/marcavehiculos/`);
}

export function getMarca(num) {
  return fetchData(`${BASE_URL}/marcavehiculos/${num}`);
}

export function updateMarca(num, data) {
  console.log(data)
  return fetchData(`${BASE_URL}/marcavehicu\los/${num}`, 'PUT', {'Content-Type': 'application/json'}, JSON.stringify(data));
 }

export function getModelos() {
  return fetchData(`${BASE_URL}/modelovehiculos/`);
}

export function getModelo(num) {
  return fetchData(`${BASE_URL}/modelovehiculos/${num}`);
}

export function updateModelo(num, data) {
  return fetchData(`${BASE_URL}/modelovehiculos/${num}`, 'PUT', {'Content-Type': 'application/json'}, JSON.stringify(data));
 }

export function getDocumentosVehiculos() {
  return fetchData(`${BASE_URL}/documentosvehicl/`);
}

export function getDocumentosVehiculo(num) {
  return fetchData(`${BASE_URL}/documentosvehicl/${num}`);
}

export function updateDocumentosVehiculo(num, data) {
  return fetchData(`${BASE_URL}/documentosvehicl/${num}`, 'PUT', {'Content-Type': 'application/json'}, JSON.stringify(data));
 }

export function getCargasCombustible() {
  return fetchData(`${BASE_URL}/cargascombustible/`);
}

export function getCargaCombustible(num) {
  return fetchData(`${BASE_URL}/cargascombustible/${num}`);
}


export function getSesionesMantenimiento() {
  return fetchData(`${BASE_URL}/sesionesmant/`);
}

export function getSesionMantenimiento(num) {
  return fetchData(`${BASE_URL}/sesionesmant/${num}`);
}

export function getInfracciones() {
  return fetchData(`${BASE_URL}/infracciones/`);
}

export function getInfraccion(num) {
  return fetchData(`${BASE_URL}/infracciones/${num}`);
}

export function getLicencias() {
  return fetchData(`${BASE_URL}/licencias/`);
}

//ERROR 500
export function getLicencia(num) {
  return fetchData(`${BASE_URL}/licencias/${num}`);
}

export function getRutas() {
  return fetchData(`${BASE_URL}/rutas/`);
}

export function getRuta(num) {
  return fetchData(`${BASE_URL}/rutas/${num}`);
}


export function getAseguranzas() {
  return fetchData(`${BASE_URL}/aseguradoras/`);
}

export function getAseguranza(num) {
  return fetchData(`${BASE_URL}/aseguradoras/${num}`);
}

export function getPolizas() {
  return fetchData(`${BASE_URL}/polizasseg/`);
}

export function getPoliza(num) {
  return fetchData(`${BASE_URL}/polizasseg/${num}`);
}

export function getCoberturas() {
  return fetchData(`${BASE_URL}/tiposcoberturas/`);
}

export function getReclamaciones() {
  return fetchData(`${BASE_URL}/reclamacionesseg/`);
}

export function getReclamacion(num) {
  return fetchData(`${BASE_URL}/reclamacionesseg/${num}`);
}
