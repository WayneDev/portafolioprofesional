from django.urls import path
from . import views
from .views import *
from knox.views import LogoutView, LogoutAllView

app_name = "users"
urlpatterns = [
    path('create-user/', views.CreateUserAPI.as_view()),
    path('update-user/<int:pk>/', views.UpdateUserAPI.as_view()),
    path('login/', views.LoginAPIView.as_view()),
    path('logout/', LogoutView.as_view()), #VISTA PREDETERMINADA DE DJANGO
    path('logout-all/', LogoutAllView.as_view()), #VISTA PREDETERMINADA DE DJANGO

    # RUTAS API USUARIOS DATAS
    path('get_user/<int:pk>/', get_user, name='search_by_id'),
    path('get_all/', get_all_users, name= 'all_users'),
    path('delete/<int:pk>/', user_detail, name='user_detail'),
    path('conductor_users/', get_conductor_users, name='get_conductor_users'),
    path('admin_users/', get_admin_users, name='get_admin_users'),

    # RUTAS ACTIVOS/INACTIVOS
    path('usersinactive/', InactiveUsersView.as_view(), name='inactive_users_list'),
    path('usersactive/', ActiveUsersView.as_view(), name='active_users_list'),
    
]