from django.shortcuts import render
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from home.models import usuarios
from .serializers import CreateUserSerializer, UpdateUserSerializer, LoginSerializer
from knox import views as knox_views
from django.contrib.auth import login
from typing import Any
from django import http
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.http.response import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.views import generic, View
from django.views.generic import ListView
from django.urls import reverse_lazy
from home.models import *
from home.forms import *
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
import json
from django.db.models import Count
from datetime import datetime, date
from django.views import View
from django.contrib.auth import authenticate
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from .serializers import UserSerializer, CreateUserSerializer

class CreateUserAPI(CreateAPIView):
    queryset = usuarios.objects.all()
    serializer_class = CreateUserSerializer
    permission_classes = (AllowAny,)

class UpdateUserAPI(UpdateAPIView):
    queryset = usuarios.objects.all()
    serializer_class = UpdateUserSerializer

class LoginAPIView(knox_views.LoginView):
    permission_classes = (AllowAny, )
    serializer_class = LoginSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.validated_data['user']
            login(request, user)
            response = super().post(request, format=None)
        else:
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

        return Response(response.data, status=status.HTTP_200_OK)
    
@api_view(['GET'])
def get_user(request, pk):
    try:
        user = usuarios.objects.get(num=pk)
        serializer = UserSerializer(user)
        return Response({'message': 'Success', 'user': serializer.data})
    except usuarios.DoesNotExist:
        return Response({'message': 'User not found'}, status=status.HTTP_404_NOT_FOUND)

@api_view(['GET'])
def get_all_users(request):
    users = usuarios.objects.all()
    serializer = UserSerializer(users, many=True)
    return Response({'message': 'Success', 'users': serializer.data})

@api_view(['GET', 'DELETE'])
def user_detail(request, pk):
    try:
        user = usuarios.objects.get(num=pk)
    except usuarios.DoesNotExist:
        return Response({'message': 'User not found'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response({'message': 'Success', 'user': serializer.data})

    elif request.method == 'DELETE':
        user.delete()
        return Response({'message': 'User deleted successfully'}, status=status.HTTP_204_NO_CONTENT)
    
@api_view(['GET'])
@permission_classes([AllowAny])
def get_conductor_users(request):
    conductor_users = usuarios.objects.filter(rol__nombre='Conductor')
    serializer = UserSerializer(conductor_users, many=True)
    return Response({'message': 'Success', 'conductor_users': serializer.data})

@api_view(['GET'])
@permission_classes([AllowAny])
def get_admin_users(request):
    admin_users = usuarios.objects.filter(rol__nombre='Administrador')
    serializer = UserSerializer(admin_users, many=True)
    return Response({'message': 'Success', 'admin_users': serializer.data})

@api_view(['GET'])
@permission_classes([AllowAny])
def get_users_active(request):
    pass

class InactiveUsersView(APIView):
    serializer_class = UserSerializer

    def get(self, request):
        inactive_users = usuarios.objects.filter(is_active=False)
        serializer = self.serializer_class(inactive_users, many=True)
        return Response({'message': 'Success', 'users': serializer.data}, status=status.HTTP_200_OK)

class ActiveUsersView(APIView):
    serializer_class = UserSerializer

    def get(self, request):
        active_users = usuarios.objects.filter(is_active=True)
        serializer = self.serializer_class(active_users, many=True)
        return Response({'message': 'Success', 'users': serializer.data}, status=status.HTTP_200_OK)


#APIs USUARIOS

# class UsuariosView(View):

#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=0):
#         usuariosview = []
#         if (pk > 0):
#             usuariosview = list(usuarios.objects.filter(num=pk).values())
#             if len(usuariosview) > 0:
#                 usuario = usuariosview[0]
#                 datos = {'message': "Succes", 'usuario': usuario}
#             else:
#                 datos = {'message': "usuario not found..."}
#             return JsonResponse(datos)
#         else:
#             usuariosview = list(usuarios.objects.values())
#             if len(usuariosview) > 0:
#                 datos = {'message': "Succes", 'usuarios': usuariosview}
#             else:
#                 datos = {'message': "usuarios not found..."}
#             return JsonResponse(datos)

#     def post(self, request):
#         jd = json.loads(request.body)

#         rol_instance = roles.objects.get(pk=jd['roles'])

#         # Crear una instancia de vehiculos con las instancias obtenidas
#         usuario = usuarios.objects.create(
#             nombrePila=jd['nombrePila'],
#             apPaterno=jd['apPaterno'],
#             apMaterno=jd['apMaterno'],
#             numTel=jd['numTel'],
#             correo=jd['correo'],
#             contrasena=jd['contrasena'],
#             rol=rol_instance,
#         )

#         datos = {'message': "Success"}
#         return JsonResponse(datos)

#     def put(self, request, pk=0):
#         usuariosview = []
#         jd = json.loads(request.body)
#         usuariosview = list(usuarios.objects.filter(num=pk).values())
        
#         if len(usuariosview) > 0:
#             usuario = usuarios.objects.get(num=pk)
#             usuario.nombrePila=jd['nombrePila']
#             usuario.apPaterno=jd['apPaterno']
#             usuario.apMaterno=jd['apMaterno']
#             usuario.numTel=jd['numTel']
#             usuario.correo=jd['correo']
#             usuario.contrasena=jd['contrasena']
            
#             # Obtén instancias de modelosVehiculos, marcasVehiculos, y tiposVehiculos
#             try:
#                 rol_instance = roles.objects.get(num=jd['roles'])
#             except usuarios.DoesNotExist:
#                 datos = {'message': "Usuario not found..."}
#                 return JsonResponse(datos, status=404)

#             usuario.rol = rol_instance

#             usuario.save()
#             datos = {'message': "Success"}
#             return JsonResponse(datos)
#         else:
#             datos = {'message': "usuario not found..."}
#             return JsonResponse(datos, status=404)

#     def delete(self, request, pk=0):
#         usuariosview = list(usuarios.objects.filter(num=pk).values())
#         if len(usuariosview) > 0:
#             usuarios.objects.filter(num=pk).delete()
#             datos = {'message':"Succes"}
#         else:
#             datos = {'message': "usuario not found..."}
#         return JsonResponse(datos)

# class UsuariosView(View):
#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

    # def get(self, request, pk=0):
    #     usuariosview = []
    #     if pk > 0:
    #         usuariosview = list(usuarios.objects.filter(num=pk).values('num', 'nombrePila', 'apPaterno', 'apMaterno', 'numTel', 'correo', 'contrasena', 'rol__nombre'))
    #         if len(usuariosview) > 0:
    #             usuario = usuariosview[0]
    #             datos = {'message': "Success", 'usuario': usuario}
    #         else:
    #             datos = {'message': "Usuario not found..."}
    #         return JsonResponse(datos)
    #     else:
    #         usuariosview = list(usuarios.objects.values('num', 'nombrePila', 'apPaterno', 'apMaterno', 'numTel', 'correo', 'contrasena', 'rol__nombre'))
    #         if len(usuariosview) > 0:
    #             datos = {'message': "Success", 'usuarios': usuariosview}
    #         else:
    #             datos = {'message': "Usuarios not found..."}
    #         return JsonResponse(datos)
        
        #////////////////////REMPLAZADO POR CreateUserAPI/////////////
    # def post(self, request):
    #     jd = json.loads(request.body)

    #     # Validación: Correo único y contraseña no igual al nombre
    #     if usuarios.objects.filter(correo=jd['correo']).exists():
    #         datos = {'message': "Correo ya registrado. Por favor, elige otro correo."}
    #         return JsonResponse(datos, status=400)

    #     if jd['nombrePila'].lower() in jd['contrasena'].lower():
    #         datos = {'message': "La contraseña no puede ser igual al nombre."}
    #         return JsonResponse(datos, status=400)

    #     rol_instance = roles.objects.get(pk=jd['rol'])

    #     # Crear una instancia de usuarios con las instancias obtenidas
    #     usuario = usuarios.objects.create(
    #         nombrePila=jd['nombrePila'],
    #         apPaterno=jd['apPaterno'],
    #         apMaterno=jd['apMaterno'],
    #         numTel=jd['numTel'],
    #         correo=jd['correo'],
    #         contrasena=jd['contrasena'],
    #         rol=rol_instance,
    #     )

    #     datos = {'message': "Success"}
    #     return JsonResponse(datos)

        #////////////////////REMPLAZADO POR UpdateUserAPI/////////////
    # def put(self, request, pk=0):
    #     usuariosview = []
    #     jd = json.loads(request.body)
    #     usuariosview = list(usuarios.objects.filter(num=pk).values('num', 'nombrePila', 'apPaterno', 'apMaterno', 'numTel', 'correo', 'contrasena', 'rol__nombre'))
        
    #     if len(usuariosview) > 0:
    #         usuario = usuarios.objects.get(num=pk)
    #         usuario.nombrePila = jd['nombrePila']
    #         usuario.apPaterno = jd['apPaterno']
    #         usuario.apMaterno = jd['apMaterno']
    #         usuario.numTel = jd['numTel']
    #         usuario.correo = jd['correo']
    #         usuario.contrasena = jd['contrasena']
            
    #         # Obtén instancias de roles
    #         try:
    #             rol_instance = roles.objects.get(pk=jd['rol'])
    #         except usuarios.DoesNotExist:
    #             datos = {'message': "Usuario not found..."}
    #             return JsonResponse(datos, status=404)

    #         usuario.rol = rol_instance

    #         usuario.save()
    #         datos = {'message': "Success"}
    #         return JsonResponse(datos)
    #     else:
    #         datos = {'message': "Usuario not found..."}
    #         return JsonResponse(datos, status=404)

    # def delete(self, request, pk=0):
    #     usuariosview = list(usuarios.objects.filter(num=pk).values('num', 'nombrePila', 'apPaterno', 'apMaterno', 'numTel', 'correo', 'contrasena', 'rol__nombre'))
    #     if len(usuariosview) > 0:
    #         usuarios.objects.filter(num=pk).delete()
    #         datos = {'message':"Success"}
    #     else:
    #         datos = {'message': "Usuario not found..."}
    #     return JsonResponse(datos)

# #APIs CONDUCTORES
# class ConductoresView(View):
#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super().dispatch(request, *args, **kwargs)

#     def get(self, request, pk=None):
#         if pk:
#             conductor = usuarios.objects.filter(rol__nombre='Conductor', num=pk).values('num', 'nombrePila', 'apPaterno', 'apMaterno', 'numTel', 'correo', 'contrasena', 'rol__nombre').first()
#             if conductor:
#                 datos = {'message': "Success", 'conductor': conductor}
#             else:
#                 datos = {'message': f"Conductor with ID {pk} not found..."}
#         else:
#             conductores_view = list(usuarios.objects.filter(rol__nombre='Conductor').values('num', 'nombrePila', 'apPaterno', 'apMaterno', 'numTel', 'correo', 'contrasena', 'rol__nombre'))
#             if len(conductores_view) > 0:
#                 datos = {'message': "Success", 'conductores': conductores_view}
#             else:
#                 datos = {'message': "No conductores found..."}
        
#         return JsonResponse(datos)